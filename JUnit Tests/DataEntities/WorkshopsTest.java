package DataEntities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class WorkshopsTest {

    public Workshops ws;

    @Before
    public void beforeTest() {
        ws = new Workshops(1);
    }

    @After
    public void afterTest() {
        ws = null;
    }

    @Test
    public void getWorkshopList() {
        assertTrue(ws.getWorkshopList() != null);
    }

    @Test@Before
    public void addWorkshopToDatabase() {
        ArrayList<String> newWs = new ArrayList<>();
        newWs.add(0, "Borris Service");
        newWs.add(1, "Baguettroad 37");
        newWs.add(2, "12345678");
        newWs.add(3, "Very good");
        assertTrue(ws.AddWorkshopToDatabase(newWs));
    }

    public int getId() {
        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;
        int id = 0;
        try {
            String sql = "SELECT workshop_id FROM workshop WHERE name = 'Borris Service'";
            con = MysqlConnect.getConnection();
            stmt = con.prepareStatement(sql);
            res = stmt.executeQuery();

            while(res.next()) {
                id = res.getInt("workshop_id");
            }
        } catch (Exception e) {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        return id;
    }

    @Test
    public void editWorkshopInDatabase() {
        ArrayList<String> edit = new ArrayList<>();
        edit.add(0, "Borris Service");
        edit.add(1, "Baguettroad 37");
        edit.add(2, "123456789");
        edit.add(3, "Very good");
        assertTrue(ws.editWorkshopInDatabase(edit, getId()));
    }

    @Test@After
    public void deletWorkshopFromDatabase() {
        assertTrue(ws.deletWorkshopFromDatabase(getId()));
    }

    @Test
    public void getWorkshopID() {
        int id = ws.getWorkshopID();
        assertTrue(id == 1);
    }

    @Test
    public void getName() {
        String name = ws.getName();
        assertTrue(name.equals("eTramp HQ"));
    }

    @Test
    public void getAdress() {
        String addr = ws.getAdress();
        assertTrue(addr.equals("Kalvskinnet 1"));
    }

    @Test
    public void getPhone() {
        int phone = ws.getPhone();
        assertTrue(phone == 98815562);
    }

    @Test
    public void getComments() {
        String comment = ws.getComments();
        assertTrue(comment.equals("Transit station for fixed bikes. After repairs, the bikes will be transferred here."));
    }
}