package DataEntities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import static org.junit.Assert.*;

public class DataHandelerTest {

    private DataHandeler dh;

    @Before
    public void beforeTest() {
        dh = new DataHandeler();
    }

    @After
    public void afterTest() {
        dh = null;
    }

    @Test
    public void login() {
        String uname = "oyvinval";
        String pass = "heioghopp";
        assertTrue(dh.login(uname, pass));
    }

    @Test
    public void getScrollPane() {
        String[] header = {"ID", "Name", "Make"};
        String sql = "SELECT * FROM model";
        assertTrue(dh.getScrollPane(sql, header) != null);
    }

    //todo: er dette riktig?

    @Test
    public void buildTableModel() throws SQLException {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = MysqlConnect.getConnection();
            stmt = con.prepareStatement("SELECT model_id FROM model WHERE make = 1");
            rs = stmt.executeQuery();
            while(rs.next()){
            }
        } catch (Exception e) {
            MysqlConnect.closeResSet(rs);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }

        String[] cols = {"Col1", "Col2", "Col3"};
        assertTrue(dh.buildTableModel(rs, cols) != null);
    }

    @Test
    public void validDate() {
        Date sqlDate = dh.validDate("2018-04-20");
        assertTrue(sqlDate != null);
    }

    @Test
    public void getRandomPassword() {
        assertTrue(dh.getRandomPassword() != null);
    }
}