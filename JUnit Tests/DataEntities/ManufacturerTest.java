package DataEntities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ManufacturerTest {
    int id;
    Manufacturer man=null;


    @Test
    public void getManufacturerList() {
//        test checks if one given string  is in the ManufaturerList
        String[] a = Manufacturer.getManufacturerList();
        for (String b : a) {
            if (b.equals("Zenvo"))
                assertEquals("Zenvo", b);
        }
    }


    @Before
    public void addManufacturerToDatabase() {
        ArrayList<String> a = new ArrayList<>();
        a.add("theName");
        a.add("theCountry");
        assertTrue(Manufacturer.addManufacturerToDatabase(a));

        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = MysqlConnect.getConnection();
            String sql = "SELECT * FROM manufacturer WHERE country=? AND name=?;";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, "theCountry");
            stmt.setString(2, "theName");
            res = stmt.executeQuery();

            while (res.next()) {
                id = res.getInt("make_id");
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        man = new Manufacturer(id);
        assertNotNull(man);
    }

    @Test
    public void editManufacturerInDatabaes() {
        ArrayList<String> a = new ArrayList<>();
        a.add("1");
        a.add("2");
        Manufacturer.editManufacturerInDatabaes(a,id);
    }

    @Test@After
    public void deleteManufacturerFromDatabase() {
        Manufacturer.deleteManufacturerFromDatabase(id);
    }

    @Test
    public void getManufID() {
        assertEquals(man.getManufID(),id);
    }


    @Test
    public void getName() {
        assertEquals("theName",man.getName());
    }

    @Test
    public void getCountry() {
        assertEquals("theCountry",man.getCountry());
    }
}