package DataEntities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class BikeTest {

    private Bike bike;

    @Before
    public void setUp() throws Exception {
        Bike bike = new Bike(266);
    }

    @After
    public void tearDown() throws Exception {
        bike = null;
    }

    @Test
    public void getRandomBike() {


    }

    @Test
    public void editBikeInDatabase() {
        ArrayList<String> edit = new ArrayList<>();
        edit.add(0, "628405");
        edit.add(1, "Red");
        edit.add(2, "2725.37057784386");
        edit.add(3, "1");
        edit.add(4, "2018-03-15");
        edit.add(5, "Toyota");
        edit.add(6, "Carina");
        edit.add(7, "45");
        assertTrue(bike.editBikeInDatabase(edit, 45));
    }

    @Test
    public void insertBikeIntoDatabase() {
        ArrayList<String> insert = new ArrayList<>();
        insert.add(0, "226838745");
        insert.add(1, "Red");
        insert.add(2, "0.0");
        insert.add(3, "0");
        insert.add(4, "2018-04-16");
        insert.add(5, "Toyota");
        insert.add(6, "Carina");
        assertTrue(bike.insertBikeIntoDatabase(insert));
    }

    @Test
    public void sendBikeToRepair() {
        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;

        int bikeId = 0;
        try {
            String sql = "SELECT bicycle_id FROM bicycle WHERE registration_nr = 226838745";
            con = MysqlConnect.getConnection();
            stmt = con.prepareStatement(sql);
            res = stmt.executeQuery();

            while(res.next()) {
                bikeId = res.getInt("bicycle_id");
            }
        } catch (Exception e) {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }

        ArrayList<String> repair = new ArrayList<>();
        repair.add(0, "2018-04-16");
        repair.add(1, "300000");
        repair.add(2, "The pedal has broken down");
        repair.add(3, "Watts the Matter");
        repair.add(4, "Pedals");
        assertTrue(bike.sendBikeToRepair(repair, bikeId));
    }

    @Test
    public void returnBikeFromRepair() {
        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;

        int bikeId = 0;
        try {
            String sql = "SELECT bicycle_id FROM bicycle WHERE registration_nr = 226838745";
            con = MysqlConnect.getConnection();
            stmt = con.prepareStatement(sql);
            res = stmt.executeQuery();

            while(res.next()) {
                bikeId = res.getInt("bicycle_id");
            }
        } catch (Exception e) {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        assertTrue(bike.returnBikeFromRepair(bikeId));
    }

    @Test
    public void deleteBikeFromDatabase() {
        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;

        int bikeId = 0;
        try {
            String sql = "SELECT bicycle_id FROM bicycle WHERE registration_nr = 226838745";
            con = MysqlConnect.getConnection();
            stmt = con.prepareStatement(sql);
            res = stmt.executeQuery();

            while(res.next()) {
                bikeId = res.getInt("bicycle_id");
            }
        } catch (Exception e) {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        assertTrue(bike.deleteBikeFromDatabase(bikeId));
    }

    @Test
    public void countAllBikes() {
        int totalNumber = bike.countAllBikes();
        assertTrue(totalNumber >= 0);
    }

    @Test
    public void countBikesAtRepair() {
        int totalNumber = bike.countBikesAtRepair();
        assertTrue(totalNumber == 5);
    }
}