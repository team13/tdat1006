package DataEntities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import static org.junit.Assert.*;

//todo: oppretter en ny member på slutten? Hvorfor?

public class MemberTest {

    private Member member;

    @Before
    public void beforeTest() {
        member = new Member(1);
    }

    @After
    public void afterTest() {
        member = null;
    }

    @Test
    public void getRandomMember() {
        assertTrue(member.getRandomMember() != null);
    }

    @Test
    public void getMemberID() {
        int id = member.getMemberID();
        assertTrue(id == 1);
    }

    @Test
    public void getFirstname() {
        String fName = member.getFirstname();
        assertTrue(fName.equals("Bergliot"));
    }

    @Test
    public void getLastname() {
        String lName = member.getLastname();
        assertTrue(lName.equals("Holter"));
    }

    @Test
    public void getBirthyear() {
        int bYear = member.getBirthyear();
        assertTrue(bYear == 1994);
    }

    @Test
    public void getEmail() {
        String eMail = member.getEmail();
        assertTrue(eMail.equals("bergliot.holter@stud.ntnu.no"));
    }

    @Test
    public void getPhone() {
        int phone = member.getPhone();
        assertTrue(phone == 46602126);
    }

    @Test@Before
    public void addMemberToDatabase() {
        ArrayList<String> newMem = new ArrayList<>();
        newMem.add(0, "Borre");
        newMem.add(1, "Borresen");
        newMem.add(2, "1994");
        newMem.add(3, "borre.borresen@borris.no");
        newMem.add(4, "12345678");
        newMem.add(5, "Male");
        assertTrue(member.addMemberToDatabase(newMem));
    }

    @Test
    public void editMemberInDatabase() {
        ArrayList<String> edit = new ArrayList<>();
        edit.add(0, "Bergliot");
        edit.add(1, "Holter");
        edit.add(2, "1994");
        edit.add(3, "bergliot.holter@stud.ntnu.no");
        edit.add(4, "46602126");
        edit.add(5, "Female");
        assertTrue(member.editMemberInDatabase(edit, 1));
    }

    @Test@After
    public void deleteMemberFromDatabase() {
        assertTrue(member.deleteMemberFromDatabase(getId()));
    }

    public int getId() {
        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;
        int id = 0;
        try {
            String sql = "SELECT member_id FROM member WHERE email = 'borre.borresen@borris.no'";
            con = MysqlConnect.getConnection();
            stmt = con.prepareStatement(sql);
            res = stmt.executeQuery();

            while(res.next()) {
                id = res.getInt("member_id");
            }
        } catch (Exception e) {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        return id;
    }

    @Test
    public void getNumberOfMembers() {
        int numberOfMem = member.getNumberOfMembers();
        assertTrue(numberOfMem >= 0);
    }
}