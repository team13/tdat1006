package DataEntities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class AdminsTest {

    private Admins admin;

    @Before
    public void beforeTest() {
        admin = new Admins(5);
    }

    @After
    public void afterTest() {
        admin = null;
    }

    @Test
    public void getAdmin_id() {
        int id = admin.getAdmin_id();
        assertTrue(id == 5);
    }

    @Test
    public void getFirstname() {
        String name = admin.getFirstname();
        assertTrue(name.equals("Christian"));
    }

    @Test
    public void getLastname() {
        String name = admin.getLastname();
        assertTrue(name.equals("Dalseth"));
    }

    @Test
    public void getBirthyear() {
        int bYear = admin.getBirthyear();
        assertTrue(bYear == 1994);
    }

    @Test
    public void getEmail() {
        String eMail = admin.getEmail();
        assertTrue(eMail.equals("chrismd@stud.ntnu.no"));
    }

    @Test
    public void getPhone() {
        int number = admin.getPhone();
        assertTrue(number == 12345678);
    }

    @Test
    public void getUsername() {
        String text = admin.getUsername();
        assertTrue(text.equals("chrismd"));
    }

    @Test
    public void editAdminInDatabase() {
        ArrayList<String> name = new ArrayList<>();
        name.add(0, "Christian");
        name.add(1, "Dalseth");
        name.add(2, "1994");
        name.add(3, "chrismd@stud.ntnu.no");
        name.add(4, "12345678");
        name.add(5, "chrismd");
        name.add(6, "5");
        assertTrue(admin.editAdminInDatabase(name, 5));
    }

    @Test
    public void getCurrentUsersName() {
        String text = admin.getCurrentUsersName("chrismd");
        assertTrue(text.equals("Christian Dalseth"));
    }

    @Test
    public void insertNewAdmin() {
        ArrayList<String> name = new ArrayList<>();
        name.add(0, "Børre");
        name.add(1, "Bjarte");
        name.add(2, "1995");
        name.add(3, "borre.bjarte@smil.no");
        name.add(4, "12345789");
        name.add(5, "borbja");
        name.add(6, "heioghopp");
        assertTrue(admin.insertNewAdmin(name));
    }

    @Test
    public void changePassword() {
        ArrayList<String> pw = new ArrayList<>();
        pw.add(0, "heioghopp");
        pw.add(1, "hei");
        assertTrue(admin.changePassword(pw, "borbja"));
    }

    @Test
    public void deleteAdminFromDatabase() {
        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;
        int id = 0;
        try {
            String sql = "SELECT user_id FROM admin_users WHERE username = 'borbja'";
            con = MysqlConnect.getConnection();
            stmt = con.prepareStatement(sql);
            res = stmt.executeQuery();

            while(res.next()) {
                id = res.getInt("user_id");
            }
        } catch (Exception e) {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        assertTrue(admin.deleteAdminFromDatabase(id));
    }
}