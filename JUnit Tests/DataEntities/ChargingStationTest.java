package DataEntities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import static org.junit.Assert.*;

//todo: fikse metode for getPosition og setName

public class ChargingStationTest {

    private ChargingStation cs;

    @Before
    public void beforeTest() {
        cs = new ChargingStation(10);
    }

    @After
    public void afterTest() {
        cs = null;
    }

    @Test
    public void getRandomStation() {
    }

    @Test
    public void getStation_id() {
        int id = cs.getStation_id();
        assertTrue(id == 10);
    }

    @Test
    public void getName() {
        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;
        String name = "";
        try {
            String sql = "SELECT station_name FROM charging_station WHERE station_id = 10";
            con = MysqlConnect.getConnection();
            stmt = con.prepareStatement(sql);
            res = stmt.executeQuery();

            while(res.next()) {
                name = res.getString("station_name");
            }
        } catch (Exception e) {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        assertTrue(name.equals("Stubban"));
    }

    @Test
    public void getStreet_name() {
        String streetName = cs.getStreet_name();
        assertTrue(streetName.equals("Stubbanvegen"));
    }

    @Test
    public void getStreet_number() {
        String streetNumber = cs.getStreet_number();
        assertTrue(streetNumber.equals("65E"));
    }

    @Test
    public void getMAX_BIKES() {
        int maxBikes = cs.getMAX_BIKES();
        assertTrue(maxBikes == 8);
    }

    @Test
    public void getCurrent_nr_bikes() {
        int currentNrBikes = cs.getCurrent_nr_bikes();
        assertTrue(currentNrBikes == 0);
    }

    @Test
    public void getPostal() {
        int postal = cs.getPostal();
        assertTrue(postal == 7036);
    }

    @Test
    public void setName() {
        String name = "Samfundet";
        cs.setName(name);
        assertTrue(name.equals(cs.getName()));
    }

    @Test
    public void getPosition() {
        Stuff.map.GNSS pos = new Stuff.map.GNSS(63.39053269999999, 10.427873599999998, 122);
        assertTrue(cs.getPosition().equals(pos));
    }

    @Test
    public void insertChargingStationIntoDatabase() {
        ArrayList<String> insert = new ArrayList<>();
        insert.add(0, "Lerkevegen");
        insert.add(1, "13");
        insert.add(2, "2016");
        insert.add(3, "100");
        insert.add(4, "0.0");
        insert.add(5, "1.1");
        cs.insertChargingStationIntoDatabase(insert);
    }

    public int getId() {
        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;
        int id = 0;
        try {
            String sql = "SELECT station_id FROM charging_station WHERE street_name = 'Lerkevegen' AND street_nr = 13";
            con = MysqlConnect.getConnection();
            stmt = con.prepareStatement(sql);
            res = stmt.executeQuery();

            while(res.next()) {
                id = res.getInt("station_id");
            }
        } catch (Exception e) {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        return id;
    }

    @Test
    public void editChargingStationInDatabase() {
        ArrayList<String> edit = new ArrayList<>();
        edit.add(0, "Lerkevegen");
        edit.add(1, "13");
        edit.add(2, "2017");
        edit.add(3, "8");
        edit.add(4, "63.39053269999999");
        edit.add(5, "10.427873599999998");

        cs.editChargingStationInDatabase(edit, getId());
    }

    @Test
    public void deleteChargingStationFromDatabase() {
        cs.deleteChargingStationFromDatabase(getId());
    }
}