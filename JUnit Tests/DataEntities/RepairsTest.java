package DataEntities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RepairsTest {

    private Repairs rep;

    @Before
    public void beforeTest() {
        rep = new Repairs();
    }

    @After
    public void afterTest() {
        rep = null;
    }

    @Test
    public void getRepairReasons() {
        assertTrue(rep.getRepairReasons() != null);
    }
}