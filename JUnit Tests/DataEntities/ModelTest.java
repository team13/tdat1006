package DataEntities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class ModelTest {
    int manudId;
    Manufacturer man;
    int modelId;
    Model model;

    @Before
    public void setUp(){
        ArrayList<String> a = new ArrayList<>();
        a.add("Manuf");
        a.add("theCountry");
        Manufacturer.addManufacturerToDatabase(a);

        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = MysqlConnect.getConnection();
            String sql = "SELECT * FROM manufacturer WHERE country=? AND name=?;";
            stmt = con.prepareStatement(sql);
            stmt.setString(2, "Manuf");
            stmt.setString(1, "theCountry");
            res = stmt.executeQuery();

            while (res.next()) {
                manudId = res.getInt("make_id");
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        man = new Manufacturer(manudId);
        insertModelIntoDatabase();
    }

    @After@Test
    public void deleteModel(){
        Model.deletModelFromDatabase(modelId);
        Manufacturer.deleteManufacturerFromDatabase(manudId);
//        assertTrue(Manufacturer.addManufacturerToDatabase(a));

    }

    @Test
    public void getModelsList() {
        //        test checks if one given string  is in the modelList
        String[] a = Model.getModelsList("Toyota");
        for (String b : a) {
            if (b.equals("Prius"))
                assertEquals("Prius", b);
        }

    }


    public void insertModelIntoDatabase() {
        ArrayList<String> a = new ArrayList<>();
        a.add("asd");
        a.add("Manuf");

        Model.insertModelIntoDatabase(a);
        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = MysqlConnect.getConnection();
            String sql = "SELECT * FROM model WHERE make=? AND name=?;";
            stmt = con.prepareStatement(sql);
            stmt.setString(2, "asd");
            stmt.setInt(1, manudId);
            res = stmt.executeQuery();

            while (res.next()) {
                modelId = res.getInt("model_id");
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        model = new Model(modelId);
    }

    @Test
    public void editModelInDatabase() {
        ArrayList<String> a = new ArrayList<>();
        a.add("2.Name");
        a.add("BMW");

        assertTrue(Model.editModelInDatabase(a, modelId));
    }

    @Test
    public void getModelID() {
        assertEquals(model.getModelID(),modelId);
    }

    @Test
    public void getName() {
        assertEquals(model.getName(),"asd");
    }

    @Test
    public void getManufacturer() {
        assertEquals(model.getManufacturer(),man.getName());
    }

    @Test
    public void getManufacturerId() {
        assertEquals(model.getManufacturerId(),manudId);
    }
}