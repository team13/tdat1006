package Simulation;

import DataEntities.Bike;
import DataEntities.MysqlConnect;
import Stuff.map.GNSS;

import java.sql.*;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Simulation{

    private static int UPDATE_INTERVAL = 10;
    private static int numberOfBikesSimulated = 10;
    public static final int BIKES_ALL = 1;
    public static final int BIKES_IN_USE = 2;
    public static final int BIKES_CHARGING = 3;
    public static final int BIKES_AT_REPAIR = 4;
    public static final int BIKES_DECOMISSIONED = 5;
    private ArrayList<Bike> bikes;
    private static Bike aBike;
    private static Runnable getter = new Runnable() {
        @Override
        public void run() {
            aBike=Simulation.queryBikes(Simulation.BIKES_CHARGING, 1).get(0);
        }
    };

    public static void main(String[] args) {
        Simulation sim = new Simulation();
    }

    /**
     * This constructor creates a simulation-object and initiates a number of threads based on the numberOfBikesSimulated variable.
     * This enables the client to simulate the trips for every bicycle on the map.
     */
    public Simulation(){

        ScheduledExecutorService schedule = Executors.newScheduledThreadPool(numberOfBikesSimulated);
        bikes = queryBikes(BIKES_CHARGING, numberOfBikesSimulated);
        for (int i = 0; i < bikes.size(); i++) {
            MyThread r = new MyThread(i,bikes.get(i));

            schedule.scheduleAtFixedRate(r, 1, 3, TimeUnit.SECONDS);
        }
    }

    /**
     * This method gets a number of random bicycles from the database table bicycle.
     * @param bikestatus - A constant to describe which pool of bicycle to select from.
     * @param limit - Number of bicycles that are created and returns an arraylist of Bicycles.
     * @return  Returns an arraylist of Bike-object. Can also return NULL.
     */
    public static ArrayList<Bike> queryBikes(int bikestatus, int limit){
        if(bikestatus > 0 && bikestatus < 6){
            String sql = null;
            switch(bikestatus){
                case BIKES_ALL:
                    sql = "SELECT bicycle_id, station_id, battery_level, nr_trips, total_distance, lat, lon, el FROM bicycle NATURAL JOIN charging_station ORDER BY RAND() LIMIT ?;";
                    break;
                case BIKES_IN_USE:
                    sql = "SELECT bicycle_id, station_id, battery_level, nr_trips, total_distance, lat, lon, el FROM bicycle NATURAL JOIN charging_station WHERE station_id IS NULL AND atRepairStation IS FALSE ORDER BY RAND() LIMIT ?;";
                    break;
                case BIKES_CHARGING:
                    sql = "SELECT bicycle_id, station_id, battery_level, nr_trips, total_distance, lat, lon, el FROM bicycle NATURAL JOIN charging_station WHERE atRepairStation IS FALSE AND station_id IS NOT NULL ORDER BY RAND() LIMIT ?;";
                    break;
                case BIKES_AT_REPAIR:
                    sql = "SELECT bicycle_id, station_id, battery_level, nr_trips, total_distance, lat, lon, el FROM bicycle NATURAL JOIN charging_station WHERE atRepairStation = 1 ORDER BY RAND() LIMIT ?;";
                    break;
                case BIKES_DECOMISSIONED:
                    return null;
            }

            ArrayList<Bike> bikes = new ArrayList<>();
            Connection con = null;
            PreparedStatement stmt = null;
            ResultSet res = null;
            try{
                con = MysqlConnect.getConnection();
                stmt = con.prepareStatement(sql);
                stmt.setInt(1, limit);
                res = stmt.executeQuery();

                while(res.next()){
                    Bike a = new Bike(res.getInt(1), res.getInt(2), res.getDouble(3), res.getInt(4), res.getDouble(5), new GNSS(res.getDouble(6), res.getDouble(7), res.getDouble(8)));
                    bikes.add(a);
                }
            } catch (SQLException err) {
                MysqlConnect.rollback(con);
                err.printStackTrace();
            } finally {
                MysqlConnect.closeStatement(stmt);
                MysqlConnect.closeResSet(res);
                MysqlConnect.setAutoCommit(con);
                MysqlConnect.closeConnection(con);
            }
            return bikes;
        }else{
            return null;
        }
    }

    /**
     * This method runs a specific thread dedicated to get bicycles from the database table Bicycle.
     * @return a bicycle (Bike-object).
     */
    public static Bike getABike(){
        getter.run();
        return aBike;
    }
}
