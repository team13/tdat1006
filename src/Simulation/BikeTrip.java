package Simulation;

import DataEntities.Bike;
import DataEntities.ChargingStation;
import DataEntities.Member;
import DataEntities.MysqlConnect;
import Stuff.map.GNSS;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.*;


/**
 *  This class enables the simulation.java to simulate a bicycle trip.
 *  The class creates a bicycle trip with a Bike object.
 */

public class BikeTrip {
    private Bike bike;
    private ChargingStation goalStation;
    private int startStation;
    private double battery=100;
    private Member bikeUser;
    private double distance=0;
    private Instant start;
    private boolean hasStation=true;
    private boolean isAtGoal;
    private final double MOVE_DIST = 0.004;

    /**
     * Constructor that creates the object BikeTrip that simulates a
     * trip with a bicycle.
     * @param bike object from the class Bike.java.
     */
    public BikeTrip(Bike bike) {
        isAtGoal = false;
        this.bike = bike;
        this.bike.setBattery_level(100.0);
        this.startStation=bike.getStationid();
        this.goalStation = ChargingStation.getRandomStation();
        this.bikeUser=Member.getRandomMember();
    }

    /**
     * This method checks if a bicycle is at a station or not.
     * TRUE if at station and FALSE if not.
     * @return boolean variable TRUE/FALSE.
     */
    public boolean hasStation() {
        return hasStation;
    }

    /**
     * This method works as a get-method that returns
     * the boolean variable isAtGoal.
     * @return boolean variable TRUE/FALSE.
     */
    public boolean isAtGoal() {
        return isAtGoal;
    }

    /**
     *This method simulates the movement of a bicycle on the map.
     * It also updates the battery-level of the bicycle.
     */
    public void moveBikeTowardsGoal() {
        GNSS newPos = calculateNextPos();
        double deltaDistance = bike.getPosition().distance(newPos);
        bike.lowerBattery((deltaDistance / 1000.0));
        updateGps(newPos);
        checkIfAtGoal();


    }

    /**
     * Method used in the simulation to check if the bicycle is close to the simulation end-station.
     */
    public void checkIfAtGoal() {
        double dist = (bike.getPosition().distance(goalStation.getPosition()));
        isAtGoal = dist < 1000;
    }

    /**
     * This method was created with the use of the GNSS class to create a new position for the simulation-route
     * on the map.
     * @return new position for the bicycle.
     */
    private GNSS calculateNextPos() {
        double deltaLatitude = bike.getPosition().getLatitude() - goalStation.getPosition().getLatitude();
        double deltaLongitude = bike.getPosition().getLongitude() - goalStation.getPosition().getLongitude();
        double newLatitude = deltaLatitude > 0.0001 ? bike.getPosition().getLatitude() - MOVE_DIST : bike.getPosition().getLatitude() + MOVE_DIST;
        double newLongitude = deltaLongitude > 0.0001 ? bike.getPosition().getLongitude() - MOVE_DIST : bike.getPosition().getLongitude() + MOVE_DIST;


        GNSS newPos = new GNSS(newLatitude, newLongitude, bike.getPosition().getElevation());

        return newPos;
    }

    /**
     * This method removes a bicycle from a charging station.
     */
    public void checkoutBikeFromStart() {
        updateGps(bike.getPosition());
        distance = GNSS.distance(bike.getPosition(), goalStation.getPosition());
        start= Instant.now();
        removeCurrentStation();
        hasStation=false;
    }

    /**
     * This method inserts positions into the database table gps and updates
     * the battery-level of a bicycle using getBike_id() in the table bicycle.
     * @param pos is used to update the GPS coordinates in the database table gps.
     */
    private void updateGps(GNSS pos) {
        Connection con = null;
        PreparedStatement stmt = null;
        PreparedStatement stmt2 = null;

        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);

            String sql = "INSERT INTO gps (bicycle_id, date, lat, lon, el) VALUES (?, ?, ?, ?, ?);";
            String sql2 = "UPDATE bicycle SET battery_level=? WHERE bicycle_id=?";
            stmt = con.prepareStatement(sql);
            stmt2 = con.prepareStatement(sql2);
            stmt2.setDouble(1,bike.getBattery_level());
            stmt2.setInt(2,bike.getBike_id());


            stmt.setInt(1, bike.getBike_id());
            stmt.setString(2, getDateTime());
            stmt.setDouble(3, pos.getLatitude());
            stmt.setDouble(4, pos.getLongitude());
            stmt.setDouble(5, pos.getElevation());

//            Thread.sleep(2000);
            stmt2.execute();
            stmt.executeUpdate();
            con.commit();
        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            err.printStackTrace();
        } catch (Exception e){
        }finally{
            bike.setPosition(pos);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeConnection(con);
        }

    }

    /**
     * This method was created to get the current date so we could insert it into our database table gps.
     * @return current date.
     */
    public static String getDateTime() {
        LocalDateTime a = LocalDateTime.now();
        LocalDate b = a.toLocalDate();
        LocalTime c = a.toLocalTime().withNano(0);
        return b.toString() + " " + c.toString();
    }

    /**
     * This method disconnects the bicycle from a charging station and is used in checkoutBikeFromStart().
     */
    private void removeCurrentStation() {
        Connection con = null;
        PreparedStatement stmt = null;
        try {

            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "UPDATE bicycle SET station_id=NULL WHERE bicycle_id = ?;";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, bike.getBike_id());
            stmt.execute();


        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeConnection(con);
        }
    }

    /**
     * This method sets the bicycle at a charging station at the end the simulation route.
     */
    public void checkInAtGoal() {

        Connection con = null;
        PreparedStatement stmt = null;
        try {

            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "UPDATE bicycle SET station_id=?, total_distance=?,nr_trips=? WHERE bicycle_id = ?;";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, goalStation.getStation_id());
            stmt.setDouble(2, bike.getTotal_mileage()+distance);
            stmt.setInt(3, bike.getNr_trips()+1);
            stmt.setInt(4, bike.getBike_id());
//            Thread.sleep(2000);
            stmt.execute();


        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        } catch (Exception e){

        }finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeConnection(con);
        }
    }

    /**
     * This method archives every trip made from a bicycle.
     */
    public void archiveTrip(){

        Connection con = null;
        PreparedStatement stmt = null;
        try {

            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "INSERT INTO trip(startStation, endStation, batteryUsage, bike, member, dist, tripTime,date) VALUES (?,?,?,?,?,?,?,NOW()) ;";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, startStation);
            stmt.setInt(2, goalStation.getStation_id() );
            stmt.setDouble(3, 100-bike.getBattery_level());
            stmt.setInt(4, bike.getBike_id());
            stmt.setInt(5, bikeUser.getMemberID());
            stmt.setDouble(6, distance);
            stmt.setDouble(7, Duration.between(start,Instant.now()).getSeconds());
            Thread.sleep(2000);
            stmt.execute();


        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        } catch (Exception e){

        }finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeConnection(con);
        }
    }

    /**
     * This method checks if a charging station is full or not.
     * @return TRUE if full and FALSE if not.
     */
    public boolean goalIsFull(){
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet res =null;
        int count =0;
        boolean ok = false;
        try {

            con = MysqlConnect.getConnection();
            String sql = "SELECT count(*) from bicycle WHERE station_id=?";

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, goalStation.getStation_id());
            res = stmt.executeQuery();

            while (res.next()){
                count = res.getInt(1);
            }
            ok = (count >= goalStation.getMax_bikes());

        } catch (SQLException err) {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        } catch (Exception e){

        }finally {
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * This method selects a new charging station goal for the bike trip object.
     */
    public void findNewGoal(){
        this.goalStation = ChargingStation.getRandomStation();
    }


}
