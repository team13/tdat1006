package Simulation;

import DataEntities.Bike;

public class MyThread implements Runnable {
    private int threadNR;
    BikeTrip trip;
    Bike bike;

    /**
     * This method creates one thread that is run in the background of the simulation of the bicycles on the map.
     *
     * @param threadNR - Indicates which thread number this is.
     * @param bike     - Bicycle on trip. Every thread contains one bicycle.
     */
    public MyThread(int threadNR, Bike bike) {
        this.threadNR = threadNR;
        this.bike = bike;

    }

    /**
     * This method runs the whole simulation. The method is called by a thread scheduler.
     */
    @Override
    public void run() {
        if (trip == null) {
            trip = new BikeTrip(bike);
        } else if (trip.hasStation()) {
            trip.checkoutBikeFromStart();
        } else {
            trip.moveBikeTowardsGoal();
            if (trip.isAtGoal()) {
                if(trip.goalIsFull()){
                    trip.findNewGoal();
                }else {
                    trip.checkInAtGoal();
                    trip.archiveTrip();
                    bike = Simulation.getABike();
                    System.out.println("theadNr:" + threadNR + " got Bike NR:" + bike.getBike_id());
                    trip = null;
                }
            }
        }
    }
}
