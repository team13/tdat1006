package DataEntities;

import Stuff.ErrorMessages;
import Stuff.map.GNSS;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * This class handles all SQL queries towards the chargingstation table in our database.
 * By gathering the queries in one class, we managed to create a better, more
 * efficient  and tidy overview of our Administration-class (shortened length of code).
 *
 * This class uses following classes:
 * GNSS() - To specify the position of the charging station.
 */

public class ChargingStation {

    private int station_id;
    private String name;
    private String street_name;
    private String street_number;
    private int max_bikes;
    private int current_nr_bikes;
    private double power_draw;
    private GNSS position;
    private int postal;

    /**
     * These parameters intentifies specifications for a charging station. This contains
     * information like station_id, street address, maximum amount of bikes, current
     * number of stationed bikes, power usage and position (GPS).
     * @param station_id        first parameter. Indentifies the specific station by its ID.
     * @param name              second parameter. Identifies the unique name of the station.
     * @param street_name       third parameter. Is the name of the street where the chargingstation
     *                          is located.
     * @param street_number     fourth parameter. Is the housenumber of the address where the charging
     *                          station is located.
     * @param max_bikes         fifth parameter. Is the maximum number of bikes the specific charging
     *                          station are able to store.
     * @param current_nr_bikes  sixth parameter. Is the current number of bikes stored at the station.
     * @param power_draw        seventh parameter. Is the power-usage of the the charging station. This is based
     *                          on number of bikes stored at the station.
     * @param position          eighth parameter. Is the position of the charging station, given in GPS-cords.
     */

    public ChargingStation(int station_id, String name, String street_name, String street_number, int max_bikes, int current_nr_bikes, double power_draw, GNSS position) {
        if (name != null && current_nr_bikes <= max_bikes && street_name != null && position != null) {
            this.station_id = station_id;
            this.name = name;
            this.street_name = street_name;
            this.street_number = street_number;
            this.max_bikes = max_bikes;
            this.current_nr_bikes = current_nr_bikes;
            this.power_draw = power_draw;
            this.position = position;
        }
    }

    /**
     * Gathering information about a specific charging station (id, street address,
     * maximum number of storing capacity and GPS-cords), based on its station ID
     * (station_id).
     * @param station_id station ID.
     */

    public ChargingStation(int station_id) {
        PreparedStatement statement = null;
        Connection con = null;
        ResultSet res = null;
        try {
            con = MysqlConnect.getConnection();
            statement = con.prepareStatement("SELECT * FROM charging_station WHERE station_id = ?");
            statement.setInt(1, station_id);
            res = statement.executeQuery();

            while (res.next()) {
                this.station_id = res.getInt("station_id");
                name = res.getString("station_name");
                street_name = res.getString("street_name");
                street_number = res.getString("street_nr");
                postal = res.getInt("postnr");
                max_bikes = res.getInt("capacity");
                double x = res.getDouble("lat");
                double y = res.getDouble("lon");
                position = new GNSS(x, y, res.getDouble("el"));
            }
        } catch (Exception e) {
            ErrorMessages.errorDisplayErrorBox(e, "Could not poll Charging station data from the database.");
        } finally {
            MysqlConnect.closeStatement(statement);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
    }

    /**ChargingStation constructor for simplified ChargingStation object.
     *
     * @param station_id identifies this unique ChargingStation.
     * @param pos reference to a GNSS global positioning object.
     */

    public ChargingStation(int station_id, GNSS pos){
        if(pos != null){
            this.station_id = station_id;
            this.position = pos;
        }
    }

    /**
     * This method is used in the administration program to add a new model.
     * @param inputs gathers information gathered in "street name / number",
     *               "post number", "capacity" and "pos x / y" panels in the
     *               administration program. This will be added to the
     *               database.
     * @return a boolean to determine wether the process was successful or not.
     * @throws NumberFormatException Thrown to indicate that the application has
     *                               attempted to convert a string to one of the
     *                               numeric types, but that the string does not
     *                               have the appropriate format.
     */

    public static boolean insertChargingStationIntoDatabase(ArrayList<String> inputs) throws NumberFormatException {

        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;
        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "INSERT INTO charging_station (street_name, street_nr, postnr, capacity, lat, lon, el, vacancy, station_name) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
            stmt = con.prepareStatement(sql);

            stmt.setString(1, inputs.get(0));
            stmt.setString(2, inputs.get(1));
            stmt.setInt(3, Integer.parseInt(inputs.get(2)));
            stmt.setInt(4, Integer.parseInt(inputs.get(3)));
            stmt.setDouble(5, Double.parseDouble(inputs.get(4)));
            stmt.setDouble(6, Double.parseDouble(inputs.get(5)));
            stmt.setDouble(7, Double.parseDouble(inputs.get(6)));
            stmt.setInt(8, 0);
            stmt.setString(9, inputs.get(7));

            ok = (stmt.executeUpdate() != 0);
            if(ok){
                con.commit();
            }else{
                con.rollback();
            }
        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            ErrorMessages.errorDisplayErrorBox(err, "Could not insert Charging station into database.");
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return ok;

    }

    /**
     * This method returns a random chosen charging station. getRandomStation()
     * is used in the mapsimulation, to generate random destinations.
     * @return returns a object of Chargingstation, that contains the random
     *         chosen station.
     */

    public static ChargingStation getRandomStation() {
        ResultSet res = null;
        Connection con = null;
        PreparedStatement statement = null;

        ChargingStation station = null;
        try {
            con = MysqlConnect.getConnection();
            statement = con.prepareStatement("SELECT station_id FROM charging_station ORDER BY RAND() LIMIT 1");
            res = statement.executeQuery();

            while (res.next()) {
                station = new ChargingStation(res.getInt("station_id"));
            }

        } catch (Exception e) {
            ErrorMessages.errorDisplayErrorBox(e, "Could not receive random Charging station.");
        } finally {
            MysqlConnect.closeStatement(statement);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        return station;
    }

    /**
     *
     * This method is used in the administration program to edit a existing model.
     * @param inputs gathers information gathered in "street name / number",
     *               "post number", "capacity" and "pos x / y" panels in the
     *               administration program. This will be added to the
     *               database.
     * @param stationID is the station ID of the specific station that the user
     *                  wish to edit.
     * @return a boolean to determine wether the process was successful or not.
     * @throws NumberFormatException Thrown to indicate that the application has
     *                               attempted to convert a string to one of the
     *                               numeric types, but that the string does not
     *                               have the appropriate format.
     */

    public static boolean editChargingStationInDatabase (ArrayList < String > inputs,int stationID) throws NumberFormatException {

            Connection con = null;
            PreparedStatement stmt = null;
            boolean ok = false;
            try {

                con = MysqlConnect.getConnection();
                con.setAutoCommit(false);
                String sql = "UPDATE charging_station SET street_name = ?, street_nr = ?, postnr = ?, capacity = ?, lat = ?, lon = ?, el = ?, station_name = ? WHERE station_id = ?;";
                stmt = con.prepareStatement(sql);
                stmt.setInt(9, stationID);
                stmt.setString(1, inputs.get(0));
                stmt.setString(2, inputs.get(1));
                stmt.setInt(3, Integer.parseInt(inputs.get(2)));
                stmt.setInt(4, Integer.parseInt(inputs.get(3)));
                stmt.setDouble(5, Double.parseDouble(inputs.get(4)));
                stmt.setDouble(6, Double.parseDouble(inputs.get(5)));
                stmt.setDouble(7, Double.parseDouble(inputs.get(6)));
                stmt.setString(8, inputs.get(7));

                int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to edit Charging Station with ID " + stationID + "?");
                ok = (security == JOptionPane.YES_OPTION && stmt.executeUpdate() != 0);
                con.commit();
            } catch (SQLException err) {
                MysqlConnect.rollback(con);
                ok = false;
                showMessageDialog(null, ErrorMessages.failedEdit("Charging station"));
            } finally {
                MysqlConnect.setAutoCommit(con);
                MysqlConnect.closeStatement(stmt);
                MysqlConnect.closeConnection(con);
            }
            return ok;
        }

    /**
     * This method is used in the administration program to delete a existing .
     * @param stationId is the station ID of the specific station that the user
     *                  wish to delete.
     * @return a boolean to determine wether the process was successful or not.
     */

    public static boolean deleteChargingStationFromDatabase (int stationId){
            Connection con = null;
            PreparedStatement stmt = null;
            boolean ok = false;
            try {
                con = MysqlConnect.getConnection();
                con.setAutoCommit(false);
                String sql = "DELETE FROM charging_station WHERE station_id = ?;";
                stmt = con.prepareStatement(sql);
                stmt.setInt(1, stationId);

                int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to delete Charging Station with ID " + stationId + "?");
                ok = (security == JOptionPane.YES_OPTION && stmt.executeUpdate() != 0);
            } catch (SQLException err) {
                ok = false;
                MysqlConnect.rollback(con);
                showMessageDialog(null, ErrorMessages.failedDelete("Charging station"));
            } finally {
                MysqlConnect.setAutoCommit(con);
                MysqlConnect.closeStatement(stmt);
                MysqlConnect.closeConnection(con);
            }
            return ok;
        }

    /**
     * Get-method for the station ID.
     * @return returns the station ID, which is an int.
     */

    public int getStation_id () {
        return station_id;
    }

    /**
     * Get-method for the name of the charging station.
     * @return returns the name of the charging station, which is an String.
     */

    public String getName(){
        return name;
    }

    /**
     * Get-method for the street-name of the charging station.
     * @return returns the street-name of the charging station, which is an String.
     */

    public String getStreet_name () {
        return street_name;
    }

    /**
     * Get-method for the street-number of the charging station.
     * @return returns the street-number of the charging station.
     */

    public String getStreet_number () {
        return street_number;
    }

    /**
     * Get-method for the maximum number of bikes, that the station can hold.
     * @return returns the maximum number of bikes, which is an int.
     */

    public int getMax_bikes () {
        return max_bikes;
    }

    /**
     * Get-method for current nr of bikes, that is parked at the station.
     * @return returns the number of parked bikes, which is an int.
     */

    public int getCurrent_nr_bikes () {
        return current_nr_bikes;
    }

    /**
     * Get-method for the postal code of the station.
     * @return returns the postal code, which is an int.
     */

    public int getPostal () {
        return postal;
    }

    /**
     * Set-method for the name of the charging station.
     * @param name is the new name of the charging station.
     */

    public void setName (String name){
        if (name != null) {
            this.street_name = name;
        }
    }

    /**
     * Get-method for the position of the charging station.
     * @return returns the position of the charging station, which is an object
     * of the GNSS-class.
     */

    public GNSS getPosition () {
        return position;
    }

    /**
     * Used to set new position of the charging station.
     * @param newPos an object of GNSS, wich contains new GPS-cords.
     * @return a boolean to determine wether the process was successful or not.
     */

    public boolean setPosition (GNSS newPos){
        if (newPos != null) {
            this.position = newPos;
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method is made to change the station ID of a specific station.
     * @param station_id station ID for the specific station that the user
     *                   wish to change ID of.
     * @return a boolean to determine wether the process was successful or not.
     */

    public boolean setStationID (int station_id){
        boolean duplicate = true;
        Connection con = null;
        try {
            String sql = "SELECT station_id FROM charging_station WHERE station_id = " + station_id + ";";
            PreparedStatement stm = con.prepareStatement(sql);

            ResultSet res = stm.executeQuery();
            res.next();
            if (res.getInt("station_id") != station_id) {
                duplicate = false;
                this.station_id = station_id;
            }
            stm.close();
        } catch (SQLException e) {
            duplicate = true;
        }
        if (!duplicate) this.station_id = station_id;
        return duplicate;
    }
}
