package DataEntities;

import Stuff.ErrorMessages;
import Stuff.PasswordAuthentication;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

/**
 * This class handles all SQL queries towards the admin table in our database.
 * By gathering the queries in one class, we managed to create a better, more
 * efficient  and tidy overview of our Administration-class (shortened length of code).
 */

public class Admins {
    private int admin_id;
    private String firstname;
    private String lastname;
    private int birthyear;
    private String email;
    private int phone;
    private String username;

    /**
     * Gathering information about a specific admin (id, name, birthyear, email,
     * phone # and username), based on its model ID (modelID).
     *
     * @param adminID admin ID.
     */

    public Admins(int adminID) {
        ResultSet res = null;
        Connection con = null;
        PreparedStatement statement = null;

        try {
            con = MysqlConnect.getConnection();
            statement = con.prepareStatement("SELECT * FROM admin_users WHERE user_id = ?");
            statement.setInt(1, adminID);
            res = statement.executeQuery();

            while (res.next()) {
                admin_id = res.getInt("user_id");
                lastname = res.getString("surname");
                firstname = res.getString("firstname");
                birthyear = res.getInt("birthyear");
                email = res.getString("email");
                phone = res.getInt("phone");
                username = res.getString("username");

            }
        } catch (Exception e) {
            ErrorMessages.errorDisplayErrorBox(e, "Error!");
        } finally {
            MysqlConnect.closeStatement(statement);
            MysqlConnect.closeConnection(con);
        }

    }

    private static PasswordAuthentication pa = new PasswordAuthentication();

    /**
     * This method returns the full name of the currently signed in use
     * (based on the username). Method was found
     *
     * @param username username of the current signed in user.
     * @return full name of currently signed user.
     */

    public static String getCurrentUsersName(String username) {
        String fullName = "";
        MysqlConnect sql = new MysqlConnect();

        ResultSet res = null;
        PreparedStatement statement = null;
        Connection con = null;

        try {
            con = sql.getConnection();
            statement = con.prepareStatement("SELECT firstname, surname FROM admin_users WHERE username=?");
            statement.setString(1, username);
            res = statement.executeQuery();

            while (res.next()) {
                fullName += res.getString(1);
                fullName += " " + res.getString(2);
            }

        } catch (Exception e) {
            ErrorMessages.errorDisplayErrorBox(e, "There was en error when polling username from the database.");
        } finally {
            MysqlConnect.closeStatement(statement);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);

        }
        return fullName;
    }

    /**
     * This method provides functionality for changing password of current signed
     * in user (based on their username).
     *
     * @param inputs   gathers information entered in the "old password" and
     *                 "new password" panel in the administration program.
     *                 This will be added to the database.
     * @param userName username of the current signed in user.
     * @return a boolean to determine wether the process was successful or not.
     */

    public static boolean changePassword(ArrayList<String> inputs, String userName) {
        PasswordAuthentication pa = new PasswordAuthentication();
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;

        try {
            con = MysqlConnect.getConnection();
            String sql = "SELECT hashWithSalt FROM admin_users WHERE username = ?";     //5 er er Chr
            stmt = con.prepareStatement(sql);
            stmt.setString(1, userName);

            String oldPass = "";
            rs = stmt.executeQuery();
            while (rs.next()) {
                oldPass = rs.getString("hashWithSalt");
            }

            String enteredOldPass = inputs.get(0);

            if (pa.authenticate(enteredOldPass, oldPass)) {
                con.setAutoCommit(false);
                String sql1 = "UPDATE admin_users SET hashWithSalt = ? WHERE username = ?;";
                PreparedStatement stmt1 = con.prepareStatement(sql1);
                stmt1.setString(1, pa.hash(inputs.get(1)));
                stmt1.setString(2, userName);
                if (stmt1.executeUpdate() != 0) {
                    ok = true;
                    con.commit();
                }else{
                    con.rollback();
                }
            }
        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            ErrorMessages.errorDisplayErrorBox(err, "Could not change the password in the database.");
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(rs);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    public static void changePassword(String userName, String newPass) {
        PasswordAuthentication pa = new PasswordAuthentication();
        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;

        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);

            String sql1 = "UPDATE admin_users SET hashWithSalt = ? WHERE username = ?;";
            stmt = con.prepareStatement(sql1);
            stmt.setString(1, pa.hash(newPass));
            stmt.setString(2, userName);
            stmt.executeUpdate();
        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            ErrorMessages.errorDisplayErrorBox(err, "Could not update password for user " + userName);
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
    }


    /**
     * This method is used in the administration program to add a new admin-user.
     *
     * @param inputs gathers information entered in the "name", "birthyear", "email",
     *               "phone #", "username" and "password" panel in the
     *               administration program. This will be added to the database.
     * @return a boolean to determine wether the process was successful or not.
     * @throws NumberFormatException throws exception if an int is inserted where a string is expected. And vice versa.
     */

    public static boolean insertNewAdmin(ArrayList<String> inputs) throws NumberFormatException {

        MysqlConnect addBikeSQL = new MysqlConnect();
        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;
        try {
            con = addBikeSQL.getConnection();
            con.setAutoCommit(false);
            String sql = "INSERT INTO admin_users (surname, firstname, birthyear, email, phone, username, hashWithSalt) VALUES (?, ?, ?, ?, ?, ?, ?);";
            stmt = con.prepareStatement(sql);

            stmt.setString(1, inputs.get(0));
            stmt.setString(2, inputs.get(1));
            if (Integer.parseInt(inputs.get(2)) < 10000 && Integer.parseInt(inputs.get(2)) > 999){
                stmt.setInt(3, Integer.parseInt(inputs.get(2)));
            }else {
                throw new NumberFormatException();
            }
            stmt.setString(4, inputs.get(3));
            stmt.setInt(5, Integer.parseInt(inputs.get(4)));
            stmt.setString(6, inputs.get(5));
            stmt.setString(7, pa.hash(inputs.get(6)));

            ok = (stmt.executeUpdate() != 0);
        } catch(SQLIntegrityConstraintViolationException e) {
            MysqlConnect.rollback(con);
            ErrorMessages.errorDisplayErrorBox("Admin user was not added to the database.");
        } catch(SQLException err) {
            MysqlConnect.rollback(con);
            ErrorMessages.errorDisplayErrorBox(err, "There was an error when attempting to add admin user.");
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeConnection(con);
            MysqlConnect.closeStatement(stmt);
        }
        return ok;

    }

    /**
     * This method is used in the administration program to edit a existing admin-user.
     *
     * @param inputs   gathers information entered in the "name", "birthyear", "email",
     *                 "phone #" and "username" panel in the administration program.
     *                 This will be added to the database.
     * @param admin_id is the admin ID of the specific admin-user that the user
     *                 wish to edit.
     * @return a boolean to determine wether the process was successful or not.
     * @throws NumberFormatException throws exception if a string is inserted where an int is expected. And vice versa
     */

    public static boolean editAdminInDatabase(ArrayList<String> inputs, int admin_id) throws NumberFormatException {

        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;

        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);

            String sql = "UPDATE admin_users SET  firstname = ?,surname = ?, birthyear = ?, email = ?, phone = ?, username=? WHERE user_id = ?;";
            stmt = con.prepareStatement(sql);

            stmt.setString(1, inputs.get(0));
            stmt.setString(2, inputs.get(1));
            if (Integer.parseInt(inputs.get(2)) < 10000 && Integer.parseInt(inputs.get(2)) > 999){
            stmt.setInt(3, Integer.parseInt(inputs.get(2)));
            }else {
                throw new NumberFormatException();
            }
            stmt.setString(4, inputs.get(3));
            stmt.setString(5, inputs.get(4));
            stmt.setString(6, inputs.get(5));
            stmt.setInt(7, admin_id);


            int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to edit Admin user with ID " + admin_id + "?");
            ok = (security == JOptionPane.YES_OPTION && stmt.executeUpdate() != 0);
            con.commit();
        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            err.printStackTrace();
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * This method is used in the administration program to delete a existing existing admin-user.
     *
     * @param admin_id is the admin ID of the specific admin-user that the user
     *                 wish to delete.
     * @return a boolean to determine wether the process was successful or not.
     */

    public static boolean deleteAdminFromDatabase(int admin_id) {
        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;

        try {
            con = MysqlConnect.getConnection();
            String sql = "DELETE FROM admin_users WHERE user_id = ?;";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, admin_id);


            int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to delete admin user with ID " + admin_id + "?");
            ok = (security == JOptionPane.YES_OPTION && stmt.executeUpdate() != 0);
            System.out.println();
            System.out.println();
        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * Get-method for the admin ID.
     * @return returns the ID, which is an int.
     */

    public int getAdmin_id() {
        return admin_id;
    }

    /**
     * Get-method for the firstname.
     * @return returns the firstname, which is an String.
     */

    public String getFirstname() {
        return firstname;
    }

    /**
     * Get-method for the lastname.
     * @return returns the lastname, which is an String.
     */

    public String getLastname() {
        return lastname;
    }

    /**
     * Get-method for the birthyear.
     * @return returns the birthyear, which is an int.
     */

    public int getBirthyear() {
        return birthyear;
    }

    /**
     * Get-method for the e-mail.
     * @return returns the e-mail, which is an String.
     */

    public String getEmail() {
        return email;
    }

    /**
     * Get-method for the phone-number.
     * @return returns the phone-number, which is an int.
     */

    public int getPhone() {
        return phone;
    }

    /**
     * Get-method for the username.
     * @return returns the username, which is an String.
     */

    public String getUsername() {
        return username;
    }

    /**
     * This method is used to check if a specific username, is connected the representative
     * e-mail address.
     * @param username is the username of the respective user.
     * @param inputEmail is the e-mail of the respective user.
     * @return a boolean to determine wether the process was successful or not.
     */

    public static boolean checkEmail(String username, String inputEmail) {

        ResultSet res = null;
        PreparedStatement statement = null;
        Connection con = null;
        boolean ok = false;

        try {
            con = MysqlConnect.getConnection();
            statement = con.prepareStatement("SELECT email FROM admin_users WHERE username = ?;");
            statement.setString(1, username);
            res = statement.executeQuery();
            String email = "";

            while (res.next()) {
                email = res.getString(1);
            }
            ok = inputEmail.equals(email);
        } catch (Exception e) {
            ErrorMessages.errorDisplayErrorBox(e, "There was en error during database query.");
        } finally {
            MysqlConnect.closeStatement(statement);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }
}
