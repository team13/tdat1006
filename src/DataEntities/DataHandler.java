package DataEntities;

import Stuff.ErrorMessages;
import Stuff.PasswordAuthentication;
import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Vector;

/**
 * This class is a general class that handles and creating tables created in the
 * administration-program. By gathering these methods in one class, we managed to create
 * a better, more efficient  and tidy overview of our Administration-class (shortened
 * length of code).
 *
 * This class uses following classes:
 * PasswordAuthentication() - This method is used to hash password.
 */

public class DataHandler {
    private PasswordAuthentication passAuth = new PasswordAuthentication();

    /**
     * This method handles login to the administration program. A user is getting access
     * to the program, by checking the given username and password against whats registered
     * in the database.
     * @param uname username entered by the user.
     * @param pass password entered by the user.
     * @return a boolean to determine wether the process was successful or not.
     */

    public boolean login(String uname, String pass) {
        String correctpass = "";

        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        MysqlConnect mysql = new MysqlConnect();

        try {
            con = mysql.getConnection();
            stmt = con.prepareStatement("SELECT hashWithSalt FROM admin_users WHERE username = ?;");
            stmt.setString(1, uname);
            rs = stmt.executeQuery();

            while (rs.next()) {
                correctpass = rs.getString(1);
            }
        } catch (SQLException e) {
            ErrorMessages.errorDisplayErrorBox(e, "Unable to log in.\nThere was an error verifying user data in the database.");
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(rs);
            MysqlConnect.closeConnection(con);
        }
        if (passAuth.authenticate(pass, correctpass)) {
            return true;
        }
        return false;

    }

    /**
     * This method creates a JScrollPane, where you will be able to specify the header
     * and the content of it. The content will be implemented through SQL-query.
     * @param query is the SQL-query.
     * @param header is the header of the JScrollPane.
     * @return returns the JScrollPane that will be implemented in the administration-
     *         program.
     */

    public JScrollPane getScrollPane(String query, String[] header) {

        Connection con = null;
        ResultSet rs = null;
        Statement stmt = null;
        JScrollPane scrollPane = null;
        JTable table = null;
        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = query;
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            table = new JTable(buildTableModel(rs, header)) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
        } catch (SQLException e) {
            ErrorMessages.errorDisplayErrorBox(e, "Unable to fetch GUI component.\nThere was an error polling data from the database.");
        } finally {
            MysqlConnect.closeResSet(rs);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        scrollPane = new JScrollPane(table);
        scrollPane.setVisible(true);

        return scrollPane;
    }

    /**
     * This method is creating a DefaultTableModel, that is used as a general
     * method for creating tables in the administration program.  Using ResultSet
     * and a array for number of columns, we're able to create the tables exactly
     * they way they are required to be.
     * @param rs ResultSet.
     * @param cols array with number of columns.
     * @return returning the created table.
     * @throws SQLException Catching SQLException, which provides information on
     *                      database access error or other errors.
     */

    public static DefaultTableModel buildTableModel(ResultSet rs, String[] cols) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            try {
                columnNames.add(cols[column - 1]);
                //  System.out.println("Column " + column + ": " + metaData.getColumnName(column));

            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println(e);
            }
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
        return new DefaultTableModel(data, columnNames);

    }

    public static DefaultTableModel buildTableModel(ResultSet rs) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<String>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        while (rs.next()) {
            Vector<Object> vector = new Vector<Object>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
        return new DefaultTableModel(data, columnNames);
    }

    /**
     * This method converts a String to a Date-object.
     * @param date is the String that will be converted to the Date-object.
     * @return returns a Date-object which is used for registration in the database
     * @throws NumberFormatException Thrown to indicate that the application has
     *                               attempted to convert a string to one of the
     *                               numeric types, but that the string does not
     *                               have the appropriate format.
     */

    public static Date validDate(String date) throws NumberFormatException {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

        java.util.Date date1 = null;
        java.sql.Date sqlDate = null;
        try {
            date1 = sf.parse(date);
            sqlDate = new java.sql.Date(date1.getTime());
        } catch(ParseException pe) {
            throw new NumberFormatException();
        }
        return sqlDate;
    }

    /**
     * This method generates a random password.
     * @return returns the password, which is a String.
     */

    public static String getRandomPassword(){
        return Long.toHexString(Double.doubleToLongBits(Math.random()));
    }
}
