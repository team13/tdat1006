package DataEntities;

import Stuff.ErrorMessages;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * This class handles all SQL queries towards the manufacturer table in our database.
 * By gathering the queries in one class, we managed to create a better, more
 * efficient  and tidy overview of our Administration-class (shortened length of code).
 */

public class Manufacturer {
    private int manufID;
    private String name;
    private String country;

    /**
     * Gathering information about a specific manufacturer (id, name and country), based on its manufacturer ID (manufID)
     * @param manufID manufacturer ID
     */

    public Manufacturer(int manufID) {
        this.manufID = manufID;
        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = MysqlConnect.getConnection();
            String sql = "SELECT * FROM manufacturer WHERE make_id = ?;";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, manufID);
            res = stmt.executeQuery();

            while (res.next()) {
                name = res.getString("name");
                country = res.getString("country");
            }
        } catch (SQLException e) {
            ErrorMessages.errorDisplayErrorBox(e, "There was an error polling Manufacturer data from the database.");
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
    }

    /**
     * This method returns a array of the registered manufactures. The content of the array
     * depends on the information registered in the database (which is gathered from the SQL-
     * query), and is presented as a table in our administration program.
     * @return  will return the content from the manufactures table. If the table is empty, the
     *          method will return null.
     */

    public static String[] getManufacturerList() {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs_modellist = null;
        String[] choices;
        try {
            con = MysqlConnect.getConnection();
            String sql = "SELECT name FROM manufacturer";

            stmt = con.prepareStatement(sql);
            rs_modellist = stmt.executeQuery(sql);

            rs_modellist.last();
            choices = new String[rs_modellist.getRow()];

            rs_modellist.beforeFirst();

            int i = 0;
            while (rs_modellist.next()) {
                choices[i] = (rs_modellist.getString("name"));
                i++;
            }
        } catch (SQLException e) {
            choices = null;
            ErrorMessages.errorDisplayErrorBox(e, "There was an error polling Manufacturer data from the database.");
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(rs_modellist);
            MysqlConnect.closeConnection(con);
        }
        return choices;
    }

    /**
     * This method is used in the administration program to add a new manufacturer.
     * @param inputs gathers information entered in the "name" and "country" panel
     *               in the administration program. This will be added to the
     *               database.
     * @return  a boolean to determine wether the process was successful or not.
     */

    public static boolean addManufacturerToDatabase(ArrayList<String> inputs) {
        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;
        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "INSERT INTO manufacturer(name, country) VALUES (?, ?);";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, inputs.get(0));
            stmt.setString(2, inputs.get(1));

            ok = ((stmt.executeUpdate() != 0));
            con.commit();
        } catch (SQLException err) {
            ok = false;
            MysqlConnect.rollback(con);
            showMessageDialog(null, ErrorMessages.failedAdd("Manufacturer"));
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * This method is used in the administration program to edit a existing manufacturer.
     * @param inputs gathers information entered in the "name" and "country" panel
     *               in the administration program. This will be added to the
     *               database.
     * @param manifID is the manufacturer ID of the specific manufacturer that the user
     *                wish to edit.
     * @return a boolean to determine wether the process was successful or not.
     */

    public static boolean editManufacturerInDatabaes(ArrayList<String> inputs, int manifID) {
        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;
        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "UPDATE manufacturer SET name = ?, country = ? WHERE make_id= ?;";

            stmt = con.prepareStatement(sql);
            stmt.setString(1, inputs.get(0));
            stmt.setString(2, inputs.get(1));
            stmt.setInt(3, manifID);

            int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to edit manufacturer with ID " + manifID + "?");
            ok = (security == JOptionPane.YES_OPTION && stmt.executeUpdate() != 0);
            con.commit();
        } catch (SQLException err) {
            ok = false;
            MysqlConnect.rollback(con);
            showMessageDialog(null, ErrorMessages.failedEdit("Manufacturer"));
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * This method is used in the administration program to delete a existing manufacturer.
     * @param manufID is the manufacturer ID of the specific manufacturer that the user
     *                wish to delete.
     * @return a boolean to determine wether the process was successful or not.
     */

    public static boolean deleteManufacturerFromDatabase(int manufID){
        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;
        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "DELETE FROM manufacturer WHERE make_id = ?;";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, manufID);

            int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to delete manufacturer with ID " + manufID+ "?");
            ok = (security == JOptionPane.YES_OPTION && stmt.executeUpdate() != 0);
            con.commit();
        } catch (SQLException err) {
            ok = false;
            MysqlConnect.rollback(con);
            showMessageDialog(null, ErrorMessages.failedAdd("Manufacturer"));
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * Get-method for the manufacturer ID.
     * @return returns the manufacturer ID, which is an int.
     */

    public int getManufID() {
        return manufID;
    }

    /**
     * Get-method for the manufacturer name.
     * @return returns the name, which is a String.
     */

    public String getName() {
        return name;
    }

    /**
     * Get-method for the country where the manufacturer is produced.
     * @return returns the name of the country where the manufacturer is produced,
     *         which is a String.
     */

    public String getCountry() {
        return country;
    }
}
