package DataEntities;

import Stuff.ErrorMessages;
import Stuff.map.GNSS;

import javax.swing.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 *  This class creates an object bicycle and handles all SQL-queries towards the bicycle table in our database.
 *  By gathering the queries in one class, we managed to create a better, more
 *  efficient  and tidy overview of our Administration-class (shortened length of code).
 *
 *  This class uses the following classes:
 *  ChargingStation() - To specify what charging station the bicycles at.
 *  LocalDate() - To specify the purchase date of the bicycle
 *  GNSS() - To specify the position of the bicycle
 *
 */
public class Bike {

    private int bike_id;
    private int member_id;
    private String regnumber;
    private int nr_trips;
    private ChargingStation station;
    private double battery_level;
    private double total_mileage;
    private String manufacturer;
    private String model;
    private String colour;
    private LocalDate purchase_date;
    private boolean repairs;
    private final double MAX_BATTERY_LEVEL = 100.00;

    private GNSS position;

    /**
     * First constructor.
     * Creating a bicycle object with the following parameters:
     * @param bike_id - First param. Bicycle ID
     * @param member_id - Second param. Member ID
     * @param regnumber - Third param. Registration number of bicycle
     * @param nr_trips - Fourth param. Number of trips made by the bicycle.
     * @param station - Fifth param. Ties the bicycle to a charging station using CharginStation().
     * @param battery_level - Sixth param. Decimal number in percentage showing battery level of the bicycle.
     * @param total_mileage - Seventh param. Decimal number in kilometres showing the total mileage of the bicycle.
     * @param manufacturer - Eight param. Specifying the manufacturer of the bicycle.
     * @param model - Ninth param. Specifying the model of the bicycle.
     * @param colour - Tenth param. Specifying the colour of the bicycle.
     * @param purchase_date - Eleventh param. Specifying the purchase date of the bicycle.
     * @param repairs - Thirteenth param. Boolean true if the bicycle is on repair. False if not.
     * @param position - Fourteenth param. Gives the bicycle a position with the GNSS().
     * @throws IllegalArgumentException - Throws exception if battery level exceeds the variable MAX_BATTERY_LEVEL.
     */
    public Bike(int bike_id, int member_id, String regnumber, int nr_trips, ChargingStation station, double battery_level, double total_mileage, String manufacturer, String model, String colour, LocalDate purchase_date, boolean repairs, GNSS position) throws IllegalArgumentException {
        if (regnumber != null && battery_level <= 100.00 && purchase_date != null && position != null) {
            this.bike_id = bike_id;
            this.member_id = member_id;
            this.regnumber = regnumber;
            this.nr_trips = nr_trips;
            this.station = station;
            this.battery_level = battery_level;
            this.total_mileage = total_mileage;
            this.manufacturer = manufacturer;
            this.model = model;
            this.colour = colour;
            this.purchase_date = purchase_date;
            this.repairs = repairs;
            this.position = position;
        } else {
            throw new IllegalArgumentException("Argument double battery_level (" + String.valueOf(battery_level) + ") exceeds maximum allowed value of " + String.valueOf(MAX_BATTERY_LEVEL) + "% or argument LocalDate purchase_date or GNSS position refers to a null object.");
        }
    }

    /**
     * Second constructor.
     * Creating a bicycle with the following parameters:
     * @param bike_id - First param. Bicycle ID
     * @param regnumber - Second param. Registration number of bicycle
     * @param battery_level - Third param. Decimal number in percentage showing battery level of the bicycle.
     * @param total_mileage - Fourth param. Decimal number in kilometres showing the total mileage of the bicycle.
     * @param position - Fifth param. Gives the bicycle a position with the GNSS().
     * @throws IllegalArgumentException - Throws exception if battery level exceeds the variable MAX_BATTERY_LEVEL.
     */
    public Bike(int bike_id, String regnumber, double battery_level, double total_mileage, GNSS position) throws IllegalArgumentException {
        if (regnumber != null && battery_level <= 100.00 && position != null) {
            this.bike_id = bike_id;
            this.regnumber = regnumber;
            this.battery_level = battery_level;
            this.total_mileage = total_mileage;
            this.position = position;
        } else {
            throw new IllegalArgumentException("Argument double battery_level (" + String.valueOf(battery_level) + ") exceeds maximum allowed value of " + String.valueOf(MAX_BATTERY_LEVEL) + "% or argument GNSS position refers to a null object.");
        }
    }

    public Bike(int bike_id, String regnumber, int member_id, double battery_level, double total_mileage, int nr_trips,  GNSS position) throws IllegalArgumentException {
        if (regnumber != null && battery_level <= 100.00 && position != null) {
            this.bike_id = bike_id;
            this.regnumber = regnumber;
            this.battery_level = battery_level;
            this.total_mileage = total_mileage;
            this.position = position;
            this.member_id = member_id;
            this.nr_trips = nr_trips;
        } else {
            throw new IllegalArgumentException("Argument double battery_level (" + String.valueOf(battery_level) + ") exceeds maximum allowed value of " + String.valueOf(MAX_BATTERY_LEVEL) + "% or argument GNSS position refers to a null object.");
        }
    }

    /**
     * Third constructor.
     * Creating a bicycle with the following parameters:
     * @param bike_id - First parameter. Bicycle ID.
     * @param station_id - Second parameter. Charging station ID.
     * @param battery_level - Third parameter. Decimal number in percentage showing battery level of the bicycle.
     * @param nr_trips - Fourth parameter. Number of trips made by the bicycle.
     * @param total_mileage - Fifth parameter. Decimal number in kilometres showing the total mileage of the bicycle.
     * @param position - Sixth parameter. Gives the bicycle a position with the GNSS().
     * @throws IllegalArgumentException - Throws exception if battery level exceeds the variable MAX_BATTERY_LEVEL.
     */
    public Bike(int bike_id, int station_id, double battery_level, int nr_trips, double total_mileage, GNSS position) throws IllegalArgumentException {
        if (battery_level <= 100.00 && position != null) {
            this.bike_id = bike_id;
            this.station = null;
            this.battery_level = battery_level;
            this.nr_trips = nr_trips;
            this.total_mileage = total_mileage;
            this.position = position;
        } else {
            throw new IllegalArgumentException("Argument double battery_level (" + String.valueOf(battery_level) + ") exceeds maximum allowed value of " + String.valueOf(MAX_BATTERY_LEVEL) + "% or argument GNSS position refers to a null object.");
        }
    }

    /**
     * Fourth constructor.
     *
     * Gathering information about a specific Bike, based on the following parameter:
     * @param bike_id - Bicycle ID.
     *
     */
    public Bike(int bike_id) throws Exception {
        ResultSet res = null;
        Connection con = null;
        PreparedStatement statement = null;

        try {
            con = MysqlConnect.getConnection();
            statement = con.prepareStatement("SELECT * FROM bicycle LEFT JOIN model ON bicycle.model_id = model.model_id LEFT JOIN manufacturer m ON bicycle.make_id = m.make_id WHERE bicycle_id=?");
            statement.setInt(1, bike_id);
            res = statement.executeQuery();
            this.bike_id = bike_id;

            while (res.next()) {
                model = res.getString("model.name");
                manufacturer = res.getString("m.name");
                colour = res.getString("color");
                total_mileage = res.getDouble("total_distance");
                nr_trips = res.getInt("nr_trips");
                purchase_date = res.getDate("production_date").toLocalDate();
                regnumber = res.getString("registration_nr");
            }
        } catch (Exception e) {
            ErrorMessages.errorDisplayErrorBox(e, "There was en error when querying bike " + bike_id + " from the database.");
            throw new Exception();
        } finally {
            MysqlConnect.closeStatement(statement);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
    }


    /**
     * This method enables the Administration class to edit a bicycle in the database with an array of inputs you want to edit.
     * The method makes queries to update the bicycle where param bike_id = bike_id.
     *
     * The method returns the boolean param TRUE for success and FALSE if not.
     *
     * @param inputs - First param. This shows as a field in the client where you edit fields with their respective values.
     * @param bike_id - Second param. Specifies which bicycle you want to edit when the query to the bicycle database is executed.
     * @return boolean value TRUE or FALSE.
     * @throws NumberFormatException throws if there's a String where an Int is expected and vice versa.
     */

    public static boolean editBikeInDatabase(ArrayList<String> inputs, int bike_id) throws NumberFormatException {
        MysqlConnect editBikeSQL = new MysqlConnect();

        Connection con = null;
        PreparedStatement modelsql = null;
        PreparedStatement manufactSql = null;
        ResultSet modelIdRes = null;
        ResultSet manufIdRes = null;
        boolean ok = false;
        try {

            con = editBikeSQL.getConnection();
            con.setAutoCommit(false);
            String model = "SELECT model_id FROM model WHERE name = ?";
            String manufact = "SELECT make_id FROM  manufacturer WHERE name = ?";
            String sql = "UPDATE bicycle SET registration_nr = ?, color = ?, total_distance = ?, nr_trips = ?, make_id = ?, model_id = ?, production_date = ? WHERE bicycle_id = ?;";
            modelsql = con.prepareStatement(model);
            manufactSql = con.prepareStatement(manufact);

            manufactSql.setString(1, inputs.get(5));
            modelsql.setString(1, inputs.get(6));
            modelIdRes = modelsql.executeQuery();
            manufIdRes = manufactSql.executeQuery();

            int modelId = 0;
            int manufId = 0;

            while (modelIdRes.next()) {
                modelId = modelIdRes.getInt(1);
            }
            while (manufIdRes.next()) {
                manufId = manufIdRes.getInt(1);
            }

            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, inputs.get(0));
            stmt.setString(2, inputs.get(1));
            stmt.setDouble(3, Double.parseDouble(inputs.get(2)));
            stmt.setInt(4, Integer.parseInt(inputs.get(3)));
            stmt.setInt(5, manufId);
            stmt.setInt(6, modelId);
            stmt.setDate(7, DataHandler.validDate(inputs.get(4)));
            stmt.setInt(8, bike_id);

            int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to edit Bike with ID " + bike_id + "?");
            ok = security == JOptionPane.YES_OPTION && stmt.executeUpdate() != 0;
            if (ok) con.commit();
        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            ErrorMessages.errorDisplayErrorBox(err, "En error occurred when saving bike " + String.valueOf(bike_id) + " to the database. Changes were not saved.");
            ok = false;
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(manufactSql);
            MysqlConnect.closeStatement(modelsql);
            MysqlConnect.closeResSet(manufIdRes);
            MysqlConnect.closeResSet(modelIdRes);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * This method deletes a bicycle from the database bicycle where the param bike_id = bicycle_id.
     * @param bike_id - First param. Specifies which bicycle you want to delete when the query to the bicycle database is executed.
     * @return The method returns true if the bicycle is successfully deleted and false if not.
     */
    public static boolean deleteBikeFromDatabase(int bike_id) {
        Connection con = null;
        PreparedStatement stmt = null;
        Boolean status = false;
        try {
            con = MysqlConnect.getConnection();
            String sql = "DELETE FROM bicycle WHERE bicycle_id = ?;";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, bike_id);
            stmt.execute();
            status = true;
        } catch (SQLIntegrityConstraintViolationException e){
            status = false;
        } catch (SQLException err) {
            status = false;
            ErrorMessages.errorDisplayErrorBox(err, "An error occured during the database transaction.");
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return status;
    }

    /**
     * This method counts all bicycles in the database and is simply used as a counter.
     * @return total number of bicycles in the database.
     */
    public static int countAllBikes() {

        int numberOfBikes = 0;
        MysqlConnect sql = new MysqlConnect();
        ResultSet res = null;
        Connection con = null;
        PreparedStatement statement = null;

        try {
            con = sql.getConnection();
            statement = con.prepareStatement("SELECT COUNT(bicycle_id) FROM bicycle;");
            res = statement.executeQuery();

            while (res.next()) {
                numberOfBikes = res.getInt(1);
            }
        } catch (Exception e) {
            ErrorMessages.errorDisplayErrorBox(e, "There was an error when querying bikes from database.");
        } finally {
            MysqlConnect.closeStatement(statement);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        return numberOfBikes;
    }

    /**
     * This method puts a bicycle in the repair table and updates the atRepairStation value to TRUE in the bicycle table.
     *
     * The method is doing queries up against the database with the following parameters:
     * @param inputs - First param. This shows as a field in the client where you edit fields with their respective values.
     * @param bicycleId - Second param. Specifies which bicycle in the table bicycle that you want to repair.
     * @return The method return true if the bicycle is successfully inserted into the repair table.
     * @throws NumberFormatException throws if there's a String where an Int is expected and vice versa.
     */
    public static boolean sendBikeToRepair(ArrayList<String> inputs, int bicycleId) throws NumberFormatException {
        MysqlConnect mysql = new MysqlConnect();

        Connection con = null;
        PreparedStatement ws_sql = null;
        ResultSet wsIdRes = null;
        PreparedStatement stmt = null;
        PreparedStatement stmt1 = null;
        PreparedStatement repReason_sql=null;
        ResultSet repReason=null;
        boolean ok = false;
        try {
            con = mysql.getConnection();
            con.setAutoCommit(false);
            String ws = "SELECT workshop_id FROM workshop WHERE name = ?;";
            String SrepReason = "SELECT reptype_id FROM repair_type WHERE type=?";
            String sql = "UPDATE bicycle SET atRepairStation = TRUE, station_id = NULL WHERE bicycle_id = ?;";
            String sql1 = "INSERT INTO repair ( reason, est_return, est_cost, bicycle_id, workshop_id, comment) VALUES (?, ?, ?, ?, ?,?);";

            ws_sql = con.prepareStatement(ws);
            ws_sql.setString(1, inputs.get(3));
            repReason_sql = con.prepareStatement(SrepReason);
            repReason_sql.setString(1, inputs.get(4));
            wsIdRes = ws_sql.executeQuery();
            repReason = repReason_sql.executeQuery();
            int ws_id = 0;
            while (wsIdRes.next()) {
                ws_id = wsIdRes.getInt(1);
            }
            int repReason_id = 0;
            while (repReason.next()) {
                repReason_id = repReason.getInt(1);
            }

            stmt = con.prepareStatement(sql);
            stmt.setInt(1, bicycleId);

            stmt1 = con.prepareStatement(sql1);
            stmt1.setInt(1, repReason_id);
            stmt1.setDate(2, DataHandler.validDate(inputs.get(0)));
            stmt1.setInt(3, Integer.parseInt(inputs.get(1)));
            stmt1.setInt(4, bicycleId);
            stmt1.setInt(5, ws_id);
            stmt1.setString(6, inputs.get(2));

            int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to send Bike with ID " + bicycleId + " to repair-workshop?");
            ok = (security == JOptionPane.YES_OPTION && stmt.executeUpdate() != 0 && stmt1.executeUpdate() != 0);
            if (ok) con.commit();
        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            ErrorMessages.errorDisplayErrorBox(err, "Bike " + String.valueOf(bicycleId) + " was not sent to repair.");
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeStatement(stmt1);
            MysqlConnect.closeStatement(ws_sql);
            MysqlConnect.closeResSet(wsIdRes);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * This method deletes a bicycle from the repair table and updates the atRepairStation value to FALSE in the bicycle table.
     * @param bikeId - First param. Specifies which bicycle you want to return when the query to the bicycle and repair database is executed.
     * @return returns TRUE if the bicycle is successfully returned and FALSE if not.
     */
    public static boolean returnBikeFromRepair(int bikeId) {
        Connection con = null;
        PreparedStatement stmt = null;
        PreparedStatement stmt1 = null;
        boolean ok = false;

        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "DELETE FROM repair WHERE bicycle_id = ?;";
            String sql1 = "UPDATE bicycle SET station_id = 1, atRepairStation=FALSE WHERE bicycle_id = ?;";

            stmt = con.prepareStatement(sql);
            stmt1 = con.prepareStatement(sql1);
            stmt.setInt(1, bikeId);
            stmt1.setInt(1, bikeId);

            int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to return bike with ID " + bikeId + " back to eTramp HQ?");
            ok = (security == JOptionPane.YES_OPTION && stmt.executeUpdate() != 0 && stmt1.executeUpdate() != 0);
            con.commit();
        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            ErrorMessages.errorDisplayErrorBox(err, "There was an error in the database transaction when returning the bike from repairs.");
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeStatement(stmt1);
            MysqlConnect.closeConnection(con);

        }
        return ok;
    }

    public static ArrayList<JComboBox<String>> getJComboBoxesBikes() {
        String[] manufChoises = Manufacturer.getManufacturerList();
        JComboBox<String> mf_list = new JComboBox<String>(manufChoises);
        mf_list.setSelectedIndex(0);

        String[] models = Model.getModelsList((String) mf_list.getSelectedItem());
        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(models);
        JComboBox<String> model_list = new JComboBox<String>(models);
        model_list.setModel(model);

        mf_list.addItemListener(e -> {
            String[] newModels = Model.getModelsList((String) mf_list.getSelectedItem());
            DefaultComboBoxModel<String> model2 = new DefaultComboBoxModel<>(newModels);
            model_list.setModel(model2);
        });
        ArrayList<JComboBox<String>> comboBoxes = new ArrayList<>();
        comboBoxes.add(mf_list);
        comboBoxes.add(model_list);
        return comboBoxes;
    }

    /**
     * This method counts the number of bicycles currently at a repair station with a query against the bicycle table.
     * This method is simply used as a counter to show bicycles at repair.
     * @return returns an Int with bicycles currently at repair.
     */
    public static int countBikesAtRepair() {

        int bikesAtRepairStation = 0;
        MysqlConnect sql = new MysqlConnect();
        ResultSet res = null;
        Connection con = null;
        PreparedStatement statement = null;

        try {

            con = sql.getConnection();
            statement = con.prepareStatement("SELECT COUNT(*) FROM repair");
            res = statement.executeQuery();

            while (res.next()) {
                bikesAtRepairStation = res.getInt(1);
            }

        } catch (Exception e) {
            ErrorMessages.errorDisplayErrorBox(e, "There was an error in the database transaction when counting bikes.");
        } finally {
            MysqlConnect.closeStatement(statement);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        return bikesAtRepairStation;
    }


    /**
     * This method inserts a new bicycle in the bicycle table.
     * The method return a boolean value TRUE if the bicycle is successfully inserted into the table. FALSE if not.
     * @param inputs - First param. This shows as a field in the client where you edit fields with their respective values.
     * @return boolean value TRUE/FALSE.
     * @throws NumberFormatException throws if there's a String where an Int is expected and vice versa.
     */
    public static boolean insertBikeIntoDatabase(ArrayList<String> inputs) throws NumberFormatException {
        Connection con = null;
        PreparedStatement stmt = null;
        PreparedStatement getMakeID = null;
        PreparedStatement getMFId = null;
        ResultSet MFIDres = null;
        ResultSet MakeIdres = null;
        boolean ok = false;
        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "INSERT INTO bicycle (registration_nr, color, total_distance, nr_trips, production_date, make_id, model_id) VALUES (?, ?, ?, ?, ?, ?, ?);";
            stmt = con.prepareStatement(sql);
            getMakeID = con.prepareStatement("SELECT model_id FROM model WHERE name=?AND make=?");
            getMFId = con.prepareStatement("SELECT make_id FROM manufacturer WHERE name=?");
            getMFId.setString(1, inputs.get(5));
            getMakeID.setString(1, inputs.get(6));
            MFIDres = getMFId.executeQuery();

            stmt.setString(1, inputs.get(0));
            stmt.setString(2, inputs.get(1));
            stmt.setDouble(3, Double.parseDouble(inputs.get(2)));
            stmt.setInt(4, Integer.parseInt(inputs.get(3)));
            stmt.setDate(5, DataHandler.validDate(inputs.get(4)));

            int makeID = 0;
            int MFID = 0;
            while (MFIDres.next()) {
                MFID = MFIDres.getInt(1);
            }

            getMakeID.setInt(2, MFID);
            MakeIdres = getMakeID.executeQuery();
            while (MakeIdres.next()) {
                makeID = MakeIdres.getInt(1);
            }
            stmt.setInt(6, MFID);
            stmt.setInt(7, makeID);

            ok = (stmt.executeUpdate() != 0);
        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            ErrorMessages.errorDisplayErrorBox("Bike was NOT added to the database.");
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeResSet(MFIDres);
            MysqlConnect.closeResSet(MakeIdres);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeStatement(getMakeID);
            MysqlConnect.closeStatement(getMFId);
            MysqlConnect.closeConnection(con);
        }
        return ok;

    }

    /**
     * Get-method for bike ID.
     * @return returns the bike ID, which is an int.
     */

    public int getBike_id() {
        return bike_id;
    }

    /**
     * Get-method for the member ID.
     * @return returns the member ID, which is an int.
     */

    public int getMember_id() {
        return member_id;
    }

    /**
     * Get-method for the registration-number.
     * @return returns the registration-number, which is an String.
     */

    public String getRegnumber() {
        return regnumber;
    }

    /**
     * Get-method for the number of registered trips.
     * @return returns the number of trips, which is an int.
     */

    public int getNr_trips() {
        return nr_trips;
    }

    /**
     * Get-method for the a station.
     * @return returns an object of the class "ChargingStation".
     */

    public ChargingStation getStation() {
        return station;
    }

    /**
     * Get-method for the battery-level.
     * @return returns the battery-level, which is an double.
     */

    public double getBattery_level() {
        return battery_level;
    }

    /**
     * Get-method for the total distance travelled.
     * @return returns the total distance travelled, which is an double.
     */

    public double getTotal_mileage() {
        return total_mileage;
    }

    /**
     * Get-method for the manufacturer.
     * @return returns the manufacturer, which is an String.
     */

    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * Get-method for the model.
     * @return returns the model, which is an String.
     */

    public String getModel() {
        return model;
    }

    /**
     * Get-method for the color.
     * @return returns the color, which is an String.
     */

    public String getColour() {
        return colour;
    }

    /**
     * Get-method for the purchase date.
     * @return returns the purchase date, which is an object of LocalDate.
     */

    public LocalDate getPurchase_date() {
        return purchase_date;
    }

    /**
     * This method is checking if the bike is registered at a repair-station.
     * @return a boolean to determine wether the process was successful or not.
     */

    public boolean isRepairs() {
        return repairs;
    }

    /**
     * Get-method for the position of the bike.
     * @return returns the position of the bike, which is an object of the class GNSS.
     */

    public GNSS getPosition() {
        return position;
    }

    /**
     * Set-method for the member ID.
     * @param member_id is the new member ID, which the user wants to change to.
     */

    public void setMember_id(int member_id) {
        this.member_id = member_id;
    }

    /**
     * Set-method for the battery level of a bike.
     * @param newBattery is the new battery level, which the user wants to change to.
     */

    public void setBattery_level(double newBattery){
        if(newBattery <= 100.00 && newBattery >= 0.0){
            this.battery_level = newBattery;
        }
    }

    /**
     * This method lowers the battery level of a bike.
     * @param percentage is the percentage the user wants to lower the battery level of.
     */

    public void lowerBattery(double percentage){
        if(((this.battery_level - percentage) >= 0.0) && ((this.battery_level - percentage) <= 100.0)){
            this.battery_level -= percentage;
        }else if(this.battery_level - percentage > 100.0){
            this.battery_level = 100.0;
        }else{
            this.battery_level = 0.0;
        }
    }

    /**
     * Get-method for station ID.
     * @return returns the station ID, which is an int.
     */

    public int getStationid(){
        ResultSet res = null;
        Connection con = null;
        PreparedStatement statement = null;

        int id=0;
        try {
            con = MysqlConnect.getConnection();
            statement = con.prepareStatement("SELECT station_id FROM bicycle WHERE bicycle_id=?");
            statement.setInt(1,bike_id);
            res = statement.executeQuery();

            while (res.next()) {
                id = res.getInt("station_id");
            }

        } catch (Exception e) {
            ErrorMessages.errorDisplayErrorBox(e, "There was an error during the database transaction.");
        } finally {
            MysqlConnect.closeStatement(statement);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
            return id;
        }
    }

    /**
     * Set-method for the position of the bike.
     * @param position is the new position of the bike, which is an object of the GNSS-class.
     */

    public void setPosition(GNSS position) {
        this.position = position;
    }

    /**
     * Equals-method that checks if two objects are the same. Returns true if alike and false if not.
     * @param o - First param. Object you want to check.
     * @return return the boolean value TRUE/FALSE.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bike bike = (Bike) o;
        return bike_id == bike.bike_id &&
                Double.compare(bike.battery_level, battery_level) == 0 &&
                Double.compare(bike.total_mileage, total_mileage) == 0 &&
                repairs == bike.repairs &&
                Objects.equals(station, bike.station) &&
                Objects.equals(manufacturer, bike.manufacturer) &&
                Objects.equals(model, bike.model) &&
                Objects.equals(colour, bike.colour) &&
                Objects.equals(purchase_date, bike.purchase_date);
    }

    /**
     * Useful toString-method.
     * @return returns a String with the bike ID of a bike.
     */

    @Override
    public String toString() {
        return "Bike ID: " + bike_id;
    }
}