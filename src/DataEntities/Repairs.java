package DataEntities;

import Stuff.ErrorMessages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static javax.swing.JOptionPane.showMessageDialog;

public class Repairs {

    /**
     * Method that creates the repair type as an array.
     * @return a string array of all repair-types.
     */
    public static String[] getRepairReasons(){
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs_RepairList = null;
        String[] choices;
        try {
            con = MysqlConnect.getConnection();
            String sql = "SELECT type FROM repair_type;";

            stmt = con.prepareStatement(sql);
            rs_RepairList = stmt.executeQuery(sql);

            rs_RepairList.last();
            choices = new String[rs_RepairList.getRow()];
            rs_RepairList.beforeFirst();

            int i = 0;
            while (rs_RepairList.next()) {
                choices[i] = (rs_RepairList.getString("type"));
                i++;
            }
        } catch (SQLException err) {
            choices = null;
            showMessageDialog(null, ErrorMessages.failedDelete("Bike"));
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(rs_RepairList);
            MysqlConnect.closeConnection(con);
        }
        return choices;
    }
}
