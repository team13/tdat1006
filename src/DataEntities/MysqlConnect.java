package DataEntities;
import Stuff.ErrorMessages;

import java.sql.*;

/**
 * This class is a general class that are used for our MySQL-connection.
 * It contains frequently used methods for closing SQL-statements, rollback
 * and printing of error messages.
 */

public class MysqlConnect {

    private final static String sqlUrl = "jdbc:mysql://mysql.stud.iie.ntnu.no:3306/oyvinval?autoReconnect=false&useSSL=false";
    private final static String userName = "oyvinval";
    private final static String password = "jP4Bfm5g";

    /**
     * This method establishes connection to the database.
     * @return returns the Connection object, that establish connection
     *         to the database, based on sqlUrl, username and password
     *         (these are hardcoded in the program).
     * @throws SQLException An exception that provides information
     *                      on a database access error or other errors.
     */

    public static Connection getConnection() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return DriverManager.getConnection(sqlUrl, userName, password);
    }

    /**
     * This method is closing a ResultSet-object, based on a given ResaultSet.
     * @param res is the ResultSet that is wished to be closed.
     */

    public static void closeResSet(ResultSet res) {
        try {
            if (res != null) {
                res.close();
            }
        } catch (SQLException e) {
            ErrorMessages.errorDisplayErrorBox(e, "Failed closing ResultSet.");
        }
    }

    /**
     * This method is closing a Statement-object, based on a given Statement.
     * @param stm is the Statement that will be closed.
     */

    public static void closeStatement(Statement stm) {
        try {
            if (stm != null) {
                stm.close();
            }
        } catch (SQLException e) {
            ErrorMessages.errorDisplayErrorBox(e, "Failed closing Statement.");
        }
    }

    /**
     * This method is closing a Connection-object, based on a given Statement.
     * @param forbindelse is the Connection that will be closed.
     */

    public static void closeConnection(Connection forbindelse) {
        try {
            if (forbindelse != null) {
                forbindelse.close();
                // System.out.println("Forbindelse (" + forbindelse + ") kuttet. ");
            }
        } catch (SQLException e) {
            ErrorMessages.errorDisplayErrorBox(e, "Failed closing connection.");
        }
    }

    /**
     * This method performs a rollback on the database. This will therefore
     * undo partly completed database changes, when a database transaction
     * is determined to have failed.
     * @param forbindelse the SQL-connection that the rollback will be
     *                    performed on.
     */

    public static void rollback(Connection forbindelse) {
        try {
            if (forbindelse != null && !forbindelse.getAutoCommit()) {
                forbindelse.rollback();
            }
        } catch (SQLException e) {
            ErrorMessages.errorDisplayErrorBox(e, "Faield to rollback the database to a safe state.");
        }
    }

    /**
     * Sets AutoCommit to true.
     * @param forbindelse the SQL-connection that the action will be
     *                    performed on.
     */

    public static void setAutoCommit(Connection forbindelse) {
        try {
            if (forbindelse != null && !forbindelse.getAutoCommit()) {
                forbindelse.setAutoCommit(true);
            }
        } catch (SQLException e) {
            ErrorMessages.errorDisplayErrorBox(e, "Failed to enable the database auto-commit feature.");
        }
    }
}
