package DataEntities;

import Stuff.ErrorMessages;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * This class handles all SQL queries towards the workshop table in our database.
 * By gathering the queries in one class, we managed to create a better, more
 * efficient  and tidy overview of our Administration-class (shortened length of code).
 */

public class Workshops {

    private int workshopID;
    private String name;
    private String adress;
    private int phone;
    private String comments;

    /**
     * Gathering information about a specific workshop (id, name, phone # and comments), based
     * on its workshop ID (workshopId).
     * @param workshopId workshop ID.
     */

    public Workshops(int workshopId){
        this.workshopID=workshopId;
        Connection con = null;
        PreparedStatement statement = null;
        ResultSet res = null;
        try {
            con = MysqlConnect.getConnection();
            statement = con.prepareStatement("SELECT * FROM workshop WHERE workshop_id = ?;");
            statement.setInt(1, workshopId);
            res = statement.executeQuery();

            while (res.next()) {
                name = res.getString("name");
                adress = res.getString("adress");
                phone = res.getInt("phone");
                comments = res.getString("comments");
            }
        } catch (Exception e) {
            ErrorMessages.errorDisplayErrorBox("There was an internal error handling Workshop");
        } finally {
            MysqlConnect.closeStatement(statement);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
    }

    /**
     * This method returns a array of the registered workshops. The content of the array
     * depends on the information registered in the database (which is gathered from the SQL-
     * querie), and is presented as a table in our administration program.
     * @return will return the content from the workshop table. If the table is empty, the
     *         method will return null.
     */

    public static String[] getWorkshopList() {
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs_workshoplist = null;
        String[] choices = null;
        try {
            con = MysqlConnect.getConnection();
            String sql = "SELECT name FROM workshop;";

            stmt = con.prepareStatement(sql);
            rs_workshoplist = stmt.executeQuery(sql);

            rs_workshoplist.last();
            choices = new String[rs_workshoplist.getRow()];
            rs_workshoplist.beforeFirst();

            int i = 0;
            while (rs_workshoplist.next()) {
                choices[i] = (rs_workshoplist.getString("name"));
                i++;
            }
        } catch (SQLException err) {
            choices = null;
            showMessageDialog(null, ErrorMessages.failedDelete("Workshop"));
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(rs_workshoplist);
            MysqlConnect.closeConnection(con);
        }
        return choices;
    }

    /**
     * This method is used in the administration program to add a new workshop.
     * @param inputs gathers information entered in the "name", "adress", "phone #"
     *               and "comments" panel in the administration program. This will
     *               be added to the database.
     * @return a boolean to determine whether the process was successful or not.
     * @throws NumberFormatException throws exception if a string is inserted where an int is expected.
     */

    public static boolean AddWorkshopToDatabase(ArrayList<String> inputs) throws NumberFormatException{
        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;
        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "INSERT INTO workshop (name, adress, phone, comments) VALUES (?, ?, ?, ?);";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, inputs.get(0));
            stmt.setString(2, inputs.get(1));
            stmt.setInt(3, Integer.parseInt(inputs.get(2)));
            stmt.setString(4, inputs.get(3));

            ok = (stmt.executeUpdate() != 0);
            con.commit();
        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            ok = false;
            showMessageDialog(null, ErrorMessages.failedAdd("Workshop"));
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * This method is used in the administration program to edit a existing workshop.
     * @param inputs gathers information entered in the "name", "adress", "phone #"
     *               and "comments" panel in the administration program. This will
     *               be added to the database.
     * @param workshopID is the workshop ID of the specific workshop that the user
     *                   wish to edit.
     * @return a boolean to determine whether the process was successful or not.
     */

    public static boolean editWorkshopInDatabase(ArrayList<String> inputs,int workshopID){

        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;
        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "UPDATE workshop SET name = ?, adress = ?, phone = ?, comments = ? WHERE workshop_id = ?;";

            stmt = con.prepareStatement(sql);
            stmt.setString(1, inputs.get(0));
            stmt.setString(2, inputs.get(1));
            stmt.setInt(3, Integer.parseInt(inputs.get(2)));
            stmt.setString(4, inputs.get(3));
            stmt.setInt(5, workshopID);

            int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to edit Workshop with ID " + workshopID + "?");
            ok = (security == JOptionPane.YES_OPTION && stmt.executeUpdate() != 0);
            con.commit();
        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            ok = false;
            showMessageDialog(null, ErrorMessages.failedEdit("Workshop"));
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * This method is used in the administration program to delete a existing workshop.
     * @param workshopID is the model ID of the specific model that the user
     *                   wish to delete.
     * @return a boolean to determine whether the process was successful or not.
     */

    public static boolean deleteWorkshopFromDatabase(int workshopID){
        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;
        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "DELETE FROM workshop WHERE workshop_id = ?;";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, workshopID);

            int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to delete workshop with ID " +
                    workshopID + "?");
            ok = (security == JOptionPane.YES_OPTION && stmt.executeUpdate() != 0);
            con.commit();
        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            ok = false;
            showMessageDialog(null, ErrorMessages.failedDelete("Workshop"));
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * Get-method for the workshop ID.
     * @return returns the workshop ID, which is an int.
     */

    public int getWorkshopID() {
        return workshopID;
    }

    /**
     * Get-method for the workshop name.
     * @return returns the workshop name, which is an String.
     */

    public String getName() {
        return name;
    }

    /**
     * Get-method for the workshop address.
     * @return returns the workshop address, which is an String.
     */

    public String getAdress() {
        return adress;
    }

    /**
     * Get-method for the phone-number.
     * @return returns the phone-number, which is an int.
     */

    public int getPhone() {
        return phone;
    }

    /**
     * Get-method for comments.
     * @return returns the comments, which is an String.
     */

    public String getComments() {
        return comments;
    }
}
