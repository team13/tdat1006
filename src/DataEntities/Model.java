package DataEntities;

import Stuff.ErrorMessages;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

import static java.awt.image.ImageObserver.ERROR;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 * This class handles all SQL queries towards the model table in our database.
 * By gathering the queries in one class, we managed to create a better, more
 * efficient  and tidy overview of our Administration-class (shortened length of code).
 */

public class Model {

    private int modelID;
    private String name;
    private String manufacturer;
    private int manufacturerId;

    /**
     * Gathering information about a specific model (id, name and make), based on
     * its model ID (modelID).
     * @param modelID model ID.
     */

    public Model(int modelID){
        this.modelID = modelID;
        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;
        PreparedStatement stmt1 = null;
        try {
            con = MysqlConnect.getConnection();
            String sql = "SELECT * FROM model WHERE model_id= ?;";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, modelID);
            res = stmt.executeQuery();

            while (res.next()) {
                name = res.getString("name");
                manufacturerId = res.getInt("make");
            }
            String sql1 = "SELECT name FROM manufacturer WHERE make_id=?";
            stmt1 = con.prepareStatement(sql1);
            stmt1.setInt(1,manufacturerId);
            res=stmt1.executeQuery();
            while (res.next()){
                manufacturer = res.getString("name");
            }
        } catch (Exception e) {
            ErrorMessages.errorDisplayErrorBox(e, "There was en error polling model data from the database.");
        } finally {
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeStatement(stmt1);
            MysqlConnect.closeConnection(con);
        }

    }

    /**
     * This method returns a array of the registered models. The content of the array
     * depends on the information registered in the database (which is gathered from the SQL-
     * querie), and is presented as a table in our administration program.
     * @param selectedItem as available models are based on a specific manufacturer,
     *                     "selectedItem" will provide necessary information (manufacturer
     *                     name), to list the models that belong to the manufacturer.
     * @return will return the content from the models table. If the table is empty, the
     *         method will return null.
     */

    public static String[] getModelsList(String selectedItem) {

        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs_modellist = null;
        String[] choices;

        try {
            con = MysqlConnect.getConnection();
            String sql = "SELECT model.name FROM manufacturer LEFT JOIN model ON make_id = model.make WHERE manufacturer.name=?";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, selectedItem);
            rs_modellist = stmt.executeQuery();

            rs_modellist.last();
            choices = new String[rs_modellist.getRow()];

            rs_modellist.beforeFirst();

            int i = 0;
            while (rs_modellist.next()) {
                choices[i] = (rs_modellist.getString("name"));
                i++;
            }
            MysqlConnect.closeConnection(con);
        } catch (SQLException e) {
            choices = null;
            ErrorMessages.errorDisplayErrorBox(e, "There was an error polling model data from the database.");
        } finally {
            MysqlConnect.closeResSet(rs_modellist);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return choices;
    }

    /**
     * This method is used in the administration program to add a new model.
     * @param inputs gathers information entered in the "name" panel and a
     *               selected manufacturer from the scrolldown menu in the
     *               administration program. This will be added to the database.
     * @return  a boolean to determine wether the process was successful or not.
     */

    public static boolean insertModelIntoDatabase(ArrayList<String> inputs) {

        Connection con = null;
        PreparedStatement stmt = null;
        PreparedStatement stmt1 = null;
        ResultSet res = null;
        boolean ok = false;
        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "INSERT INTO model(name, make) VALUES(?,?)";
            String sql1 = "SELECT make_id FROM manufacturer WHERE name = ?";
            stmt = con.prepareStatement(sql);
            stmt1 = con.prepareStatement(sql1);

            stmt1.setString(1, inputs.get(1));
            res = stmt1.executeQuery();

            int manifId = 0;
            while (res.next()) {
                manifId = res.getInt(1);
            }
            stmt.setString(1, inputs.get(0));
            stmt.setInt(2, manifId);
            ok = (stmt.executeUpdate() != 0);
            con.commit();
        } catch (SQLException err) {
            ok = false;
            MysqlConnect.rollback(con);
            showMessageDialog(null, ErrorMessages.failedAdd("Model"));
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeStatement(stmt1);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        return ok;

    }

    /**
     * This method is used in the administration program to edit a existing model.
     * @param inputs gathers information entered in the "name" panel and a
     *               selected manufacturer from the scrolldown menu in the
     *               administration program. This will be added to the database.
     * @param modelID is the model ID of the specific model that the user
     *                wish to edit.
     * @return a boolean to determine wether the process was successful or not.
     */

    public static boolean editModelInDatabase(ArrayList<String> inputs, int modelID){
        Connection con = null;
        PreparedStatement stmt = null;
        PreparedStatement stmt1 = null;
        ResultSet res = null;

        boolean ok = false;
        try {
            con = MysqlConnect.getConnection();
            String sql = "UPDATE model SET name=?,make=? WHERE model_id=?";
            String sql1 = "SELECT make_id FROM manufacturer WHERE name=?";
            stmt = con.prepareStatement(sql);
            stmt1 = con.prepareStatement(sql1);

            stmt1.setString(1, inputs.get(1));
            res = stmt1.executeQuery();

            int manifId = 0;
            while (res.next()) {
                manifId = res.getInt(1);
            }

            stmt.setString(1, inputs.get(0));
            stmt.setInt(2, manifId);
            stmt.setInt(3, modelID);
            int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to edit member with ID " + modelID + "?");
            ok = (security == JOptionPane.YES_OPTION && (stmt.executeUpdate() != 0));
        } catch (SQLException err) {
            ok = false;
            MysqlConnect.rollback(con);
            showMessageDialog(null, ErrorMessages.failedEdit("Model"));
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeStatement(stmt1);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * This method is used in the administration program to delete a existing manufacturer.
     * @param modelID is the model ID of the specific model that the user
     *                wish to delete.
     * @return a boolean to determine wether the process was successful or not.
     */

    public static boolean deletModelFromDatabase(int modelID){
        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;
        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "DELETE FROM model WHERE model_id= ?;";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, modelID);

            int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to delete model with ID " + modelID + "?");
            ok = (security == JOptionPane.YES_OPTION && stmt.executeUpdate() != 0);
        } catch(SQLIntegrityConstraintViolationException e){
            ok = false;
            MysqlConnect.rollback(con);
            showMessageDialog(null, "A bicycle of this type is in use. Model cannot be deleted.");
        } catch (SQLException err) {
            ok = false;
            MysqlConnect.rollback(con);
            showMessageDialog(null, ErrorMessages.failedDelete("Model"));
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * Get-method to retrieve modelID.
     * @return modelID
     */
    public int getModelID() {
        return modelID;
    }

    /**
     * Get-method to retrieve name.
     * @return name.
     */
    public String getName() {
        return name;
    }

    /**
     * Get-method to retrieve manufacturer.
     * @return manufacturer.
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * Get-method to retrieve manufacturerID
     * @return manufacturerID.
     */
    public int getManufacturerId() {
        return manufacturerId;
    }
}
