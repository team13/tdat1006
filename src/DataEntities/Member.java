package DataEntities;

import Stuff.ErrorMessages;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * This class handles all SQL queries towards the member table in the database.
 * By gathering the queries in one class, we managed to create a better, more
 * efficient  and tidy overview of our Administration-class (shortened length of code).
 */
public class Member {
    private int memberID;
    private String firstname;
    private String lastname;
    private boolean isMale;
    private int birthyear;
    private String email;
    private int phone;

    /**
     * Constructor that make queries against the table member in the database
     * where param memberID = member_id in the table.
     *
     * @param memberID - Specifies which member ID to look for in the database.
     */
    public Member(int memberID) {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet res = null;
        this.memberID = memberID;
        try {
            con = MysqlConnect.getConnection();
            String sql = "SELECT * FROM member WHERE member_id = ?;";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, memberID);
            res = stmt.executeQuery();

            while (res.next()) {
                lastname = res.getString("surname");
                firstname = res.getString("firstname");
                birthyear = res.getInt("birthyear");
                email = res.getString("email");
                phone = res.getInt("phone");
                if(res.getString("gender").equals("M")){
                    isMale = true;
                }else {
                    isMale = false;
                }
            }
        } catch (Exception e) {
            ErrorMessages.errorDisplayErrorBox(e, "There was a problem polling Member data from the database.");
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
    }

    /**
     * This metod makes a query to the database table member
     * and returns a random member from the database.
     * @return random member from the database table member.
     */
     public static Member getRandomMember() {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet res = null;
        Member mem = null;
        try {
            con = MysqlConnect.getConnection();
            String sql = "SELECT member_id FROM member ORDER BY rand() LIMIT 1;";
            stmt = con.prepareStatement(sql);
            res = stmt.executeQuery();

            while (res.next()) {
                mem = new Member(res.getInt("member_id"));
            }
        } catch (Exception e) {
            mem = null;
            ErrorMessages.errorDisplayErrorBox(e, "There was an error polling Member data from the database.");
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
         return mem;
    }

    /**
     * This method simply just count all the members in the table member in the database.
     * @return Int with number of members.
     */
    public static int getNumberOfMembers() {
        int numberOfMembers = 0;
        MysqlConnect sql = new MysqlConnect();
        ResultSet res = null;
        Connection con = null;
        PreparedStatement statement = null;

        try {

            con = sql.getConnection();
            statement = con.prepareStatement("SELECT COUNT(member_id) FROM member;");
            res = statement.executeQuery();

            while (res.next()) {
                numberOfMembers = res.getInt(1);
            }
        } catch (Exception e) {
            numberOfMembers = -1;
            ErrorMessages.errorDisplayErrorBox(e, "There was an error polling Member data from the database.");
        } finally {
            MysqlConnect.closeStatement(statement);
            MysqlConnect.closeResSet(res);
            MysqlConnect.closeConnection(con);
        }
        return numberOfMembers;
    }

    /**
     * This method adds a new member to the table member in the database.
     * Returns TRUE if successfully inserted and FALSE if not.
     * @param inputs - First param. This shows as a field in the client where you edit fields with their respective values.
     * @return boolean value TRUE/FALSE
     * @throws NumberFormatException throws if there's a String where an Int is expected and vice versa.
     */
    public static boolean addMemberToDatabase(ArrayList<String> inputs) throws NumberFormatException {

        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;
        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "INSERT INTO member(firstname,surname, birthyear, email, phone,gender) VALUES (?, ?, ?, ?, ?, ?);";
            stmt = con.prepareStatement(sql);
            stmt.setString(1, inputs.get(0));
            stmt.setString(2, inputs.get(1));
            if (Integer.parseInt(inputs.get(2)) < 10000 && Integer.parseInt(inputs.get(2)) > 999) {
                stmt.setInt(3, Integer.parseInt((inputs.get(2))));
            } else {
                throw new NumberFormatException();
            }
            stmt.setString(4, inputs.get(3));
            stmt.setInt(5, Integer.parseInt(inputs.get(4)));
            if (inputs.get(5).equals("Male")) {
                stmt.setString(6, "M");
            }else {
                stmt.setString(6, "F");
            }
            ok = ((stmt.executeUpdate() != 0));
            con.commit();
        } catch (SQLException err) {
            MysqlConnect.rollback(con);
            ok = false;
            showMessageDialog(null, ErrorMessages.failedAdd("Member"));
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }


    /**
     * This method deletes a member from the table member in the database
     * where the param memberID = member_id. The method returns TRUE if
     * the member is successfully deleted and FALSE if not.
     * @param memberID - Specifies which member to delete.
     * @return boolean value TRUE/FALSE.
     */
    public static boolean deleteMemberFromDatabase(int memberID) {
        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;
        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "DELETE FROM member WHERE member_id = ?;";
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, memberID);

            int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to delete member with ID " + memberID + "?");
            ok = (security == JOptionPane.YES_OPTION && stmt.executeUpdate() != 0);
            con.commit();
        } catch (SQLException err) {
            ok = false;
            MysqlConnect.rollback(con);
            showMessageDialog(null, "Member");
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * This method is used to edit a member in the table member in the database.
     * param inputs shows as fields in the client where you can edit the information you want.
     * The method return the boolean value TRUE if successfully edited and FALSE if not.
     * @param inputs - First param. This shows as a field in the client where you edit fields with their respective values.
     * @param memberID - Second param. Specifies which member you want to edit in the table member in the database.
     * @return boolean value TRUE/FALSE.
     */
    public static boolean editMemberInDatabase(ArrayList<String> inputs, int memberID) {
        Connection con = null;
        PreparedStatement stmt = null;
        boolean ok = false;
        try {
            con = MysqlConnect.getConnection();
            con.setAutoCommit(false);
            String sql = "UPDATE member SET firstname = ?,surname = ?,  birthyear = ?, email = ?, phone = ?,gender = ? WHERE member_id = ?;";

            stmt = con.prepareStatement(sql);
            stmt.setString(1, inputs.get(0));
            stmt.setString(2, inputs.get(1));
            if (Integer.parseInt(inputs.get(2)) < 10000 && Integer.parseInt(inputs.get(2)) > 999) {
                stmt.setInt(3, Integer.parseInt((inputs.get(2))));
            }else {
                throw new NumberFormatException();
            }
            stmt.setString(4, inputs.get(3));
            stmt.setInt(5, Integer.parseInt(inputs.get(4)));
            if (inputs.get(5).equals("Male")) {
                stmt.setString(6, "M");
            }else {
                stmt.setString(6, "F");
            }
            stmt.setInt(7, memberID);

            int security = JOptionPane.showConfirmDialog(null, "Are you sure you would like to edit member with ID " + memberID + "?");
            ok = (security == JOptionPane.YES_OPTION && stmt.executeUpdate() != 0);
            con.commit();
        } catch (SQLException err) {
            ok = false;
            MysqlConnect.rollback(con);
            showMessageDialog(null, ErrorMessages.failedEdit("Member"));
        } finally {
            MysqlConnect.setAutoCommit(con);
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeConnection(con);
        }
        return ok;
    }

    /**
     * Get-method for the member ID.
     * @return returns the member ID, which is an int.
     */

    public int getMemberID() {
        return memberID;
    }

    /**
     * Get-method for the firstname.
     * @return returns the firstname, which is an String.
     */

    public String getFirstname() {
        return firstname;
    }

    /**
     * Get-method for the lastname.
     * @return returns the lastname, which is an String.
     */

    public String getLastname() {
        return lastname;
    }

    /**
     * This method is checking if the member is a male or not.
     * @return boolean value TRUE/FALSE.
     */

    public boolean isMale() {
        return isMale;
    }

    /**
     * Get-method for the birthyear.
     * @return returns the birthyear, which is an int.
     */

    public int getBirthyear() {
        return birthyear;
    }

    /**
     * Get-method for the e-mail.
     * @return returns the e-mail, which is an String.
     */

    public String getEmail() {
        return email;
    }

    /**
     * Get-method for the phone-number.
     * @return returns the phone-number, which is an int.
     */

    public int getPhone() {
        return phone;
    }
}
