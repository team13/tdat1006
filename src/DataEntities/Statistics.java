package DataEntities;

// import Stuff.Simulation;
import Stuff.ErrorMessages;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.awt.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.jfree.chart.axis.*;
import org.jfree.data.general.DefaultPieDataset;

/**
 *  This class generates the statistics for out Administration-program. We are gathering
 *  data through SQL-queries towards our database. By gathering the queries in one class,
 *  we managed to create a better, more efficient and tidy overview of our
 *  Administration-class (shortened length of code).
 */

public class Statistics extends JPanel {

    private ChartPanel chartPanel = null;
    private Connection con = null;
    private Statement stmt = null;
    private ResultSet rs = null;
    private MysqlConnect mysql = new MysqlConnect();

    private final Color poll_background = new Color(204, 204, 255);


    private final String month1 = "Jan 17";
    private final String month2 = "Feb 17";
    private final String month3 = "Mar 17";
    private final String month4 = "Apr 17";
    private final String month5 = "May 17";
    private final String month6 = "Jun 17";
    private final String month7 = "Jul 17";
    private final String month8 = "Aug 17";
    private final String month9 = "Sep 17";
    private final String month10 = "Oct 17";
    private final String month11 = "Nov 17";
    private final String month12 = "Dec 18";
    private final String month13 = "Jan 18";
    private final String month14 = "Feb 18";
    private final String month15 = "Mar 18";

    /**
     * First constructor.
     * Creates a statistics object with the following parameteres:
     *
     * @param applicationTitle - First param. Title of the application
     * @param chartTitle - Second param. Title of the specific chart
     * @param chart - Third param. Specifies the chart that will be implemented
     */

    public Statistics( String applicationTitle , String chartTitle, String chart ) {
        JFreeChart barChart = null;
        final CategoryPlot plot;


        if (chart.equals("demographics")) {
            barChart = ChartFactory.createBarChart(chartTitle,"Age groups","Amount", statsGenderAgeDemographics(), PlotOrientation.VERTICAL,true, true, false);
            chartPanel = new ChartPanel( barChart );

            plot = barChart.getCategoryPlot();
            plot.setBackgroundPaint(Color.lightGray);
            plot.setBackgroundPaint(poll_background);
            plot.setDomainGridlinePaint(Color.DARK_GRAY);
            plot.setRangeGridlinePaint(Color.DARK_GRAY);
        }

        else if (chart.equals("repaircost")) {
            barChart = ChartFactory.createBarChart(chartTitle,"Month","Amount", statsRepairCosts(), PlotOrientation.VERTICAL,true, true, false);
            chartPanel = new ChartPanel( barChart );

            plot = barChart.getCategoryPlot();
            plot.setBackgroundPaint(Color.lightGray);
            plot.setBackgroundPaint(poll_background);
            plot.setDomainGridlinePaint(Color.DARK_GRAY);
            plot.setRangeGridlinePaint(Color.DARK_GRAY);
        }

        else if (chart.equals("repairreason")) {
            barChart = ChartFactory.createBarChart(chartTitle,"Reason","Amount", statsRepairReason(), PlotOrientation.VERTICAL,true, true, false);
            chartPanel = new ChartPanel( barChart );

            plot = barChart.getCategoryPlot();
            plot.setBackgroundPaint(Color.lightGray);
            plot.setBackgroundPaint(poll_background);
            plot.setDomainGridlinePaint(Color.DARK_GRAY);
            plot.setRangeGridlinePaint(Color.DARK_GRAY);
        }

        else if (chart.equals("powerusage")) {

            JFreeChart lineChart = ChartFactory.createLineChart(chartTitle, "Power Usage 2017","Kilowatt Hours", statsPowerUsage(), PlotOrientation.VERTICAL, false,false,false);
            chartPanel = new ChartPanel( lineChart );

            plot = lineChart.getCategoryPlot ();
            plot.setBackgroundPaint(Color.lightGray);
            NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
            rangeAxis.setRange (1360, 1440);
            plot.setBackgroundPaint(poll_background);
            plot.setDomainGridlinePaint(Color.DARK_GRAY);
            plot.setRangeGridlinePaint(Color.DARK_GRAY);

            chartPanel.setMouseZoomable( true , false );
        }

        else if (chart.equals("renters")) {
            barChart = ChartFactory.createBarChart(chartTitle,"Age groups","Number of bikes", numberOfRentersInAgeGroups(), PlotOrientation.VERTICAL,true, true, false);
            chartPanel = new ChartPanel(barChart);

            plot = barChart.getCategoryPlot();
            plot.setBackgroundPaint(Color.lightGray);
            plot.setBackgroundPaint(poll_background);
            plot.setDomainGridlinePaint(Color.DARK_GRAY);
            plot.setRangeGridlinePaint(Color.DARK_GRAY);
        }

        else if (chart.equals("powerrepaircosts")) {
            barChart = ChartFactory.createBarChart(chartTitle,"Repair and Power costs 2017","Price in NOK", statsRepairPowerCosts(), PlotOrientation.VERTICAL,true, true, false);
            chartPanel = new ChartPanel(barChart);

            plot = barChart.getCategoryPlot();
            plot.setBackgroundPaint(Color.lightGray);
            plot.setBackgroundPaint(poll_background);
            plot.setDomainGridlinePaint(Color.DARK_GRAY);
            plot.setRangeGridlinePaint(Color.DARK_GRAY);
        }


        chartPanel.setPreferredSize(new java.awt.Dimension( 700 , 367 ) );

        add(chartPanel);
        setLayout(new GridLayout(1,1));

    }

    /**
     * This method creates a statistic for all the repair-expenses throughout the months in 2017
     * @return returns and array that contains all the gathered information
     */

    public int[] getRepairCostsArray() {
        int[] repairCost = new int[15];

        try {
            con = MysqlConnect.getConnection();
            String sql = "" +
                    "SELECT SUM(cost) AS cost FROM repair_history WHERE MONTH(repairstart) = '1' AND YEAR(repairstart) = 2017\n" +
                    "UNION\n" +
                    "SELECT SUM(cost) AS cost FROM repair_history WHERE MONTH(repairstart) = '2' AND YEAR(repairstart) = 2017\n" +
                    "UNION\n" +
                    "SELECT SUM(cost) AS cost FROM repair_history WHERE MONTH(repairstart) = '3' AND YEAR(repairstart) = 2017\n" +
                    "UNION\n" +
                    "SELECT SUM(cost) AS cost FROM repair_history WHERE MONTH(repairstart) = '4' AND YEAR(repairstart) = 2017\n" +
                    "UNION\n" +
                    "SELECT SUM(cost) AS cost FROM repair_history WHERE MONTH(repairstart) = '5' AND YEAR(repairstart) = 2017\n" +
                    "UNION\n" +
                    "SELECT SUM(cost) AS cost FROM repair_history WHERE MONTH(repairstart) = '6' AND YEAR(repairstart) = 2017\n" +
                    "UNION\n" +
                    "SELECT SUM(cost) AS cost FROM repair_history WHERE MONTH(repairstart) = '7' AND YEAR(repairstart) = 2017\n" +
                    "UNION\n" +
                    "SELECT SUM(cost) AS cost FROM repair_history WHERE MONTH(repairstart) = '8' AND YEAR(repairstart) = 2017\n" +
                    "UNION\n" +
                    "SELECT SUM(cost) AS cost FROM repair_history WHERE MONTH(repairstart) = '9' AND YEAR(repairstart) = 2017\n" +
                    "UNION\n" +
                    "SELECT SUM(cost) AS cost FROM repair_history WHERE MONTH(repairstart) = '10' AND YEAR(repairstart) = 2017\n" +
                    "UNION\n" +
                    "SELECT SUM(cost) AS cost FROM repair_history WHERE MONTH(repairstart) = '11' AND YEAR(repairstart) = 2017\n" +
                    "UNION\n" +
                    "SELECT SUM(cost) AS cost FROM repair_history WHERE MONTH(repairstart) = '12' AND YEAR(repairstart) = 2017\n";

            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);

            int counter = 0;

            while (rs.next()) {
                repairCost[counter] = (rs.getInt("cost"));
                counter++;
            }
        } catch (SQLException e) {
            repairCost = null;
            ErrorMessages.errorDisplayErrorBox(e, "There was an error when polling statistics data from the database.");
        } finally {
                MysqlConnect.closeStatement(stmt);
                MysqlConnect.closeResSet(rs);
                MysqlConnect.closeConnection(con);
        }
        return repairCost;
    }

    /**
     * This method creates an array of the power usage of every bicycle for every month of the year.
     * @return the power usage of the bicycles in KiloWatt Hours (KW/H).
     */
    public int[] getPowerUsageArray() {
        int[] powerUsages = new int[16];

        try {
            con = MysqlConnect.getConnection();
            String sql = "" +
                    "SELECT SUM(100-batteryUsage) as bUse FROM trip WHERE MONTH(date) = '1' AND YEAR(date) = '2017'\n" +
                    "UNION\n" +
                    "SELECT SUM(100-batteryUsage) as bUse FROM trip WHERE MONTH(date) = '2' AND YEAR(date) = '2017'\n" +
                    "UNION\n" +
                    "SELECT SUM(100-batteryUsage) as bUse FROM trip WHERE MONTH(date) = '3' AND YEAR(date) = '2017'\n" +
                    "UNION\n" +
                    "SELECT SUM(100-batteryUsage) as bUse FROM trip WHERE MONTH(date) = '4' AND YEAR(date) = '2017'\n" +
                    "UNION\n" +
                    "SELECT SUM(100-batteryUsage) as bUse FROM trip WHERE MONTH(date) = '5' AND YEAR(date) = '2017'\n" +
                    "UNION\n" +
                    "SELECT SUM(100-batteryUsage) as bUse FROM trip WHERE MONTH(date) = '6' AND YEAR(date) = '2017'\n" +
                    "UNION\n" +
                    "SELECT SUM(100-batteryUsage) as bUse FROM trip WHERE MONTH(date) = '7' AND YEAR(date) = '2017'\n" +
                    "UNION\n" +
                    "SELECT SUM(100-batteryUsage) as bUse FROM trip WHERE MONTH(date) = '8' AND YEAR(date) = '2017'\n" +
                    "UNION\n" +
                    "SELECT SUM(100-batteryUsage) as bUse FROM trip WHERE MONTH(date) = '9' AND YEAR(date) = '2017'\n" +
                    "UNION\n" +
                    "SELECT SUM(100-batteryUsage) as bUse FROM trip WHERE MONTH(date) = '10' AND YEAR(date) = '2017'\n" +
                    "UNION\n" +
                    "SELECT SUM(100-batteryUsage) as bUse FROM trip WHERE MONTH(date) = '11' AND YEAR(date) = '2017'\n" +
                    "UNION\n" +
                    "SELECT SUM(100-batteryUsage) as bUse FROM trip WHERE MONTH(date) = '12' AND YEAR(date) = '2017'\n" +
                    "";

            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);

            int counter = 0;

            while (rs.next()) {
                int powerUsage = 0;
                int batteryVoltage = 36; // Volts
                int batteryCapcity = 17; // Ampere hours.

                powerUsage = (rs.getInt("bUse") / 100) * (batteryVoltage * batteryCapcity); // WattHours
                powerUsage = powerUsage / 1000; // Change to KiloWatt Hours
                powerUsages[counter] = (powerUsage);
                counter++;
            }
        } catch (SQLException e) {
            powerUsages = null;
            ErrorMessages.errorDisplayErrorBox(e, "There was an error polling power useage data from the database.");
        } finally {
                MysqlConnect.closeStatement(stmt);
                MysqlConnect.closeResSet(rs);
                MysqlConnect.closeConnection(con);
        }
        return powerUsages;
    }

    /**
     * This method creates a dataset for every month with the use of the method getPowerUsageArray().
     * @return an DefaultCategoryDataset of every month with their own set of power usage.
     */
    public CategoryDataset statsPowerUsage() {

        int[] powerUsages = getPowerUsageArray();

        final DefaultCategoryDataset dataset = new DefaultCategoryDataset( );

        dataset.addValue(powerUsages[0], "MJ", month1);
        dataset.addValue(powerUsages[1], "MJ", month2);
        dataset.addValue(powerUsages[2], "MJ", month3);
        dataset.addValue(powerUsages[3], "MJ", month4);
        dataset.addValue(powerUsages[4], "MJ", month5);
        dataset.addValue(powerUsages[5], "MJ", month6);
        dataset.addValue(powerUsages[6], "MJ", month7);
        dataset.addValue(powerUsages[7], "MJ", month8);
        dataset.addValue(powerUsages[8], "MJ", month9);
        dataset.addValue(powerUsages[9], "MJ", month10);
        dataset.addValue(powerUsages[10], "MJ", month11);
        dataset.addValue(powerUsages[11], "MJ", month12);

        return dataset;

    }

    /**
     * This method creates a statistic for the specific repair reasons which has occurred
     * throughout 2017
     * @return returns a "CategoryDataset" which is used for the statistics
     */

    public CategoryDataset statsRepairReason() {

        final DefaultCategoryDataset  dataset = new DefaultCategoryDataset( );

        dataset.addValue(22, "Changing tires", "");
        dataset.addValue(28, "LED/Display panel", "");
        dataset.addValue(27, "Motor inhibitor", "");
        dataset.addValue(24, "Throttle", "");
        dataset.addValue(20, "Pedals", "");
        dataset.addValue(30, "Pedal assist/magnetic disc", "");
        dataset.addValue(23, "Faulty wiring", "");
        dataset.addValue(20, "Poor assembly issues", "");
        dataset.addValue(30, "Frame damage", "");
        dataset.addValue(23, "Water damage", "");
        dataset.addValue(22, "Battery: blown fuse", "");
        dataset.addValue(25, "Battery: low voltage", "");
        dataset.addValue(8, "Electrical engine: Weird noise", "");
        dataset.addValue(5, "Electrical engine: Damage", "");


        return dataset;

    }

    /**
     * This method creates a statistic for the powercosts as a result of the performed
     * repairs
     * @return returns a "CategoryDataset" which is used for the statistics
     */

    public CategoryDataset statsRepairPowerCosts() {

        int[] repairCost = getRepairCostsArray();
        int[] powerUsages = getPowerUsageArray();
        double priceKWH = 50.19;
        double[] totalCost  = new double[12];

        for (int i = 0; i < 12; i++) {
            totalCost[i] = repairCost[i] + (powerUsages[i] * priceKWH);
        }

        final DefaultCategoryDataset dataset = new DefaultCategoryDataset( );

        dataset.addValue(totalCost[0], "Total cost", month1);
        dataset.addValue(totalCost[1], "Total cost", month2);
        dataset.addValue(totalCost[2], "Total cost", month3);
        dataset.addValue(totalCost[3], "Total cost", month4);
        dataset.addValue(totalCost[4], "Total cost", month5);
        dataset.addValue(totalCost[5], "Total cost", month6);
        dataset.addValue(totalCost[6], "Total cost", month7);
        dataset.addValue(totalCost[7], "Total cost", month8);
        dataset.addValue(totalCost[8], "Total cost", month9);
        dataset.addValue(totalCost[9], "Total cost", month10);
        dataset.addValue(totalCost[10], "Total cost", month11);
        dataset.addValue(totalCost[11], "Total cost", month12);


        return dataset;

    }

    /**
     *This method created an array with the method getRepairCostsArray().
     * @return a dataset of the total repair costs for every month.
     */
    public CategoryDataset statsRepairCosts() {

        int[] repairCost = getRepairCostsArray();

        final DefaultCategoryDataset dataset = new DefaultCategoryDataset( );

        dataset.addValue(repairCost[0], "total cost", month1);
        dataset.addValue(repairCost[1], "total cost", month2);
        dataset.addValue(repairCost[2], "total cost", month3);
        dataset.addValue(repairCost[3], "total cost", month4);
        dataset.addValue(repairCost[4], "total cost", month5);
        dataset.addValue(repairCost[5], "total cost", month6);
        dataset.addValue(repairCost[6], "total cost", month7);
        dataset.addValue(repairCost[7], "total cost", month8);
        dataset.addValue(repairCost[8], "total cost", month9);
        dataset.addValue(repairCost[9], "total cost", month10);
        dataset.addValue(repairCost[10], "total cost", month11);
        dataset.addValue(repairCost[11], "total cost", month12);


        return dataset;

    }

    /**
     * This method creates a statistic for bikes-users, partitioned on gender and birthyear
     * @return returns a "CategoryDataset" which is used for the statistics
     */

    public CategoryDataset statsGenderAgeDemographics() {


        int[] demographic = new int[18];

        try {

            con = MysqlConnect.getConnection();
            String sql = "" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'M' AND birthyear >= 1918 AND birthyear <= 1927\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'F' AND birthyear >= 1918 AND birthyear <= 1927\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'M' AND birthyear >= 1928 AND birthyear <= 1937\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'F' AND birthyear >= 1928 AND birthyear <= 1937\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'M' AND birthyear >= 1938 AND birthyear <= 1947\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'F' AND birthyear >= 1938 AND birthyear <= 1947\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'M' AND birthyear >= 1948 AND birthyear <= 1957\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'F' AND birthyear >= 1948 AND birthyear <= 1957\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'M' AND birthyear >= 1958 AND birthyear <= 1967\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'F' AND birthyear >= 1958 AND birthyear <= 1967\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'M' AND birthyear >= 1968 AND birthyear <= 1977\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'F' AND birthyear >= 1968 AND birthyear <= 1977\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'M' AND birthyear >= 1978 AND birthyear <= 1988\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'F' AND birthyear >= 1978 AND birthyear <= 1988\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'M' AND birthyear >= 1988 AND birthyear <= 1995\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'F' AND birthyear >= 1988 AND birthyear <= 1995\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'M' AND birthyear >= 1996 AND birthyear <= 2002\n" +
                    "UNION\n" +
                    "SELECT COUNT(*) AS antall FROM member WHERE gender = 'F' AND birthyear >= 1996 AND birthyear <= 2002\n" +
                    "";
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);

            int counter = 0;

            while (rs.next()) {
                demographic[counter] = Integer.parseInt(rs.getString("antall"));
                counter++;
            }
        } catch (SQLException e) {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(rs);
            MysqlConnect.closeConnection(con);
            ErrorMessages.errorDisplayErrorBox(e, "There was an error polling age data from the database.");
            return null;
        } finally {
                MysqlConnect.closeStatement(stmt);
                MysqlConnect.closeResSet(rs);
                MysqlConnect.closeConnection(con);
        }

        final String male = "Male";
        final String female = "Female";
        final String age1 = "16-22";
        final String age2 = "23-30";
        final String age3 = "31-40";
        final String age4 = "41-50";
        final String age5 = "51-60";
        final String age6 = "61-70";
        final String age7 = "71-80";
        final String age8 = "81-90";
        final String age9 = "91-100";

        final DefaultCategoryDataset dataset = new DefaultCategoryDataset( );


        dataset.addValue(demographic[0], female, age1);
        dataset.addValue(demographic[1], male, age1);
        dataset.addValue(demographic[2], female, age2);
        dataset.addValue(demographic[3], male, age2);
        dataset.addValue(demographic[4], female, age3);
        dataset.addValue(demographic[5], male, age3);
        dataset.addValue(demographic[6], female, age4);
        dataset.addValue(demographic[7], male, age4);
        dataset.addValue(demographic[8], female, age5);
        dataset.addValue(demographic[9], male, age5);
        dataset.addValue(demographic[10], female, age6);
        dataset.addValue(demographic[11], male, age6);
        dataset.addValue(demographic[12], female, age7);
        dataset.addValue(demographic[13], male, age7);
        dataset.addValue(demographic[14], female, age8);
        dataset.addValue(demographic[15], male, age8);
        dataset.addValue(demographic[16], female, age9);
        dataset.addValue(demographic[17], male, age9);
        return dataset;
    }

    /**
     * This method creates an array with the ages of renters divided into age-categories.
     * @return a dataset with the ages of the people who rents bicycles.
     */
    public CategoryDataset numberOfRentersInAgeGroups() {

        int[] numberOfRenters = new int[9];

        try {
            con = MysqlConnect.getConnection();
            String sql = "" +
                    "SELECT COUNT(member) AS number FROM trip WHERE member IN(SELECT member_id FROM member WHERE birthyear >= 1918 AND birthyear <= 1927)\n" +
                    "UNION\n" +
                    "SELECT COUNT(member) AS number FROM trip WHERE member IN(SELECT member_id FROM member WHERE birthyear >= 1928 AND birthyear <= 1937)\n" +
                    "UNION\n" +
                    "SELECT COUNT(member) AS number FROM trip WHERE member IN(SELECT member_id FROM member WHERE birthyear >= 1938 AND birthyear <= 1947)\n" +
                    "UNION\n" +
                    "SELECT COUNT(member) AS number FROM trip WHERE member IN(SELECT member_id FROM member WHERE birthyear >= 1948 AND birthyear <= 1957)\n" +
                    "UNION\n" +
                    "SELECT COUNT(member) AS number FROM trip WHERE member IN(SELECT member_id FROM member WHERE birthyear >= 1958 AND birthyear <= 1967)\n" +
                    "UNION\n" +
                    "SELECT COUNT(member) AS number FROM trip WHERE member IN(SELECT member_id FROM member WHERE birthyear >= 1968 AND birthyear <= 1977)\n" +
                    "UNION\n" +
                    "SELECT COUNT(member) AS number FROM trip WHERE member IN(SELECT member_id FROM member WHERE birthyear >= 1978 AND birthyear <= 1988)\n" +
                    "UNION\n" +
                    "SELECT COUNT(member) AS number FROM trip WHERE member IN(SELECT member_id FROM member WHERE birthyear >= 1988 AND birthyear <= 1995)\n" +
                    "UNION\n" +
                    "SELECT COUNT(member) AS number FROM trip WHERE member IN(SELECT member_id FROM member WHERE birthyear >= 1996 AND birthyear <= 2002)\n" +
                    "";
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);

            int counter = 0;

            while(rs.next()) {
                numberOfRenters[counter] = Integer.parseInt(rs.getString("number"));
                counter++;
            }
        } catch (SQLException e) {
            ErrorMessages.errorDisplayErrorBox(e, "There was an error polling rental data from the database.");
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(rs);
            MysqlConnect.closeConnection(con);
            return null;
        } finally {
            MysqlConnect.closeStatement(stmt);
            MysqlConnect.closeResSet(rs);
            MysqlConnect.closeConnection(con);
        }

        final String group = "Age group";
        final String age1 = "16-22";
        final String age2 = "23-30";
        final String age3 = "31-40";
        final String age4 = "41-50";
        final String age5 = "51-60";
        final String age6 = "61-70";
        final String age7 = "71-80";
        final String age8 = "81-90";
        final String age9 = "91-100";

        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        dataset.addValue(numberOfRenters[0], group, age1);
        dataset.addValue(numberOfRenters[1], group, age2);
        dataset.addValue(numberOfRenters[2], group, age3);
        dataset.addValue(numberOfRenters[3], group, age4);
        dataset.addValue(numberOfRenters[4], group, age5);
        dataset.addValue(numberOfRenters[5], group, age6);
        dataset.addValue(numberOfRenters[6], group, age7);
        dataset.addValue(numberOfRenters[7], group, age8);
        dataset.addValue(numberOfRenters[8], group, age9);

        return dataset;
    }

    /**
     * Main-method
     *
     */

    public static void main( String[ ] args ) {
        Statistics a = new Statistics("adsa","asd", "powerrepaircosts");
        JFrame f=new JFrame("asd");
        f.add(a);

        f.setPreferredSize(new Dimension(500, 500));
        f.setSize(new Dimension(600, 600));
        f.setVisible(true);
    }
}

