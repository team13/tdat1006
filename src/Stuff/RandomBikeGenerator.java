package Stuff;

import DataEntities.MysqlConnect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RandomBikeGenerator {

    public static void main(String[] args) {
        generateBikes(1000);
    }

    private static void generateBikes(int number){
        String[] colours = {"Red", "Blue", "Black", "Gray", "Pink", "Cyan", "Purple", "Yellow", "Green", "Brown", "Metallic Blue"};
        Random generator = new Random();

        for(int i = 0; i < number; i++){
            String color = colours[generator.nextInt(colours.length)];
            String regnr = getSaltString(6);
            double batteryLevel = generator.nextDouble() * 100.0;
            double distanceCovered = generator.nextDouble() * 10000.0;
            batteryLevel = Stuff.map.DataManager.round(batteryLevel, 2);
            distanceCovered = Stuff.map.DataManager.round(distanceCovered, 2);
            int tripsTotal = generator.nextInt(200);
            int makeId = (generator.nextInt(11) + 1);
            int modelId = ((makeId * 6) - generator.nextInt(6));
            int stationId = (generator.nextInt(30) + 1);

            String randMonth = "";
            int rand_Month = 0;
            rand_Month = (ThreadLocalRandom.current().nextInt(1, 13));
            if (rand_Month < 10 ) {
                randMonth = "0" + rand_Month;
            } else {
                randMonth = "" + rand_Month;
            }

            int daysInMonth = 0;

            if (randMonth.equals("02")) {
                daysInMonth = 28;
            } else if (randMonth.equals("01") || randMonth.equals("03") || randMonth.equals("05") || randMonth.equals("07") || randMonth.equals("08") || randMonth.equals("10") || randMonth.equals("12")) {
                daysInMonth = 31;
            } else {
                daysInMonth = 30;
            }
            String productionDate = ("2016-" + randMonth + "-" + ThreadLocalRandom.current().nextInt(1, daysInMonth++));
            if(addBike(regnr, color, batteryLevel, distanceCovered, tripsTotal, productionDate, makeId, modelId, stationId)){
                System.out.println("Added: " + i);
            }
        }
    }

    public static String getDateTime(){
        LocalDateTime a = LocalDateTime.now();
        LocalDate b = a.toLocalDate();
        LocalTime c = a.toLocalTime().withNano(0);
        return b.toString() + " " + c.toString();
    }

    private static String getSaltString(int length) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();

    }

    private static boolean addBike(String regnr, String colour, double batteryLevel, double distanceCovered, int tripsTotal, String productionDate, int makeID, int modelID, int stationID){
        if(regnr != null){
            String sql = "INSERT INTO bicycle (registration_nr, color, battery_level, total_distance, nr_trips, production_date, make_id, model_id, station_id, atRepairStation) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            Connection con = null;
            PreparedStatement stmt = null;
            boolean ok = false;

            try{
                con = MysqlConnect.getConnection();
                stmt = con.prepareStatement(sql);

                stmt.setString(1, regnr);
                stmt.setString(2, colour);
                stmt.setDouble(3, batteryLevel);
                stmt.setDouble(4, distanceCovered);
                stmt.setInt(5, tripsTotal);
                stmt.setString(6, productionDate);
                stmt.setInt(7, makeID);
                stmt.setInt(8, modelID);
                stmt.setInt(9, stationID);
                stmt.setBoolean(10, false);

                if(stmt.execute()) {
                    ok = true;
                }
            } catch (SQLException err) {
                MysqlConnect.rollback(con);
                err.printStackTrace();
                ok = false;
            } finally {
                MysqlConnect.closeStatement(stmt);
                MysqlConnect.setAutoCommit(con);
                MysqlConnect.closeConnection(con);
            }
            return ok;
        }else{
            return false;
        }
    }
}
