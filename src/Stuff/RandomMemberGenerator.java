package Stuff;

import DataEntities.MysqlConnect;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ThreadLocalRandom;

/**
 * This class is used to randomly generate data. It has the ability to
 * generate random trips, repairs and members. This includes
 * surname, firstname, gender, birthyear, email, phone, password
 * and username. These are randomly pciked from a premade list,
 * These will be placed in our database, and shown below the
 * "member"-tab in the administration-program.
 */

public class RandomMemberGenerator {

    public static void main(String[] args) {
       //  makeRandomRepairs();
        // makeRandomTrips();
       // makeRandomMembers();
    }



    public static void makeRandomTrips() {


        int antall = 11892;

        int daysInMonth = 0;
        for (int i = 0; i < antall; i++) {
            int bike = ThreadLocalRandom.current().nextInt(1, 100);
            int startStation = ThreadLocalRandom.current().nextInt(1, 30);
            int endStation = ThreadLocalRandom.current().nextInt(1, 30);
            int member = ThreadLocalRandom.current().nextInt(1, 4500);
            double dist = Math.ceil(ThreadLocalRandom.current().nextDouble(500.00, 50000.00));
            double batteryUsage = (Math.ceil(dist * 2 / 1000));
            double tripTime = Math.ceil(dist *9.6);


            String randYear = "2018";
            String randMonth = "";
            int rand_Month = 0;
            rand_Month = (ThreadLocalRandom.current().nextInt(1, 5));
            if (rand_Month < 10 ) {
                randMonth = "0" + rand_Month;
            } else {
                randMonth = "" + rand_Month;
            }


            if (randMonth.equals("02")) {
                daysInMonth = 28;
            } else if (randMonth.equals("01") || randMonth.equals("03") || randMonth.equals("05") || randMonth.equals("07") || randMonth.equals("08") || randMonth.equals("10") || randMonth.equals("12")) {
                daysInMonth = 31;
            } else {
                daysInMonth = 30;
            }
            String date = (randYear + "-" + randMonth + "-" + ThreadLocalRandom.current().nextInt(1, daysInMonth));

            writeToFile("INSERT INTO trip (startStation, endStation, batteryUsage, bike, member, dist, tripTime, date) VALUES (\""+startStation+"\", \""+endStation+"\", \""+batteryUsage+"\", \""+bike+"\", \""+member+"\", \""+dist+"\", \""+tripTime+"\", \""+date+"\");\n");
        }
    }




    public static void makeRandomRepairs() {


    int antall = 250;

    int daysInMonth = 0;
        for (int i = 0; i < antall; i++) {
            int bike_id = ThreadLocalRandom.current().nextInt(1, 1000);
            String randYear = "2017";
            String randMonth = "";
            int rand_Month = 0;
            rand_Month = (ThreadLocalRandom.current().nextInt(1, 12));
            if (rand_Month < 10 ) {
                randMonth = "0" + rand_Month;
            } else {
                randMonth = "" + rand_Month;
            }


            if (randMonth.equals("02")) {
                daysInMonth = 28;
            } else if (randMonth.equals("01") || randMonth.equals("03") || randMonth.equals("05") || randMonth.equals("07") || randMonth.equals("08") || randMonth.equals("10") || randMonth.equals("12")) {
                daysInMonth = 31;
            } else {
                daysInMonth = 30;
            }
            String date = (randYear + "-" + randMonth + "-" + ThreadLocalRandom.current().nextInt(1, daysInMonth));

            int randWorkshop = ThreadLocalRandom.current().nextInt(1, 8);
            int randLength = ThreadLocalRandom.current().nextInt(4, 15);
            int randCost = (ThreadLocalRandom.current().nextInt(10, 99) * 100);

            // String email = (randFornavn[i].toLowerCase() + "." + randEtternavn[i].toLowerCase() + randEmail[emailnr]);
            // System.out.println(date + " " + feilnr + " " + bike_id + " ");
            System.out.println("INSERT INTO repair_history (bike_id, repairstart, dmgtype, length, workshop, cost) VALUES (\""+bike_id+"\", \""+date+"\", \""+ThreadLocalRandom.current().nextInt(1, 15)+"\", \""+randLength+"\", \""+randWorkshop+"\", + \""+randCost+"\");");
        }
    }


    public static void makeRandomMembers() {
        int antall = 5000;
        String[] randFornavn = new String[antall];
        String[] randEtternavn = new String[antall];
        String[] randomNavn = new String[antall];
        String[] kjonn = new String[antall];

        MysqlConnect mysql = new MysqlConnect();

        try {
            Connection con = mysql.getConnection();
            String sql = "SELECT * FROM fornavn";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            rs.last();
            String[] fornavn = new String[rs.getRow()];
            String[] gender = new String[rs.getRow()];
            rs.beforeFirst();
            int k = 0;
            while(rs.next()) {
                fornavn[k] = (rs.getString("fornavn"));
                if (Integer.parseInt(rs.getString("ID")) > 622) {
                    gender[k] = "F";
                } else {
                    gender[k] = "M";
                }
                k++;
            }

            MysqlConnect.closeConnection(con);

            for (int i = 0; i < antall; i++) {
                int randomNum = ThreadLocalRandom.current().nextInt(0, fornavn.length);
                randFornavn[i] = fornavn[randomNum];
                kjonn[i] = gender[randomNum];
            }


        } catch (SQLException e) {
            // con.rollback();
            e.printStackTrace();
        }

        try {
            Connection con = mysql.getConnection();
            String sql = "SELECT * FROM etternavn";
            Statement stmt = con.createStatement();
            ResultSet rs= stmt.executeQuery(sql);

            rs.last();
            String[] etternavn = new String[rs.getRow()];
            rs.beforeFirst();
            int i = 0;
            while(rs.next()) {
                etternavn[i] = (rs.getString("etternavn"));
                i++;
            }
            MysqlConnect.closeConnection(con);


            for (int j = 0; j < antall; j++) {
                int randomNum = ThreadLocalRandom.current().nextInt(0, etternavn.length);
                randEtternavn[j] = etternavn[randomNum];
            }

        } catch (SQLException e) {
            // con.rollback();
            e.printStackTrace();
        }

        String[] randEmail = {"@gmail.com", "@hotmail.com", "@random.org", "@telia.no", "@nextgentel.no", "@crispo.no", "@stud.ntnu.no", "@online.no", "@email.com", "@yahoo.com"};

        for (int i = 0; i < antall; i++) {
            int birthyear = ThreadLocalRandom.current().nextInt(1920, 2000);
            String username = (randFornavn[i].toLowerCase() + "." + randEtternavn[i].toLowerCase()+ "." + birthyear);
            String password = "randomPassword";
            int emailnr = ThreadLocalRandom.current().nextInt(0, 9);
            int phone = ThreadLocalRandom.current().nextInt(11111111, 99999999);
            String email = (randFornavn[i].toLowerCase() + "." + randEtternavn[i].toLowerCase() + randEmail[emailnr]);

            // System.out.println("INSERT INTO member (surname, firstname, gender, birthyear, email, phone, password, username) VALUES (\""+randEtternavn[i]+"\", \""+randFornavn[i]+"\", \""+kjonn[i]+"\", \""+birthyear+"\", \""+ email +"\", \""+phone+"\", \""+ username +"\", \""+ password +"\");");

           //  writeToFile("INSERT INTO member (surname, firstname, gender, birthyear, email, phone, password, username) VALUES (\""+randEtternavn[i]+"\", \""+randFornavn[i]+"\", \""+kjonn[i]+"\", \""+birthyear+"\", \""+ email +"\", \""+phone+"\", \""+ username +"\", \""+ password +"\");");
        }
    }

    public static void writeToFile(String str) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter("output.txt", true));

            writer.write(str);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
    }

}
