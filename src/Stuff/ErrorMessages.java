package Stuff;

import javax.swing.*;
import javax.swing.JOptionPane.*;

/**
 *  Helper class which aggregates Informational and Error messaging, enabling fast and effortless altering of frequently used text strings.
 */

public class ErrorMessages {

    /**
     * Private empty constructor indicating that this class should not be instantiated.
     */
    private ErrorMessages(){}




    // Error Messages

    /**
     * Creates a generic text string used specifically for NumberFormatExceptions
     * @return String with a predefined text.
     */
    public static String errorNumberFormat(){
        return "Could not interpret one or more inputs as numeral values. Please ensure that only numeral values are inputted into numeral fields.";
    }

    /**
     * Creates a generic predefined text string used for error messages including no table items selected.
     * @param entity Input String where the user can define which entity is not selected from the the entity table.
     * @return Returns a String with a predefined error message regarding ArrayOutOfBoundsException in tables with no items selected.
     */
    public static String errorNoItemSelected(String entity){
        return "No " + entity + " selected. Please select a " + entity + " from the table and try again.";
    }

    /**
     * General on-screen error message display method. Used when exceptions are thrown and the user needs to be informed.
     * Provides the following error code syntax.
     *
     * sourceClass.sourceMethod(): sourceLine
     *
     * @param e The source Exception object.
     * @param message Method specific message, which relays end-user information.
     */
    public static void errorDisplayErrorBox(Exception e, String message){
        String fullClassName = Thread.currentThread().getStackTrace()[2].getClassName();
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
        String a = className + "." + methodName + "(): line: " + lineNumber;
        String b = message + "\n\nErroneous code: " + a;
        JOptionPane.showMessageDialog(null, b);
    }

    /**
     * General on-screen error message display method.
     * @param message Method specific message, which relays end-user information.
     */
    public static void errorDisplayErrorBox(String message){
        JOptionPane.showMessageDialog(null, message);
    }




    // Success Messages

    /**
     * Creates a generic predefined text string warning the user that some edit operation has succeeded.
     * @param entity Specifies which entity edit operation succeeded.
     * @return Returns a String with a predefined text.
     */
    public static String successEdit(String entity){
        return entity + " was successfully edited.";
    }

    /**
     * Creates a generic predefined text string warning the user that some add operation has succeeded.
     * @param entity Specifies which entity add operation succeeded.
     * @return Returns a String with a predefined text.
     */
    public static String successAdd(String entity){
        return entity + " was successfully added to the database.";
    }

    /**
     * Creates a generic predefined text string warning the user that some delete operation has succeeded.
     * @param entity Specifies which entity delete operation succeeded.
     * @return Returns a String with a predefined text.
     */
    public static String successDelete(String entity){
        return entity + " was successfully deleted from the database.";
    }




    // Failure messages

    /**
     * Creates a generic predefined text string warning the user that some edit operation has failed.
     * @param entity Specifies which entity edit operation failed.
     * @return Returns a String with a predefined text.
     */
    public static String failedEdit(String entity){
        return entity + " was NOT edited.";
    }

    /**
     * Creates a generic predefined text string warning the user that some add operation has failed.
     * @param entity Specifies which entity add operation failed.
     * @return Returns a String with a predefined text.
     */
    public static String failedAdd(String entity){
        return entity + " was NOT added to the database.";
    }

    /**
     * Creates a generic predefined text string warning the user that some delete operation has failed.
     * @param entity Specifies which entity delete operation failed.
     * @return Returns a String with a predefined text.
     */
    public static String failedDelete(String entity){
        return entity + " was NOT deleted from the database.";
    }
}
