/**
 * The GNSS, short for Global Navigation Satellite System, enables the creation of GNSS coordinates in accordance with the WGS84 datum.
 * A GNSS object consists of latitude, longitude and elevation. Additional methods provide metric distance calculations between two GNSS coordinates.
 *
 * @author Håvard Ramberg
 * @version 1.1
 * @since 2018-03-05
 */

package Stuff.map;

import DataEntities.MysqlConnect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import static java.lang.Math.cos;

public class GNSS {

    public final String DATUM = "WGS84";
    private double longitude;
    private double latitude;
    private double elevation;
    public static final double MAX_LONGITUDE = 180.000000;
    public static final double MIN_LONGITUDE = -180.000000;
    public static final double MAX_LATITUDE = 90.000000;
    public static final double MIN_LATITUDE = -90.000000;
    private int gps_id;

    /**
     * GNSS constructor. GNSS(double longitude, double latitude, double elevation).
     * Latitude ang longitude values must conform with the maximum and minimum threshold, provided as final class variables.
     *
     * */

    public GNSS(double latitude, double longitude, double elevation) {
        if (longitude <= MAX_LONGITUDE && longitude >= MIN_LONGITUDE && latitude <= MAX_LATITUDE && latitude >= MIN_LATITUDE) {
            this.longitude = longitude;
            this.latitude = latitude;
            this.elevation = elevation;
        }
    }
    public GNSS(double latitude, double longitude, double elevation, int gps_id) {
        if (longitude <= MAX_LONGITUDE && longitude >= MIN_LONGITUDE && latitude <= MAX_LATITUDE && latitude >= MIN_LATITUDE) {
            this.longitude = longitude;
            this.latitude = latitude;
            this.elevation = elevation;
            this.gps_id = gps_id;
        }
    }

    public static ArrayList<GNSS> getAllBikeCoordinats() {
        ArrayList<GNSS> bikecoordinate = new ArrayList<>();
            ResultSet res = null;
            Connection con = null;
            PreparedStatement statement = null;

            try {
                con = MysqlConnect.getConnection();
                statement = con.prepareStatement("SELECT * FROM gps");
                res = statement.executeQuery();

                int i = 0;
                while (res.next()) {
                    bikecoordinate.add(new GNSS(res.getDouble("lat"), res.getDouble("lon"), res.getDouble("el"),res.getInt("gps_id")));
                    i++;
                }

            } catch (Exception e) {
                System.out.println("Error: " + e);
            } finally {
                MysqlConnect.closeStatement(statement);
                MysqlConnect.closeConnection(con);
            }

        return bikecoordinate;
        }


    /**
     * public double getLatitude()
     * @return Returns the GNSS coordinate latitude as a double data type.
     * */
    public double getLatitude() {
        return latitude;
    }

    /**
     * public double getLongitude()
     * @return Returns the GNSS coordinate longitude as a double data type.
     * */
    public double getLongitude() {
        return longitude;
    }

    /**
     * public double getGps_id()
     * @return Returns the GNSS coordinates database id
     * */
    public int getGps_id() {
        return gps_id;
    }

    /**
     * public double getElevation()
     * @return Returns the GNSS coordinate elevation as a double data type. A value of 0.0 references sea level.
     * Positive values indicate an elevation above sea level. Negative values indicate an elevation below sea level.
     *
     */
    public double getElevation() {
        return elevation;
    }

    /**
     * public String toString()
     * @return Returns GNSS coordinates as standard, easy to read single sentence coordinates.
     */
    public String toString() {
        String a = String.format("%.2f", +latitude);
        String b = String.format("%.2f", +longitude);
        String c = String.format("%.2f", +elevation);
        String returnStatement;
        if (latitude > 0) {
            returnStatement = a + "N ";
        } else {
            returnStatement = a + "S ";
        }
        if (longitude > 0) {
            returnStatement += b + "E ";
        } else {
            returnStatement += b + "W ";
        }
        returnStatement += ("El = " + c);
        return returnStatement;
    }

    /**Calculates the great circle distance between the current GNSS postion this.GNSS and y.GNSS position. Derived from the haversine formula.
     *
     * @param y GNNS coordinate of target position.
     * @return Distance between this.GNSS and y.GNSS cordinate measured in metres. Delta distance = y.Position - this.Position.
     */

    public double distance(GNSS y) {

        double r = earthRadius(this); // Earth radius given the this position.

        double deltaLat = Math.toRadians(y.getLatitude() - this.latitude);
        double deltaLon = Math.toRadians(y.getLongitude() - this.longitude);
        double a = Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2)
                + cos(Math.toRadians(this.latitude)) * cos(Math.toRadians(y.getLatitude()))
                * Math.sin(deltaLon / 2) * Math.sin(deltaLon / 2);
        double b = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = r * b;

        double height = y.getElevation() - this.elevation;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }

    /**Calculates the great circle distance between GNSS postions x and y. Derived from the haversine formula.
     *
     * @param x GNSS coordinate of start position.
     * @param y GNSS coordinate of target position.
     * @return Distance between x.GNSS coordinate and y.GNSS coordinate measured in metres. Delta distance = y.Position - x.Position.
     */
    public static double distance(GNSS x, GNSS y) {

        double r = earthRadius(x); // Earth radius given the x position.

        double deltaLat = Math.toRadians(y.getLatitude() - x.getLatitude());
        double deltaLon = Math.toRadians(y.getLongitude() - x.getLongitude());
        double a = Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2)
                + cos(Math.toRadians(x.getLatitude())) * cos(Math.toRadians(y.getLatitude()))
                * Math.sin(deltaLon / 2) * Math.sin(deltaLon / 2);
        double b = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = r * b;

        double height = y.getElevation() - x.getElevation();

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }

    /**
     * Calculates the local input GNSS coordinates Earth radius. Computer programmed formula
     * is derived from the Geocentric radius formula given on https://en.wikipedia.org/wiki/Earth_radius
     * The final equatorial and polar radii are dervide from the WGS84 ellipsoid.
     *
     * @param coordinate GNSS coordinates for target location radius calculation.
     * @return returns a decimal double value with the local Earth radius at the given GNSS coordinate.
     */
    private static double earthRadius(GNSS coordinate) {
        final double EQUATORIAL_RADIUS = 6378137;
        final double POLAR_RADIUS = 6356752;

        double a = cos(coordinate.getLatitude()) * Math.pow(EQUATORIAL_RADIUS, 2);
        double b = cos(coordinate.getLatitude()) * Math.pow(POLAR_RADIUS, 2);
        double c = cos(coordinate.getLatitude()) * EQUATORIAL_RADIUS;
        double d = cos(coordinate.getLatitude()) * POLAR_RADIUS;

        double up = Math.pow(a, 2) + Math.pow(b, 2);
        double down = Math.pow(c, 2) + Math.pow(d, 2);

        return Math.sqrt(up / down);
    }

    /**
     *  Generates a randomly generated GNSS coordinate limited by the two input GNSS coordinates min and max.
     *
     * @param min Minimum GNSS coordinate. All randomly generated GNSS coordinates will have longitude, latitude and elevation data higher than the Minimum GNSS coordinate.
     * @param max Maximum GNSS coordinate. All randomly generated GNSS coordinates will have longitude, latitude and elevation data lower than the Maximum GNSS coordinate.
     * @return A randomly generated GNSS coordinate within the limits provided by min and max.
     */
    public static GNSS randomCoordinate(GNSS min, GNSS max) {
        java.util.Random generator = new java.util.Random();
        double randomLat = generator.nextDouble() * (max.getLatitude() - min.getLatitude()) + min.getLatitude();
        double randomLon = generator.nextDouble() * (max.getLongitude() - min.getLongitude()) + min.getLongitude();
        double randomEl = generator.nextDouble() * (max.getElevation() - min.getElevation()) + min.getElevation();
        return new GNSS(randomLat, randomLon, randomEl);
    }
}