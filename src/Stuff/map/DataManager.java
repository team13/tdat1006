/**
 *
 * DataManager offers functions for WebMap related data polling.
 *
 * @author  Håvard Ramberg
 * @version 1.1
 * @since 2018-03-05
 *
 * */
package Stuff.map;

import DataEntities.Bike;
import DataEntities.ChargingStation;
import DataEntities.MysqlConnect;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

public class DataManager {
    private MysqlConnect mysql = new MysqlConnect();
    private final Stuff.map.WebMap controlledApp;

    /**
     * Creates an instance of DataManager.
     * @param controlledApp the object which controlls this DataManager instance.
     */
    public DataManager(Stuff.map.WebMap controlledApp){
        this.controlledApp = controlledApp;
    }

    /**
     * Queries bicycle information from the datasource of the controlledApp.
     * @return Returns an ArrayList<Bike> of Bike objects that whose information will be displayed on the map.
     */
    public ArrayList<ChargingStation> queryChargingStations(){

        Stuff.map.GNSS min = new Stuff.map.GNSS(63.3652524,10.343883799999958, 0);
        Stuff.map.GNSS max = new Stuff.map.GNSS(63.45608840000001,10.447525700000028, 300);

        PreparedStatement stm = null;
        Connection con = null;
        ResultSet res = null;
        ArrayList<ChargingStation> outputList = new ArrayList<>();
        try{
            con = MysqlConnect.getConnection();
            String sql = "SELECT station_id, station_name, street_name, street_nr, capacity, vacancy, lat, lon, el FROM charging_station";
            stm = con.prepareStatement(sql);
            res = stm.executeQuery();

            while(res.next()){
                Stuff.map.GNSS position = new Stuff.map.GNSS(res.getDouble(7),res.getDouble(8),res.getDouble(9));
                int station_id = res.getInt(1);
                String station_name = res.getString(2);
                String street_name = res.getString(3);
                String street_nr = res.getString(4);
                int capacity = res.getInt(5);
                int vacancy = res.getInt(6);
                outputList.add(new ChargingStation(station_id, station_name, street_name, street_nr, capacity, vacancy, 100, position));
            }
        }catch(Exception e){
            outputList = null;
        }finally{
            MysqlConnect.closeConnection(con);
            MysqlConnect.closeStatement(stm);
            MysqlConnect.closeResSet(res);
        }
        return outputList;
    }

    /**
     * Queries bicycle information from the datasource of the controlledApp.
     * @return Returns an ArrayList<Bike> of Bike objects whose information will be displayed on the map.
     */
    public ArrayList<Bike> queryBikes(){

        Stuff.map.GNSS min = new Stuff.map.GNSS(63.3652524,10.343883799999958, 0);
        Stuff.map.GNSS max = new Stuff.map.GNSS(63.45608840000001,10.447525700000028, 300);

        PreparedStatement stm = null;
        Connection con = null;
        ResultSet res = null;
        ArrayList<Bike> outputList = new ArrayList<>();
        try{
            con = MysqlConnect.getConnection();
            String sql = "SELECT t1.bicycle_id, registration_nr, ROUND(battery_level, 1) AS battery_level, nr_trips, member_id, lat, lon, el, oldLat, oldLon, oldEl FROM (" +
                    "SELECT * FROM gps WHERE (bicycle_id, date) IN (SELECT bicycle_id, MAX(date) AS date FROM gps GROUP BY bicycle_id)) t1 JOIN (" +
                    "SELECT bicycle_id, date AS oldTime, lon AS oldLon, lat AS oldLat, el AS oldEl FROM gps WHERE (bicycle_id, date) IN(" +
                    "SELECT bicycle_id, MAX(date) FROM gps WHERE (bicycle_id, date) NOT IN(" +
                    "SELECT bicycle_id, date FROM gps WHERE (bicycle_id, date) IN (" +
                    "SELECT bicycle_id, MAX(date) AS date FROM gps GROUP BY bicycle_id)" +
                    ") GROUP BY bicycle_id" +
                    ")" +
                    ") t2 ON (t1.bicycle_id = t2.bicycle_id) JOIN bicycle ON (t1.bicycle_id = bicycle.bicycle_id)" +
                    "WHERE t1.bicycle_id IN (SELECT bicycle_id FROM bicycle WHERE station_id IS NULL AND atRepairStation IS FALSE);";
            stm = con.prepareStatement(sql);
            res = stm.executeQuery();

            while(res.next()){
                Stuff.map.GNSS pos = new Stuff.map.GNSS(res.getDouble(6), res.getDouble(7), res.getDouble(8));
                Stuff.map.GNSS oldPos = new Stuff.map.GNSS(res.getDouble(9), res.getDouble(10), res.getDouble(11));
                int bike_id = res.getInt(1);
                int nr_trips = res.getInt(4);
                int member_id = res.getInt(5);
                String reg_nr = res.getString(2);
                double battery_level = res.getDouble(3);
                double deltaDistance = oldPos.distance(pos);
                NumberFormat formatter = new DecimalFormat("#0.0");
                outputList.add(new Bike(bike_id, reg_nr, member_id, battery_level, round(deltaDistance, 0), nr_trips, pos));
            }
        }catch(Exception e){
            outputList = null;
        }finally{
            MysqlConnect.closeConnection(con);
            MysqlConnect.closeStatement(stm);
            MysqlConnect.closeResSet(res);
        }
        return outputList;
    }

    /**
     * Rounds double decimals values to a specified amount of decimal count.
     *
     * @param value the initial double decimal value that should be rounded.
     * @param places decimal count. How many decimal values after comma.
     * @return returns the rounded double decimal value of 'value',
     */

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
