/**
 *
 * WebMap creates an instance of a JavaFX scene which can be used by external applications to run a Google Maps APIv3
 * inside a Java application.
 *
 * @author  Håvard Ramberg
 * @version 1.1
 * @since 2018-03-05
 *
 * */

package Stuff.map;

import javax.swing.*;
import javax.swing.JOptionPane.*;
import DataEntities.Bike;
import DataEntities.ChargingStation;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class WebMap{

    private Scene scene;
    private StackPane root;
    private WebView webView;
    private WebEngine webEngine;
    private ToggleGroup mapTypeGroup;
    private ToggleButton road, satellite, hybrid, terrain, showBikes, showStations;
    private Stuff.map.DataManager backend;
    private ScheduledExecutorService scheduler;
    private final long UPDATE_INTERVAL = 15;

    /**
     * Used for external initialization.
     * @return Returns a Scene object containing the Google Maps within an HTML-enclosure.
     */
    public Scene initExternal(){
        this.backend = new Stuff.map.DataManager(this);
        initGoogleMap();
        initRoot();
        initStationPlot();
        initBikePlot();
        initScheduler();
        return scene;
    }

    /**
     * Initializes the HTML webview and engages the WebEngine, which enables the JavaFX application to run HTML services.
     */
    private void initGoogleMap(){
        webView = new WebView();
        webEngine = webView.getEngine();
        webEngine.load(getClass().getResource("rel/google.html").toString());
    }

    /**
     * Initializes the root Scene object.
     */
    private void initRoot(){
        root = new StackPane();
        scene = new Scene(root,600,400, Color.web("#666970"));
        try{
            scene.getStylesheets().add(WebMap.class.getResource("rel/style.css").toExternalForm());
        }catch (Exception e){
            e.printStackTrace();
        }

        // create map type buttons
        mapTypeGroup = new ToggleGroup();
        road = new ToggleButton("Road");
        road.setSelected(true);
        road.setToggleGroup(mapTypeGroup);

        satellite = new ToggleButton("Satellite");
        satellite.setToggleGroup(mapTypeGroup);

        hybrid = new ToggleButton("Hybrid");
        hybrid.setToggleGroup(mapTypeGroup);

        terrain = new ToggleButton("Terrain");
        terrain.setToggleGroup(mapTypeGroup);

        showBikes = new ToggleButton("Bikes");
        showBikes.setSelected(true);

        showStations = new ToggleButton("Stations");
        showStations.setSelected(true);

        mapTypeGroup.selectedToggleProperty().addListener(
            new ChangeListener<Toggle>() {
                public void changed(
                    ObservableValue<? extends Toggle> observableValue,
                    Toggle toggle, Toggle toggle1) {
                        if (road.isSelected()) {
                            webEngine.executeScript("document.setMapTypeRoad()");
                        } else if (satellite.isSelected()) {
                            webEngine.executeScript("document.setMapTypeSatellite()");
                        } else if (hybrid.isSelected()) {
                            webEngine.executeScript("document.setMapTypeHybrid()");
                        } else if (terrain.isSelected()) {
                            webEngine.executeScript("document.setMapTypeTerrain()");
                        }
                    }
        });

        showBikes.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(newValue){
                    webEngine.executeScript("document.showAllBikes()");
                }else{
                    webEngine.executeScript("document.hideAllBikes()");
                }
            }
        });

        showStations.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(newValue){
                    webEngine.executeScript("document.showAllStations()");
                }else{
                    webEngine.executeScript("document.hideAllStations()");
                }
            }
        });


        Button zoomIn = new Button("Zoom In");
        zoomIn.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent actionEvent) {
                webEngine.executeScript("document.zoomIn()");
            }
        });
        Button zoomOut = new Button("Zoom Out");
        zoomOut.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent actionEvent) {
                webEngine.executeScript("document.zoomOut()");
            }
        });

        ToolBar toolBar = new ToolBar();
        toolBar.getStyleClass().add("map-toolbar");
        toolBar.getItems().addAll(
                new Label("Map view: "), road, satellite, hybrid, terrain, createSpacer(50),
                new Label("Toggle view: "), showBikes, showStations, createSpacer(0),
                new Label("Location: "), zoomIn, zoomOut);
        root.getChildren().addAll(webView, toolBar);
        StackPane.setAlignment(toolBar, Pos.TOP_CENTER);
    }

    /**
     * Queries bicycle data from the database and calls external functions to plot bicycle markers on the map.
     */
    private void initBikePlot(){
        ArrayList<Bike> bikes = backend.queryBikes();
        // Waits until the Google Map application has completed its initial initiation before plotting Bikes on the map.
        webEngine.getLoadWorker().stateProperty().addListener((ov, oldState, newState) -> {
            if (newState == Worker.State.SUCCEEDED) {
                for(int i = 0; i < bikes.size(); i++){
                    try{
                        webEngine.executeScript("document.createBikeMarker(" + bikes.get(i).getPosition().getLatitude() + ", " +
                                bikes.get(i).getPosition().getLongitude()+ ", " +
                                bikes.get(i).getBike_id() + ", '" + bikes.get(i).getRegnumber() + "', " + bikes.get(i).getMember_id() + ", " +
                                bikes.get(i).getBattery_level() + ", " + bikes.get(i).getTotal_mileage() + ")");
                    }catch(NullPointerException e){
                        JOptionPane.showMessageDialog(null, "Bicycle with ID: " + bikes.get(i).getBike_id() + " is causing errors during the marker plotting process.\n" +
                                "Please verify that bicycle with ID " + bikes.get(i).getBike_id() + " has correct information in every data field.");
                        break;
                    }
                }
                webEngine.executeScript("document.showAllBikes()");
            }
        });
    }

    /**
     * Queries charging station data from the database and calls external functions to plot charging station markers on the map.
     */
    private void initStationPlot(){
        ArrayList<ChargingStation> stations = backend.queryChargingStations();
        // Waits until the Google Map application has completed its initial initiation before plotting Charging Stations on the map.
        webEngine.getLoadWorker().stateProperty().addListener((ov, oldState, newState) -> {
            if (newState == Worker.State.SUCCEEDED) {
                for(int i = 0; i < stations.size(); i++){
                    try{
                        webEngine.executeScript("document.createStationMarker("+stations.get(i).getPosition().getLatitude()+", "+
                                stations.get(i).getPosition().getLongitude()+ ", " + stations.get(i).getStation_id() + ", '" + stations.get(i).getName() + "', '" +
                                stations.get(i).getStreet_name() + "', '" + stations.get(i).getStreet_number() + "', " + stations.get(i).getMax_bikes() + ", "
                                + stations.get(i).getCurrent_nr_bikes() + ")");
                    }catch(NullPointerException e){
                        JOptionPane.showMessageDialog(null, "Charging Station with ID: " + stations.get(i).getStation_id() + " is causing errors during the marker plotting process.\n" +
                        "Please verify that station with ID " + stations.get(i).getStation_id() + " has correct information in every data field.");
                        break;
                    }
                }
                webEngine.executeScript("document.showAllStations()");
            }
        });
    }

    /**
     * Initiates a scheduler which runs a set of instructions, loop, every UPDATE_INTERVAL seconds.
     * Periodically updates the map with new bicycle positions.
     */
    private void initScheduler(){
        try{
            scheduler = Executors.newScheduledThreadPool(1, runnable -> {
                Thread t = new Thread(runnable);
                t.setDaemon(true);
                return t;
            });
            Runnable loop = new Runnable() {
                public void run() {
                    Platform.runLater(new Runnable() { // Ensures that the intended task is run in the JavaFX thread.
                        @Override
                        public void run() {
                            executeMapUpdate(backend.queryBikes());
                        }
                    });
                }
            };
            scheduler.scheduleAtFixedRate(loop, 10, UPDATE_INTERVAL, TimeUnit.SECONDS);
        }catch (Exception e){
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Updates the map view with updated bicycle information.
     * @param bikes ArrayList<Bike> is a list of Bike objects queried from the bicycle database.
     */
    private void executeMapUpdate(ArrayList<Bike> bikes){
        webEngine.executeScript("document.deleteAllBikes()"); // Purges map of all former bike markers.
        for(int i = 0; i < bikes.size(); i++){
            webEngine.executeScript("document.createBikeMarker(" + bikes.get(i).getPosition().getLatitude() + ", " +
                    bikes.get(i).getPosition().getLongitude()+", " +
                    bikes.get(i).getBike_id() + ", '" + bikes.get(i).getRegnumber() +"', " + bikes.get(i).getMember_id() + ", " +
                    bikes.get(i).getBattery_level() + ", " + bikes.get(i).getTotal_mileage() + ")");
        }
        if(showBikes.isSelected()){
            webEngine.executeScript("document.showAllBikes()");
        }
    }

    /**
     * Creates an invisible component used for JavaFX component offset.
     * @param pixels width of spacer in pixels.
     * @return returns a Node object which has the width of pixels.
     */
    private Node createSpacer(int pixels) {
        if(pixels == 0){
            Region spacer = new Region();
            HBox.setHgrow(spacer, Priority.ALWAYS);
            return spacer;
        }else{
            Region spacer = new Region();
            spacer.setMinWidth(pixels);
            return spacer;
        }
    }
}