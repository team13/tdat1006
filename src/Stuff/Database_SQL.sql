CREATE TABLE manufacturer(
  make_id INTEGER AUTO_INCREMENT,
  name VARCHAR(30),
  country VARCHAR(30),
  PRIMARY KEY (make_id)
);

CREATE TABLE model(
  model_id INTEGER AUTO_INCREMENT,
  name VARCHAR(30),
  PRIMARY KEY (model_id)
);

CREATE TABLE member(
  member_id INTEGER AUTO_INCREMENT,
  surname VARCHAR(30),
  firstname VARCHAR(30),
  birthyear INTEGER,
  email VARCHAR(50),
  phone INTEGER,
  PRIMARY KEY (member_id)
);

CREATE TABLE charging_station(
  station_id INTEGER AUTO_INCREMENT,
  street_name VARCHAR(50),
  street_nr INTEGER,
  postnr INTEGER,
  capasity INTEGER,
  vacany INTEGER,
  PRIMARY KEY (station_id)
);

CREATE TABLE bicycle(
  bicycle_id INTEGER AUTO_INCREMENT,
  registration_nr VARCHAR(30) UNIQUE,
  color VARCHAR(30),
  battery_level DOUBLE,
  total_distance DOUBLE,
  nr_trips INTEGER,
  production_date DATE,
  make_id INTEGER,
  model_id INTEGER,
  station_id INTEGER,
  member_id INTEGER,
  PRIMARY KEY (bicycle_id),
  FOREIGN KEY (make_id) REFERENCES manufacturer(make_id),
  FOREIGN KEY (model_id) REFERENCES model(model_id),
  FOREIGN KEY (station_id) REFERENCES charging_station(station_id),
  FOREIGN KEY (member_id) REFERENCES member(member_id)
);

CREATE TABLE workshop(
  workshop_id INTEGER AUTO_INCREMENT,
  name VARCHAR(30),
  adress VARCHAR(30),
  phone INTEGER,
  comments VARCHAR(100),
  PRIMARY KEY (workshop_id)
);

CREATE TABLE repair(
  repair_id INTEGER AUTO_INCREMENT,
  reason VARCHAR(50),
  est_return INTEGER,
  est_cost INTEGER,
  bicycle_id INTEGER,
  workshop_id INTEGER,
  PRIMARY KEY (repair_id),
  FOREIGN KEY (bicycle_id) REFERENCES bicycle(bicycle_id),
  FOREIGN KEY (workshop_id) REFERENCES workshop(workshop_id)
);

CREATE TABLE admin_users(
  user_id INTEGER AUTO_INCREMENT,
  surename VARCHAR(30),
  firstname VARCHAR(30),
  birthyear INTEGER,
  email VARCHAR(30),
  phone INTEGER,
  username VARCHAR(30),
  password VARCHAR(30),
  PRIMARY KEY (user_id)
);

CREATE TABLE gps_route(
  rout_id INTEGER AUTO_INCREMENT,
  navn VARCHAR(30),
  beskrivelse VARCHAR(50),
  PRIMARY KEY (rout_id)
);

CREATE TABLE gps_coords(
  coords_id INTEGER AUTO_INCREMENT,
  posX INTEGER,
  posY INTEGER,
  rout_id INTEGER,
  bicycle_id INTEGER,
  PRIMARY KEY (coords_id),
  FOREIGN KEY (rout_id) REFERENCES gps_route(rout_id),
  FOREIGN KEY (bicycle_id) REFERENCES bicycle(bicycle_id)
);

CREATE TABLE gps(
  gps_id INTEGER AUTO_INCREMENT,
  posX INTEGER,
  posY INTEGER,
  bicycle_id INTEGER,
  PRIMARY KEY (gps_id),
  FOREIGN KEY (bicycle_id) REFERENCES bicycle(bicycle_id)
);

CREATE TABLE cs_history(
  history_id INTEGER AUTO_INCREMENT,
  station_id INTEGER,
  day DATE,
  watts DOUBLE,
  PRIMARY KEY (history_id),
  FOREIGN KEY (station_id) REFERENCES charging_station(station_id)
);

CREATE TABLE bicycle_history(
  history_id INTEGER AUTO_INCREMENT,
  bicycle_id INTEGER,
  member_id INTEGER,
  day DATE,
  PRIMARY KEY (history_id),
  FOREIGN KEY (bicycle_id) REFERENCES bicycle(bicycle_id),
  FOREIGN KEY (member_id) REFERENCES member(member_id)
);

INSERT INTO admin_users(user_id, surename, firstname, birthyear, email, phone, username, password)
  VALUES(0001, "Valstadvse", "Øyvind", 1979, "oyvinval@stud.ntnu.no", 12345678, "oyvinval", "heioghopp");

INSERT INTO admin_users(user_id, surename, firstname, birthyear, email, phone, username, password)
  VALUES(0002, "Lilleeng", "Simon", 1992, "simonlill@stud.ntnu.no", 12345678, "simonlil", "heioghopp");

INSERT INTO admin_users(user_id, surename, firstname, birthyear, email, phone, username, password)
  VALUES(0003, "Nygård", "Marius", 1995, "marinyg@stud.ntnu.no", 12345678, "marinyg", "heioghopp");

INSERT INTO admin_users(user_id, surename, firstname, birthyear, email, phone, username, password)
  VALUES(0004, "Ramberg", "Håvard", 1995, "haavram@stud.ntnu.no", 12345678, "haavram", "heioghopp");

INSERT INTO admin_users(user_id, surname, firstname, birthyear, email, phone, username)
  VALUES(0005, "Dalseth", "Christian", 1994, "chrismd@stud.ntnu.no", 12345678, "chrismd");

DELETE FROM admin_users WHERE user_id = 21;

DELETE FROM bicycle WHERE registration_nr = '126838745';

SELECT bicycle_id FROM bicycle WHERE registration_nr = 226838745;

SELECT hashWithSalt FROM admin_users WHERE username = "oyvinval";

UPDATE admin_users SET hashWithSalt = "$31$16$tn4SGJII_uaQMRD_kqA4NTZLrQ84oop6zFqvzu8wVhU" WHERE user_id = 5;

ALTER TAble admin_users DROP COLUMN password;

INSERT INTO workshop (workshop_id, name, adress, phone, comments)
  VALUES(1, "eTramp HQ", "Gunneruds gate 5", 12345678, "Our storage");

INSERT INTO charging_station (station_id, street_name, street_nr, postnr, capacity, vacancy, posX, posY)
  VALUES (1, "Gunneruds gate", "5", 7055, 100, 0, 0.000000, 0.000000);

UPDATE bicycle SET station_id = null WHERE station_id = 1;

ALTER TABLE repair MODIFY COLUMN est_return VARCHAR(50);

SELECT bicycle_id, MAX(date) FROM gps GROUP BY bicycle_id;

UPDATE bicycle SET production_date = '2005-07-01' WHERE bicycle_id = 27;

SELECT production_date FROM bicycle WHERE bicycle_id = 27;

-- Queries used for plotting bikes on the WebMap

SELECT * FROM gps WHERE (bicycle_id, date) IN (SELECT bicycle_id, MAX(date) AS date FROM gps GROUP BY bicycle_id) AND bicycle_id = 1;

SELECT bicycle_id, date AS oldTime, lon AS oldLon, lat AS oldLat, el AS oldEl FROM gps WHERE (bicycle_id, date) IN(
  SELECT bicycle_id, MAX(date) FROM gps WHERE (bicycle_id, date) NOT IN(
    SELECT bicycle_id, date FROM gps WHERE (bicycle_id, date) IN (SELECT bicycle_id, MAX(date) AS date FROM gps GROUP BY bicycle_id)
  ) GROUP BY bicycle_id
) AND bicycle_id = 1;

SELECT t1.bicycle_id, t1.date, registration_nr, battery_level, nr_trips, member_id, lat, lon, el, oldTime, oldLat, oldLon, oldEl FROM (SELECT * FROM gps WHERE (bicycle_id, date) IN (SELECT bicycle_id, MAX(date) AS date FROM gps GROUP BY bicycle_id)) t1 JOIN (
  SELECT bicycle_id, date AS oldTime, lon AS oldLon, lat AS oldLat, el AS oldEl FROM gps WHERE (bicycle_id, date) IN(
    SELECT bicycle_id, MAX(date) FROM gps WHERE (bicycle_id, date) NOT IN(
      SELECT bicycle_id, date FROM gps WHERE (bicycle_id, date) IN (SELECT bicycle_id, MAX(date) AS date FROM gps GROUP BY bicycle_id)
    ) GROUP BY bicycle_id
  )
) t2 ON (t1.bicycle_id = t2.bicycle_id) JOIN bicycle ON (t1.bicycle_id = bicycle.bicycle_id);

SELECT station_id FROM charging_station WHERE street_name = 'Lerkevegen' AND street_nr = 13;

DELETE FROM charging_station WHERE station_id = 54;

DELETE FROM member WHERE member_id = 4876;

SELECT member_id FROM member WHERE email = 'børre.børresen@børris.no';

DELETE FROM admin_users WHERE user_id = 29;

SELECT member_id FROM member WHERE email = 'borre.borresen@borris.no'

SELECT COUNT(member_id) AS antall FROM member WHERE birthyear >= 1918 AND birthyear <= 1927;

SELECT COUNT(*) AS antall FROM member WHERE gender = 'M' AND birthyear >= 1918 AND birthyear <= 1927

SELECT COUNT(*) AS antall FROM member WHERE gender = 'F' AND birthyear >= 1918 AND birthyear <= 1927

SELECT COUNT(member) AS number FROM trip WHERE member IN(SELECT member_id FROM member WHERE birthyear >= 1918 AND birthyear <= 1927);