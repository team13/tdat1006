package GUI;

import DataEntities.Bike;

import javax.swing.*;
import java.awt.*;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * This class is made to configure and create InputAreas in the administration-
 * program. By gathering these methods in one class, we managed to create
 * a better, more efficient  and tidy overview of our Administration-class (shortened
 * length of code).
 */

public class InputArea extends JPanel{
    private ArrayList<String> inputs=new ArrayList<>();
    private ArrayList<JTextField> fields = new ArrayList<>();
    private ArrayList<JComboBox<String>> comboBoxes = new ArrayList<>();
    private JButton cancel = new JButton("Cancel");
    private JButton confirm = new JButton("Confirm");

    /**
     * This method creates a InputArea with the specifications listed below.
     * @param fieldNames first parameter. The name of the field.
     * @param ignore1 second parameter. Is the parameter that ignores field input and places a JComboBox instead.
     * @param ignore2 third parameter. Same as second parameter.
     * @param comboBoxes fourth parameter. Adding ComboBox to the specific InputArea.
     */

    public InputArea(String[] fieldNames, String ignore1,String ignore2,ArrayList<JComboBox<String>> comboBoxes){
        JPanel dataView = new JPanel(new GridLayout(fieldNames.length, 1, 5, 5));
        JPanel labelView = new JPanel(new GridLayout(fieldNames.length, 1, 5, 5));
        JPanel buttons = new JPanel(new GridLayout(fieldNames.length, 1, 5, 5));
        this.comboBoxes = comboBoxes;
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        int i = 0;
        for (String name : fieldNames) {
            labelView.add(new JLabel(name));
            if (!(name.equals(ignore1) || name.equals(ignore2))) {
                JTextField tmp = new JTextField();
                fields.add(tmp);
                dataView.add(tmp);
            } else {
                dataView.add(comboBoxes.get(i));
                i++;
            }
        }

        labelView.setMaximumSize(new Dimension(200, 200));
        labelView.setMinimumSize(new Dimension(120, 60));
        dataView.setMinimumSize(new Dimension(150, 60));
        dataView.setMaximumSize(new Dimension(250, 200));
        buttons.setMaximumSize(new Dimension(90, 200));

        buttons.add(confirm);
        buttons.add(cancel);
        add(labelView);
        add(dataView);
        add(Box.createRigidArea(new Dimension(4, 0)));
        add(buttons);
    }

    /**
     * This method creates an InputArea with the specifications listed below.
     * @param fieldNames The name of the specific field.
     */

    public InputArea(String[] fieldNames){
        JPanel dataView = new JPanel(new GridLayout(fieldNames.length, 1, 5, 5));
        JPanel labelView = new JPanel(new GridLayout(fieldNames.length, 1, 5, 5));
        JPanel buttons = new JPanel(new GridLayout(fieldNames.length, 1, 5, 5));
        this.comboBoxes = comboBoxes;
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        for (String name : fieldNames) {
            labelView.add(new JLabel(name));
            JTextField tmp = new JTextField();
            tmp.setMinimumSize(new Dimension(200, 30));
            fields.add(tmp);
            dataView.add(tmp);
        }

        labelView.setMaximumSize(new Dimension(120, 220));
        dataView.setMinimumSize(new Dimension(150, 60));
        dataView.setMaximumSize(new Dimension(250, 220));
        buttons.setMaximumSize(new Dimension(90, 220));

        buttons.add(confirm);
        buttons.add(cancel);
        add(labelView);
        add(dataView);
        add(Box.createRigidArea(new Dimension(4, 0)));
        add(buttons);
    }

    /**
     * This method is obtaining the current inputs, in the current ComboBox.
     * @return returns an ArrayList of Strings for the ComboBox.
     */

    public ArrayList<String> getCurrentInputs() {
        inputs.clear();
        for (JTextField field:fields){
            inputs.add(field.getText());
        }
        for (JComboBox<String> comboBox:comboBoxes){
            inputs.add((String)comboBox.getSelectedItem());
        }
        return inputs;
    }

    /**
     * This method lets the user configure and edit the specific field and ComboBox.
     * @param fieldsText is the text used to edit the field.
     * @param comboBoxesText is the text used to edit the ComboBox.
     */

    public void setFields(ArrayList<String> fieldsText,ArrayList<String> comboBoxesText){
        for (int i =0;i<fields.size();i++){
            fields.get(i).setText(fieldsText.get(i));
        }
        for (int i=0;i<comboBoxes.size();i++) {
            comboBoxes.get(i).setSelectedItem(comboBoxesText.get(i));
        }
    }


    /**
     * Get-method for JButton
     * @return reference to cancel-button.
     */
    public JButton getCancel() {
        return cancel;
    }

    /**
     * Get-method for JButton
     * @return reference to confirm-button.
     */
    public JButton getConfirm() {
        return confirm;
    }
}
