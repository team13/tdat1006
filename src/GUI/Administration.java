package GUI;

import DataEntities.*;
import Stuff.ErrorMessages;
import Stuff.GoogleMail;
import Stuff.PasswordAuthentication;
import Stuff.map.WebMap;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.basic.BasicBorders;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URI;
import java.util.ArrayList;

import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 * This program contains methods for eTramps administration program.
 * Through this program administrators will be able to administrate users
 * bikes, charging stations, workshops, repairs etc. (by using functions
 * as add, delete and edit).
 * <p>
 * The program provides a map functionality which gives an overview over
 * eTramps rental bikes and the charging stations available in Trondheim.
 */

public class Administration extends JFrame implements ActionListener {
    private PasswordAuthentication passAuth = new PasswordAuthentication();
    private boolean isLoggedIn = false;
    private JPanel header, menu, footer, main;
    private Box headerFlow;
    private Box homePanelBottom;
    private Box homePage;
    private JFrame frame;
    private JLabel headerText;
    private JLabel loginPaneError;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private DataHandler dataHandler = new DataHandler();
    private static String userName;
    private JTable table;
    private WebMap mapView;
    JComboBox<String> model_list = null;
    JComboBox<String> mf_list = null;
    JScrollPane displayTable = null;
    JScrollPane modelTabe = null;
    JScrollPane mananufTable = null;
    Image img;
    JPanel center;

    // Initialization
    public Administration() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setBackground(Color.LIGHT_GRAY);
        frame.setVisible(true);
        login();
    }

    /**
     * This method initializes the login-area for the administration program.
     */

    private void init() {
        frame.setSize(1200, 700);
        frame.setMinimumSize(new Dimension(750, 500));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout(10, 10));
        frame.setBackground(Color.LIGHT_GRAY);
        frame.setTitle("eTramp Admin Program");

        header = new JPanel();
        header.setLayout(new BoxLayout(header, BoxLayout.X_AXIS));

        menu = new JPanel();
        menu.setLayout(new BoxLayout(menu, BoxLayout.Y_AXIS));

        main = new JPanel(new BorderLayout(1, 1));

        headerText = new JLabel();
        headerText.setForeground(Color.black);
        headerText.setFont(new Font("Arial", Font.BOLD, 24));
        headerFlow = Box.createHorizontalBox();
        headerFlow.add(Box.createRigidArea(new Dimension(190, 30)));
        headerFlow.add(headerText);
        headerFlow.add(Box.createHorizontalGlue());
        header.setBackground(Color.decode("#CCCCCC"));
        header.setBorder(new BasicBorders.ButtonBorder(Color.WHITE, Color.WHITE, Color.GRAY, Color.GRAY));
        header.add(headerFlow);

        frame.add(header, BorderLayout.NORTH);
        frame.add(menu, BorderLayout.WEST);
        frame.add(main, BorderLayout.CENTER);
        frame.add(Box.createRigidArea(new Dimension(0, 0)), BorderLayout.EAST);

        frame.setVisible(true);
    }

    /**
     * This method creates the login screen to the administration program.
     * It is possible login with given username and password, including resetting your password.
     */

    private void login() {
        frame.setTitle("eTramp - User login");
        frame.setSize(1200, 700);
        frame.setMinimumSize(new Dimension(330, 350));
        JPanel loginPane = new JPanel();
        loginPane.setLayout(null);
        loginPane.setPreferredSize(new Dimension(300, 300));
        loginPane.setMinimumSize(new Dimension(300, 300));
        loginPane.setMaximumSize(new Dimension(300, 300));
        loginPane.setAlignmentX(Component.CENTER_ALIGNMENT);
        loginPane.setBackground(new Color(204, 204, 255));
        loginPane.setBorder(BorderFactory.createLineBorder(Color.decode("#AAAAAA"), 1));

        JLabel l2 = new JLabel("Username");
        JLabel l3 = new JLabel("Password");
        usernameField = new JTextField("");
        passwordField = new JPasswordField("");
        JButton btn1 = new JButton("Login");
        JButton btn2 = new JButton("Reset Password");

        JLabel loginPaneInfo = new JLabel("User login");
        loginPaneInfo.setFont(new Font("Arial", Font.BOLD, 20));

        loginPaneError = new JLabel("");
        loginPaneError.setFont(new Font("Arial", Font.PLAIN, 12));
        loginPaneError.setForeground(Color.decode("#DC143C"));

        loginPaneInfo.setBounds(20, 20, 260, 40);
        loginPaneError.setBounds(20, 190, 260, 90);
        l2.setBounds(20, 80, 60, 30);
        l3.setBounds(20, 120, 60, 30);
        usernameField.setBounds(90, 80, 200, 30);
        passwordField.setBounds(90, 120, 200, 30);
        btn1.setBounds(20, 170, 110, 40);
        btn2.setBounds(140, 170, 150, 40);

        loginPane.add(loginPaneInfo);
        loginPane.add(loginPaneError);
        loginPane.add(l2);
        loginPane.add(usernameField);
        loginPane.add(l3);
        loginPane.add(passwordField);
        loginPane.add(btn1);
        loginPane.add(btn2);

        /* addActionListener(e -> fucntion()); is a lambda expression. Java 8 or newer is required for lambda expressions work. */

        btn1.addActionListener(e -> onClickLogin());
        btn2.addActionListener(e -> onClickResetPassword());

        frame.setLayout(new FlowLayout());
        frame.add(loginPane);
        frame.revalidate();
        frame.repaint();
    }

    /**
     * This method creates functionality for user-logout.
     */

    private void logout() {
        isLoggedIn = false;
        main.removeAll();
        menu.removeAll();
        header.removeAll();
        frame.remove(main);
        frame.remove(menu);
        frame.remove(header);
        login();
    }

    /**
     * This method is a general method for cleaning screens.
     * This will be done at most occasions when creating new screens in
     * the admin-program. This method is therefore really handy for further use.
     */

    private void cleanPage() {
        main.removeAll();
        main.revalidate();
        main.repaint();
        main.removeAll();
    }

    /**
     * This method is running when clicking on login-button. The button has a actionlistener.
     * If correct username and password is given, the user will be transferred to the
     * administration program. If not you will be given an error message telling you
     * that the username / password combination is incorrect.
     */

    private void onClickLogin() {
        loginPaneError.setText(null);
        isLoggedIn = dataHandler.login(usernameField.getText(), passwordField.getText());
        userName = usernameField.getText();

        if (isLoggedIn) {
            frame.setVisible(false);
            frame.getContentPane().removeAll();
            init();
            addMenuButtons();
            addHeaderButtons();
            showHomePage();
            frame.repaint();
            frame.setVisible(true);
        } else {
            loginPaneError.setText("<html><p>The given user name and password combination </br> is incorrect. Please try again.</p></html>");
        }
    }

    /**
     * This method lets the user change his / her password, by entering the username and registered
     * e-mail. An email with a new, random generated password will be sent to the user.
     */

    private void onClickResetPassword() {
        String username = showInputDialog(null, "Please enter your username:");
        String epost = showInputDialog(null, "Please enter your e-mail:");
        boolean ok = Admins.checkEmail(username, epost);
        if (ok) {
            String newPass = DataHandler.getRandomPassword();
            String email_message = ("Hello!\n" +
                    "\n" +
                    "her is your new password:" +
                    "\n\n" +
                    "Your password: " + newPass + "\n" +
                    "\n" +
                    "If you have any questions, please feel free to contact us.\n" +
                    "\n" +
                    "Best wishes, \n" +
                    "The E-Tramp ElCycle Team\n" +
                    "\n");
            try {
                GoogleMail.Send("etramp.elsykkel", "heioghopp", epost, "Welcome to E-Tramp ElSykkel!", email_message);
                Admins.changePassword(username, newPass);
                showMessageDialog(main, "Please check your inbox");
            } catch (Exception emailerror) {
                ErrorMessages.errorDisplayErrorBox(emailerror, "An error occurred during the send email process, pleas contact your system administrator");
            }

        } else {
            showMessageDialog(null, "The email address you entered did not match our records");
        }

    }

    /**
     * This method availables the created buttons for the users.
     *
     * @param ae adds ActionEvent to the specific buttons
     */

    @Override
    public void actionPerformed(ActionEvent ae) {
        String command = ae.getActionCommand();
        if (isLoggedIn) {
            cleanPage();
            switch (command) {
                case "Home":
                    showHomePage();
                    break;
                case "Logout":
                    logout();
                    break;

                case "Repairs":
                    showRepairs();
                    break;
                case "Charging Stations":
                    showChargingStations();
                    break;
                case "Add station":
                    openAddStationPage();
                    break;
                case "Edit station":
                    openEditStationPage();
                    break;
                case "Delete station":
                    openDeleteStationPage();
                    break;
                case "Admin Users":
                    showAdmins();
                    break;
                case "Add Admin":
                    openAddAdminPage();
                    break;
                case "Edit Admin":
                    openEditAdminPage();
                    break;
                case "Delete Admin":
                    openDeleteAdminPage();
                    break;
                case "Members":
                    showMembers();
                    break;
                case "Change password":
                    openChangePasswordPage();
                    break;
                case "Delete member":
                    openDeleteMemberPage();
                    break;
                case "Bikes":
                    showBikes();
                    break;
                case "Repair Bike":
                    openRepairBikePage();
                    break;
                case "New bike":
                    openAddBikePage();
                    break;
                case "Edit bike":
                    openEditBikePage();
                    break;
                case "Delete bike":
                    openDeleteBikePage();
                    break;
                case "Add member":
                    openAddMemberPage();
                    break;
                case "Edit member":
                    openEditMemberPage();
                    break;
                case "Workshops":
                    showWorkShops();
                    break;
                case "Edit Workshop":
                    openEditWorkshopPage();
                    break;
                case "Add Workshop":
                    openAddWorkshopPage();
                    break;
                case "Delete Workshop":
                    openDeleteWorkshopPage();
                    break;
                case "Makes & models":
                    showMakesAndModels();
                    break;
                case "Add Make":
                    openAddMakePage();
                    break;
                case "Edit Make":
                    openEditMakePage();
                    break;
                case "Delete Make":
                    openDeleteMakePage();
                    break;
                case "Add Model":
                    openAddModelPage();
                    break;
                case "Edit Model":
                    openEditModelPage();
                    break;
                case "Delete Model":
                    openDeletModelPage();
                    break;
                case "Return Bike":
                    openReturnBike();
                    break;
                case "View Map":
                    openMapView();
                    break;
                case "Settings":
                    showSettingsPage();
                    break;
                case "Change Password":
                    openChangePasswordPage();
                    break;
                case "Age/gender":
                    showStat1();
                    break;
                case "Repair costs":
                    showStat2();
                    break;
                case "Power usage":
                    showStat3();
                    break;
                case "Number of renters":
                    showStat4();
                    break;
                case "Bike damage":
                    showStat5();
                    break;
                case "Total costs":
                    showStat6();
                    break;
                case "Quit":
                    System.exit(0);
                    break;
            }
        } else {
            showMessageDialog(main, "Undesignated button clicked. Pleae report error to the System Administrator. \nReturning to home screen.");
            showHomePage();
        }
    }

    /**
     * This is a general method for creating actionListeners to buttons in JPanel.
     *
     * @param buttonText We have chosen to create the "String[] buttonText"
     *                   to make it more easier and efficient when creating
     *                   new buttons for future methods.
     * @return The method is return a JPanel, which is creating the specific button.
     */

    private JPanel getEditButtons(String[] buttonText) {
        JPanel buttonGroup = new JPanel(new FlowLayout(FlowLayout.LEFT));
        for (int i = 0; i < buttonText.length; i++) {
            JButton a = new JButton(new String(buttonText[i]));
            a.addActionListener(this);
            buttonGroup.add(a);
        }
        return buttonGroup;
    }

    /**
     * This method is adding buttons to the header of the admin-GUI.
     * This contains a logout and settings button, including information
     * of current logged in user.
     */

    private void addHeaderButtons() {

        JButton logout = new JButton("Logout");
        JButton settings = new JButton("Settings");
        JLabel userNameLabel = new JLabel("Logged in as " + Admins.getCurrentUsersName(userName));

        settings.addActionListener(this);
        logout.addActionListener(this);

        headerFlow.add(userNameLabel);
        headerFlow.add(Box.createRigidArea(new Dimension(10, 0)));
        headerFlow.add(settings);
        headerFlow.add(Box.createRigidArea(new Dimension(5, 0)));
        headerFlow.add(logout);
        headerFlow.add(Box.createRigidArea(new Dimension(10, 0)));
    }

    /**
     * This method is adding the menu buttons to the admin-GUI.
     * These are: Home, bikes, charging stations, admin users, members, repairs,
     * workshops, makes & models, view map and quit. By clicking these you will be
     * directed to their respective areas.
     */

    private void addMenuButtons() {

        String[] buttons = {"Home", "Bikes", "Charging Stations", "Admin Users", "Members", "Repairs", "Workshops",
                "Makes & models", "View Map", "Quit"};
        int counter = 1;
        for (String button : buttons) {
            JButton tmp = new JButton(button);
            tmp.addActionListener(this);
            tmp.setMinimumSize(new Dimension(180, 39));
            tmp.setPreferredSize(new Dimension(180, 39));
            tmp.setMaximumSize(new Dimension(200, 50));


            String imgname = "menubut_" + counter + ".png";
            counter++;
            try {
                img = ImageIO.read(this.getClass().getResource(imgname));

                tmp.setIcon(new ImageIcon(img));
                tmp.setIconTextGap(4);
            } catch (Exception ex) {
                ErrorMessages.errorDisplayErrorBox(ex,"there was an error when reading images for menu buttons, please contact your systems administrator");
            }
            tmp.setHorizontalAlignment(SwingConstants.LEFT);
            menu.add(tmp);
            menu.add(Box.createRigidArea(new Dimension(10, 10)));
        }
        menu.add(Box.createVerticalGlue());

    }

    //////// Settings

    /**
     * This method creates the settings-page. It is using the "cleanPage();"- and
     * "getEditButtons();"- previously described. This will be added in the northern
     * part of the window.
     */

    public void showSettingsPage() {
        cleanPage();
        headerText.setText("Settings");
        String[] buttonText = {"Change Password"};
        JPanel underTable = new JPanel(new BorderLayout());
        underTable.add(getEditButtons(buttonText), BorderLayout.WEST);
        main.add(underTable, BorderLayout.NORTH);
    }

    /**
     * This method opens the page where you can change your password entering the old and new password.
     */
    public void openChangePasswordPage() {
        GUI.InputArea inputPanel = new GUI.InputArea(new String[]{"Old password: ", "New password: "});
        inputPanel.getCancel().addActionListener(e -> showSettingsPage());
        inputPanel.getConfirm().addActionListener(e -> {
            boolean ok = Admins.changePassword(inputPanel.getCurrentInputs(), userName);
            String txt = ok ? "Your password was changed" : "Password was not changed. ";
            showMessageDialog(main, txt);
        });
        main.add(inputPanel, BorderLayout.NORTH);
    }

    //////// HOME PAGE

    /**
     * This method is creating the homepage of the admin-GUI.
     */

    public void showHomePage() {
        headerText.setText("Homepage");
        JPanel homePanelTop = new JPanel(new GridLayout(1, 2));

        JPanel welcomeMessage = new JPanel(new BorderLayout());
        JPanel latestNews = new JPanel(new BorderLayout());

        JLabel welcome = new JLabel("Welcome to eTramp");
        welcome.setFont(new Font("Arial", Font.BOLD, 20));
        welcomeMessage.add(welcome, BorderLayout.NORTH);
        welcomeMessage.add(new JLabel("<html>" +
                "For information on how to use this system, please refer to the provided users manual. Alternatively, visit the eTramp GitLab Wiki page.."));

        JLabel news = new JLabel("Latest News!");
        news.setFont(new Font("Arial", Font.BOLD, 20));
        latestNews.add(news, BorderLayout.NORTH);
        latestNews.add(new JLabel("<html>Tue, April 17th: The system goes into final stage of development.\n" +
                "Thu, April 12th: Important 2nd team meeting to review progress.\n" +
                "Mon, March 26th: Easter holidays! Plenty of time to do more work. Or not.\n" +
                ""));

        homePanelTop.add(welcomeMessage);
        homePanelTop.add(latestNews);

        main.add(homePanelTop);

        homePage = new Box(1);
        homePage.add(homePanelTop);
        homePanelBottom = new Box(1);
        homePanelBottom.add(new DataEntities.Statistics("Age & Gender", "Members sorted by age and gender", "demographics"));
        homePage.add(homePanelBottom);


        main.add(homePage, BorderLayout.CENTER);
        main.add(getEditButtons(new String[]{"Age/gender", "Repair costs", "Power usage", "Number of renters", "Bike damage", "Total costs"}), BorderLayout.SOUTH);

    }

    /**
     * Six methods that creates graphs with statistics on the home page.
     * showStat1 fetches a DataEntity from the Statistics class that fetches
     * member information, sorted by age and gender.
     */
    private void showStat1() {
        homePanelBottom.removeAll();
        homePanelBottom.add(new DataEntities.Statistics("Age & Gender", "Members sorted by age and gender", "demographics"), BorderLayout.CENTER);
        main.add(homePage);
        main.add(getEditButtons(new String[]{"Age/gender", "Repair costs", "Power usage", "Number of renters", "Bike damage", "Total costs"}), BorderLayout.SOUTH);

    }

    /**
     * showStat2 looks through the repair history and displays a bar chart
     * where each bar represents the costs of repair for that month. Only 2017 is displayed.
     */

    private void showStat2() {
        homePanelBottom.removeAll();
        homePanelBottom.add(new DataEntities.Statistics("Repair Costs", "Monthly repair costs", "repaircost"), BorderLayout.CENTER);
        main.add(homePage);
        main.add(getEditButtons(new String[]{"Age/gender", "Repair costs", "Power usage", "Number of renters", "Bike damage", "Total costs"}), BorderLayout.SOUTH);

    }

    /**
     * When a bike is returned to a charging station, it is registered in a trip table, where the
     * remaining battery charge is noted. Based on this information, the method calculates how many
     * Watts the battery has produced.
     */

    private void showStat3() {
        homePanelBottom.removeAll();
        homePanelBottom.add(new DataEntities.Statistics("Power Usage", "Power Usage of 2017", "powerusage"), BorderLayout.CENTER);
        main.add(homePage);
        main.add(getEditButtons(new String[]{"Age/gender", "Repair costs", "Power usage", "Number of renters", "Bike damage", "Total costs"}), BorderLayout.SOUTH);
    }

    /**
     * As opposed to showStat, this statistics shows the age of the users who have actually
     * checked out and used a bike, as opposed to just who the members are.
     */

    private void showStat4() {
        homePanelBottom.removeAll();
        homePanelBottom.add(new DataEntities.Statistics("Age", "Number of bikes rented, by age group", "renters"), BorderLayout.CENTER);
        main.add(homePage);
        main.add(getEditButtons(new String[]{"Age/gender", "Repair costs", "Power usage", "Number of renters", "Bike damage", "Total costs"}), BorderLayout.SOUTH);
    }

    /**
     * All repairs are logged and the reason for repairs are shown here.
     */

    private void showStat5() {
        homePanelBottom.removeAll();
        homePanelBottom.add(new DataEntities.Statistics("Repair causes", "Causes for repair", "repairreason"), BorderLayout.CENTER);
        main.add(homePage);
        main.add(getEditButtons(new String[]{"Age/gender", "Repair costs", "Power usage", "Number of renters", "Bike damage", "Total costs"}), BorderLayout.SOUTH);
    }

    /**
     * This combines the costs of repair with the amount of Kilowatt-hours used and calculates
     * the monthy cost of running the rental service.
     */

    private void showStat6() {
        homePanelBottom.removeAll();
        homePanelBottom.add(new DataEntities.Statistics("Age", "Power and Repair Costs 2017", "powerrepaircosts"), BorderLayout.CENTER);
        main.add(homePage);
        main.add(getEditButtons(new String[]{"Age/gender", "Repair costs", "Power usage", "Number of renters", "Bike damage", "Total costs"}), BorderLayout.SOUTH);
    }

    //////// CURRENT ADMIN USER SETTINGS


    //////// BIKES

    /**
     * This method creates the overview of the respective bikes for rental. This includes colums like
     * Bike-ID, reg. #, make & model, color etc.
     * Buttons for new-, edit-, delete- and repair bike is also featured. Those will lead to a own
     * area where appropriate action is available.
     * The window do also have a feature for counting number of bikes registered in the system.
     */

    public void showBikes() {
        cleanPage();
        headerText.setText("Bicycle Overview");
        String[] columnBikesMenu = {"ID", "Reg. #", "Make", "Model", "Color", "Distance", "# of trips", "Prod. date", "Battery Level %", "C-Station", "Used by"};
        displayTable = dataHandler.getScrollPane("SELECT bicycle_id, registration_nr, manufacturer.name, model.name, color, ROUND(total_distance, 1) AS total_distance, nr_trips, " +
                "production_date, ROUND(battery_level, 1) AS battery_level, station_id, member_id FROM bicycle LEFT JOIN manufacturer ON bicycle.make_id = manufacturer.make_id LEFT JOIN" +
                " model ON bicycle.model_id = model.model_id", columnBikesMenu);

        JLabel bikeCounter = new JLabel("Current number of bicycles: " + Bike.countAllBikes());
        String[] buttonText = {"New bike", "Edit bike", "Delete bike", "Repair Bike"};

        JPanel underTable = new JPanel(new BorderLayout());
        underTable.add(getEditButtons(buttonText), BorderLayout.WEST);
        underTable.add(bikeCounter, BorderLayout.EAST);

        main.add(displayTable, BorderLayout.CENTER);
        main.add(underTable, BorderLayout.SOUTH);
    }

    /**
     * This method opens a tab for adding new bikes to the admin program. You will be able to add completely new
     * information to a new bike, with restrictions to the types of the respective variables.
     */

    public void openAddBikePage() {
        String[] temp = new String[]{
                "Registration ID: ",
                "Color:",
                "Total Distance: ",
                "# of trips: ",
                "Production Date:(yyyy-mm-dd) ",
                "Manufacturer: ",
                "Model: "};
        InputArea inputPanel = new InputArea(temp, "Manufacturer: ", "Model: ", Bike.getJComboBoxesBikes());
        inputPanel.getCancel().addActionListener(e -> showBikes());
        inputPanel.getConfirm().addActionListener(e -> {
            try {
//
                boolean ok = Bike.insertBikeIntoDatabase(inputPanel.getCurrentInputs());
                String asd = ok ? "Bike successfully entered into database." : "Bike not added\nDoes the Registration number already exist ?";
                showMessageDialog(main, asd);
                showBikes();
            } catch (NumberFormatException err) {
                showMessageDialog(main, "make sure you enter numbers where necessary");
            }
        });

        main.add(displayTable, BorderLayout.CENTER);
        main.add(inputPanel, BorderLayout.SOUTH);

    }

    /**
     * This method opens a tab for editing a specific chosen bike. A bike will be selected from
     * the bike table, by clicking the on the appropriate one. You will be able to add / edit completely
     * new information to the bike, with restrictions to the types of the respective variables.
     */

    public void openEditBikePage() {
        String[] temp = {"Registration ID: ",
                "Color:",
                "Total Distance: ",
                "# of trips: ",
                "Production Date:(yyyy-mm-dd) ",
                "Manufacturer: ",
                "Model: "};
        int bikeId = -1;
        try {
            JTable bikeTable = (JTable) displayTable.getViewport().getView();
            bikeId = (int) bikeTable.getValueAt(bikeTable.getSelectedRow(), 0);
            Bike bike = new Bike(bikeId);

            InputArea textInput = new InputArea(temp, "Manufacturer: ", "Model: ", Bike.getJComboBoxesBikes());

            ArrayList<String> comboBoxesTxt = new ArrayList<>();
            ArrayList<String> fieldsText = new ArrayList<String>();

            comboBoxesTxt.add(bike.getManufacturer());
            comboBoxesTxt.add(bike.getModel());
            fieldsText.add(bike.getRegnumber());
            fieldsText.add(bike.getColour());
            fieldsText.add(String.valueOf(bike.getTotal_mileage()));
            fieldsText.add(String.valueOf(bike.getNr_trips()));
            fieldsText.add(bike.getPurchase_date().toString());

            textInput.setFields(fieldsText, comboBoxesTxt);


            textInput.getCancel().addActionListener(e -> showBikes());
            textInput.getConfirm().addActionListener(e -> {
                boolean ok = Bike.editBikeInDatabase(textInput.getCurrentInputs(), bike.getBike_id());
                String txt = ok ? "Bike was edited successfully" : "Bike was NOT edited!";
                JOptionPane.showMessageDialog(null, txt);
                showBikes();
            });

            main.add(displayTable, BorderLayout.CENTER);
            main.add(textInput, BorderLayout.SOUTH);
        } catch (ArrayIndexOutOfBoundsException e) {
            showMessageDialog(null, "You have to select a bike that you want to edit.");
            showBikes();
        } catch (Exception e){
            ErrorMessages.errorDisplayErrorBox(e, "There was an error fetching data about bike " + String.valueOf(bikeId));
        }
    }

    /**
     * This method opens a window for deleting a specific bike. A bike will be selected from
     * the bike table, by clicking the on the appropriate one. When hitting the delete-button
     * a confirmation window will appear, and the user will be asked to confirm / decline the
     * operation.
     */

    public void openDeleteBikePage() {

        try {
            JTable table = (JTable) displayTable.getViewport().getView();
            int bikeId = (int) table.getValueAt(table.getSelectedRow(), 0);

            int security = JOptionPane.showConfirmDialog(main, "Are you sure you would like to delete bike with ID " + bikeId + "?");
            boolean ok = security == JOptionPane.YES_OPTION && Bike.deleteBikeFromDatabase(bikeId);
            String txt = ok ? "Bike successfully deleted from database. " : "Bike was not deleted";
            showMessageDialog(main, txt);
        } catch (ArrayIndexOutOfBoundsException e) {
            showMessageDialog(main, "You have to select a bike that you want to delete.");
        }
        showBikes();
    }

    /**
     * This method opens a tab where the user will be able to return av specific bike for repair.
     * A bike will be selected from the bike table, by clicking the on the appropriate one. When
     * confirming given information a confirmation window will appear, and the user will be asked
     * to confirm / decline the operation.
     */

    public void openRepairBikePage() {

        String[] inputNames = {
                "Estimated return:(yyyy-mm-dd) ",
                "Estimated cost (NOK): ",
                "Comment: ",
                "Workshop: ",
                "Reason: "};
        JComboBox<String> workshop_list = new JComboBox<String>(Workshops.getWorkshopList());      //Workshop list
        workshop_list.setSelectedIndex(0);
        JComboBox<String> repairReason_list = new JComboBox<String>(Repairs.getRepairReasons());      //Workshop list
        repairReason_list.setSelectedIndex(0);

        ArrayList<JComboBox<String>> comboBoxes = new ArrayList();
        comboBoxes.add(workshop_list);
        comboBoxes.add(repairReason_list);

        InputArea textPanel = new InputArea(inputNames, "Workshop: ", "Reason: ", comboBoxes);
        textPanel.getCancel().addActionListener(e -> showBikes());
        textPanel.getConfirm().addActionListener(e -> {
            try {
                JTable table = (JTable) displayTable.getViewport().getView();
                int bicycleId = (int) table.getValueAt(table.getSelectedRow(), 0);

                boolean ok = Bike.sendBikeToRepair(textPanel.getCurrentInputs(), bicycleId);
                String txt = ok ? "Bike successfully sent. " : "Bike was not sent. ";
                showMessageDialog(main, txt);
                showBikes();
            } catch (ArrayIndexOutOfBoundsException err) {
                ErrorMessages.errorNoItemSelected("bike");
                showBikes();
            } catch (NumberFormatException err) {
                ErrorMessages.errorNumberFormat();

            }
        });

        main.add(displayTable, BorderLayout.CENTER);
        main.add(textPanel, BorderLayout.SOUTH);
    }


    //////// CHARGING STATIONS

    /**
     * This method creates the overview of the existing charging stations. This includes colums like
     * Station-ID, street name, street number, capacity etc.
     * Buttons for new-, edit- and delete station is also featured. Those will lead to a own
     * area where appropriate actions is available.
     */

    public void showChargingStations() {
        cleanPage();
        headerText.setText("Charging Stations");
        String[] columnNamesStation = {"ID", "Street name", "Street #", "Postal #", "Capacity", "Vacancy", "Latitude", "Longitude"};
        displayTable = dataHandler.getScrollPane("SELECT station_id,street_name,street_nr,postnr,capacity,vacancy,lat,lon FROM charging_station", columnNamesStation);

        main.add(displayTable, BorderLayout.CENTER);
        String[] buttons = new String[]{"Add station", "Edit station", "Delete station"};
        main.add(getEditButtons(buttons), BorderLayout.SOUTH);
    }

    /**
     * This method opens a tab for adding new charging stations to the admin program. You will be able to add
     * completely new information to a new station, with restrictions to the types of the respective variables.
     */

    public void openAddStationPage() {
        String[] temp = new String[]{"Street name: ", "Street number: ", "Post number: ",
                "Capacity: ", "Pos X: ", "Pos Y: ", "Elevation: ", "Description: "};

        InputArea textInput = new InputArea(temp);
        textInput.getCancel().addActionListener(e -> showChargingStations());
        main.add(displayTable, BorderLayout.CENTER);
        main.add(textInput, BorderLayout.SOUTH);

        textInput.getConfirm().addActionListener(e -> {
            try {
                boolean ok = ChargingStation.insertChargingStationIntoDatabase(textInput.getCurrentInputs());
                String asd = ok ? ErrorMessages.successAdd("Charging station") : ErrorMessages.failedAdd("Charging station");
                showMessageDialog(main, asd);
                showChargingStations();
            } catch (NumberFormatException err) {
                ErrorMessages.errorNumberFormat();
            }
        });

    }

    /**
     * This method opens a tab for editing a specific chosen charging station. A station will be selected
     * from the bike table, by clicking the on the appropriate one. You will be able to add / edit
     * completely new information to the station, with restrictions to the types of the respective variables.
     */

    public void openEditStationPage() {

        try {
            JTable stationTable = (JTable) displayTable.getViewport().getView();
            int station_Id = (int) stationTable.getValueAt(stationTable.getSelectedRow(), 0);

            String[] fieldNames = {"Street Name: ", "Street Number: ",
                    "Postnr: ", "Capacity: ", "Pos X: ", "Pos Y: ", "Elevation: ", "Description: "};
            InputArea textPanel = new InputArea(fieldNames);
            ChargingStation station = new ChargingStation(station_Id);
            ArrayList<String> fieldValues = new ArrayList<>();

            fieldValues.add(station.getStreet_name());
            fieldValues.add(station.getStreet_number());
            fieldValues.add(String.valueOf(station.getPostal()));
            fieldValues.add(String.valueOf(station.getMax_bikes()));
            fieldValues.add(String.valueOf(station.getPosition().getLatitude()));
            fieldValues.add(String.valueOf(station.getPosition().getLongitude()));
            fieldValues.add(String.valueOf(station.getPosition().getElevation()));
            fieldValues.add(station.getName());
            textPanel.setFields(fieldValues, null);
            textPanel.getCancel().addActionListener(e -> showChargingStations());
            textPanel.getConfirm().addActionListener(e -> {
                try {
                    boolean ok = ChargingStation.editChargingStationInDatabase(textPanel.getCurrentInputs(), station_Id);
                    String asd = ok ? ErrorMessages.successEdit("Charging station") : ErrorMessages.failedEdit("Charging station");
                    showMessageDialog(main, asd);
                    showChargingStations();
                } catch (NumberFormatException err) {
                    showMessageDialog(main, ErrorMessages.errorNumberFormat());
                }
            });
            main.add(displayTable, BorderLayout.CENTER);
            main.add(textPanel, BorderLayout.SOUTH);
        } catch (ArrayIndexOutOfBoundsException e) {
            showMessageDialog(main, ErrorMessages.errorNoItemSelected("Charging station"));
            showChargingStations();
        }
    }

    /**
     * This method opens a window for deleting a specific charging station. A station will be selected from
     * the station table, by clicking the on the appropriate one. When hitting the delete-button
     * a confirmation window will appear, and the user will be asked to confirm / decline the
     * operation.
     */

    public void openDeleteStationPage() {
        try {

            JTable table = (JTable) displayTable.getViewport().getView();
            int stationId = (int) table.getValueAt(table.getSelectedRow(), 0);
            boolean ok = ChargingStation.deleteChargingStationFromDatabase(stationId);
            String asd = ok ? "Charging station successfully deleted form database." : "Charging station was not deleted from the database.";
            showMessageDialog(main, asd);
            showChargingStations();
        } catch (IndexOutOfBoundsException e) {
            showMessageDialog(main, "No charging station selected. Please select a Charging station and try again.");
            showChargingStations();
        }
    }

    //////// ADMIN

    /**
     * This method creates the overview of the respective admin users. This includes colums like
     * User-ID, firstname, surname, birthyear etc.
     * Buttons for new-, edit- and delete admin is also featured. Those will lead to a own
     * area where appropriate action is available.
     */

    public void showAdmins() {
        cleanPage();
        headerText.setText("Admin users");
        String[] columnNamesAdmins = {"ID", "Fornavn", "Etternavn", "Birthyear", "Email", "Phone #", "Username"};
        displayTable = dataHandler.getScrollPane("SELECT user_id, firstname,surname,birthyear,email,phone,username FROM admin_users", columnNamesAdmins);

        main.add(displayTable, BorderLayout.CENTER);
        main.add(getEditButtons(new String[]{"Add Admin", "Edit Admin", "Delete Admin"}), BorderLayout.SOUTH);
    }

    /**
     * This method opens a tab for adding new admin users to the admin program. You will be able to add
     * completely new information to a new user, with restrictions to the types of the respective variables.
     * When confirming the creation of the user, a e-mail will be sent to the given address with necessary
     * user information like username and password.
     */

    public void openAddAdminPage() {

        InputArea inputPanel = new InputArea(new String[]{"Surname: ", "First Name: ",
                "Birth year: (yyyy)", "E-mail: ", "Phone: ", "Username: ", "Password: ",
                "Repeat Password: "});


        main.add(displayTable, BorderLayout.CENTER);
        main.add(inputPanel, BorderLayout.SOUTH);
        inputPanel.getCancel().addActionListener(e -> showAdmins());
        inputPanel.getConfirm().addActionListener(e -> {
            ArrayList<String> inputs = inputPanel.getCurrentInputs();

            if (inputs.get(6).equals(inputs.get(7))) {
                try {
                    boolean ok = Admins.insertNewAdmin(inputs);
                    if (ok) {
                        showMessageDialog(main, "Admin user successfully added.");
                        String email_message = ("Hello!\n" +
                                "\n" +
                                "Welcome to E-tramp. You are recieving this email because you have been added as a system user at E-Tramp Elsykkel.\n" +
                                "You will shortly be recieving the software needed to access our administative system.\n" +
                                "\n\n" +
                                "Your username: " + inputs.get(5) + "\n" +
                                "Your password: " + inputs.get(6) + "\n" +
                                "\n" +
                                "If you have any questions, please feel free to contact us.\n" +
                                "\n" +
                                "Best wishes, \n" +
                                "The E-Tramp ElCycle Team\n" +
                                "\n");
                        try {
                            GoogleMail.Send("etramp.elsykkel", "heioghopp", inputs.get(3), "Welcome to E-Tramp ElSykkel!", email_message);
                        } catch (Exception emailerror) {
                            ErrorMessages.errorDisplayErrorBox(emailerror, "There was an error sending email. Please contact the System Administrator.");
                        }
                    } else {
                        showMessageDialog(main, "An error occured during creation of new admin user. Pleae contact the system administrator.");
                    }
                } catch (NumberFormatException er) {
                    showMessageDialog(main, "Number format conversion error. Please ensure that fields that require numeric values have numbers inserted.");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Passwords do not match.");
            }
            showAdmins();
        });
    }

    /**
     * This method opens a tab for editing a specific chosen admin user. A user will be selected
     * from the user table, by clicking the on the appropriate one. You will be able to add / edit
     * completely new information to the user, with restrictions to the types of the respective variables.
     */

    public void openEditAdminPage() {
        int admin_id = 0;
        String[] fields = {"First Name: ", "Surname: ", "Birth Year: (yyyy)", "E-Mail: ", "Phone ", "Username: "};
        try {
            JTable adminTable = (JTable) displayTable.getViewport().getView();
            admin_id = (int) adminTable.getValueAt(adminTable.getSelectedRow(), 0);

            Admins admin = new Admins(admin_id);
            ArrayList<String> fieldsValues = new ArrayList<>();
            fieldsValues.add(admin.getFirstname());
            fieldsValues.add(admin.getLastname());
            fieldsValues.add(String.valueOf(admin.getBirthyear()));
            fieldsValues.add(admin.getEmail());
            fieldsValues.add(String.valueOf(admin.getPhone()));
            fieldsValues.add(admin.getUsername());


            InputArea inputPanel = new InputArea(fields);
            inputPanel.setFields(fieldsValues, null);
            main.add(displayTable, BorderLayout.CENTER);
            main.add(inputPanel, BorderLayout.SOUTH);
            inputPanel.getCancel().addActionListener(e -> showAdmins());
            inputPanel.getConfirm().addActionListener(e -> {
                try {
                    boolean ok = Admins.editAdminInDatabase(inputPanel.getCurrentInputs(), admin.getAdmin_id());

                    String asd = ok ? "Admin user was edited successfully" : "Admin user was NOT edited";
                    showMessageDialog(main, asd);
                    showAdmins();
                } catch (NumberFormatException err) {
                    showMessageDialog(main, "Number format conversion error. Please ensure that fields that require numeric values have numbers inserted.");
                }

            });

        } catch (ArrayIndexOutOfBoundsException e) {
            showMessageDialog(null, "No admin user selected. Please select an admin user and try again.");
            showAdmins();
        }

    }

    /**
     * This method opens a window for deleting a specific admin user. A user will be selected from
     * the user table, by clicking the on the appropriate one. When hitting the delete-button
     * a confirmation window will appear, and the user will be asked to confirm / decline the
     * operation.
     */

    public void openDeleteAdminPage() {
        try {
            JTable table = (JTable) displayTable.getViewport().getView();
            int adminId = (int) table.getValueAt(table.getSelectedRow(), 0);
            boolean ok = Admins.deleteAdminFromDatabase(adminId);
            String txt = ok ? "Admin successfully deleted from database. " : "Admin was not deleted from the database";
            showMessageDialog(main, txt);
            showAdmins();
        } catch (ArrayIndexOutOfBoundsException e) {
            showMessageDialog(null, "No admin user selected. Please select an admin user and try again.");
        }

        showAdmins();
    }

    //////// REPAIRS

    /**
     * This method creates the overview of the respective bikes sent to repair. This includes colums like
     * Repair-ID, bike, workshop ID, reason etc.
     * Buttons for return bike to eTramp HQ is also featured. This will lead to a own
     * area where appropriate action is available.
     * The window do also have a feature for counting number of bikes returned for repair.
     */

    public void showRepairs() {
        headerText.setText("Repairs");
        String[] columnNames = {"Repair ID", "Bike", "Workshop", "Reason", "Comment", "Estimated cost", "Estimated return date"};
        displayTable = dataHandler.getScrollPane("SELECT distinct repair_id, bicycle_id, name, type,comment, est_cost, est_return FROM repair NATURAL JOIN workshop Join repair_type ON reptype_id=reason;", columnNames);
        main.add(displayTable, BorderLayout.CENTER);


        JPanel underMainTable = new JPanel(new BorderLayout());
        underMainTable.add(getEditButtons(new String[]{"Return Bike"}), BorderLayout.WEST);
        underMainTable.add(new JLabel("Current number of bikes at repair: " + Bike.countBikesAtRepair()), BorderLayout.EAST);
        main.add(underMainTable, BorderLayout.SOUTH);
        main.repaint();
        main.revalidate();

    }

    /**
     * This method makes it possible to return bikes back to eTramp HQ. When hitting the return button
     * the user will be asked to confirm the action. By htting the confirm button, the bike will be sent
     * back to the bike table, and placed in charging station 1 by default. Charging station 1 equals
     * eTramp HQ.
     */

    public void openReturnBike() {

        try {
            JTable table = (JTable) displayTable.getViewport().getView();
            int bikeId = (int) table.getValueAt(table.getSelectedRow(), 1);
            boolean ok = Bike.returnBikeFromRepair(bikeId);
            String txt = ok ? "Bike successfully returned to eTramp HQ. " : "Bike was not returned to eTramp HQ. ";
            showMessageDialog(main, txt);
            showRepairs();
        } catch (ArrayIndexOutOfBoundsException e) {
            showMessageDialog(null, ErrorMessages.errorNoItemSelected("repair petition"));
        }
    }

    //////// WORKSHOPS

    /**
     * This method creates the overview of the respective workshops. This includes colums like
     * Workshop-ID, name, address, phone # etc.
     * Buttons for add-, edit- and delete workshop is also featured. This will lead to a own
     * area where appropriate action is available.
     */

    public void showWorkShops() {
        cleanPage();
        headerText.setText("Workshops");
        String[] columnNamesWorkshops = {"ID", "Name", "Adress", "Phone #", "Comments"};
        displayTable = dataHandler.getScrollPane("Select * from workshop", columnNamesWorkshops);
        main.add(displayTable, BorderLayout.CENTER);
        main.add(getEditButtons(new String[]{"Add Workshop", "Edit Workshop", "Delete Workshop"}), BorderLayout.SOUTH);
    }

    /**
     * This method opens a tab for adding new workshops to the admin program. You will be able to add
     * completely new information to a new workshop, with restrictions to the types of the respective variables.
     */

    public void openAddWorkshopPage() {

        String[] fields = {"Name: ", "Adress: ", "Phonenumber: ", "Comments: "};
        InputArea inputPanel = new InputArea(fields);
        inputPanel.getCancel().addActionListener(e -> showWorkShops());
        inputPanel.getConfirm().addActionListener(e -> {
            try {
                boolean ok = Workshops.AddWorkshopToDatabase(inputPanel.getCurrentInputs());
                String asd = ok ? ErrorMessages.successAdd("Workshop") : ErrorMessages.failedAdd("Workshop");
                showMessageDialog(main, asd);
                showWorkShops();
            } catch (NumberFormatException err) {
                showMessageDialog(main, ErrorMessages.errorNumberFormat());
            }
        });

        main.add(displayTable, BorderLayout.CENTER);
        main.add(inputPanel, BorderLayout.SOUTH);

    }

    /**
     * This method opens a tab for editing a specific chosen workshop. A workshop will be selected
     * from the workshop table, by clicking the on the appropriate one. You will be able to add / edit
     * completely new information to the workshop, with restrictions to the types of the respective variables.
     */

    public void openEditWorkshopPage() {
        int workshopId = 0;
        try {
            JTable Table = (JTable) displayTable.getViewport().getView();
            workshopId = (int) Table.getValueAt(Table.getSelectedRow(), 0);
            Workshops workshop = new Workshops(workshopId);
            ArrayList<String> fieldsValues = new ArrayList<>();
            fieldsValues.add(workshop.getName());
            fieldsValues.add(workshop.getAdress());
            fieldsValues.add(String.valueOf(workshop.getPhone()));
            fieldsValues.add(workshop.getComments());

            String[] fields = {"Name: ", "Adress: ", "Phonenumber: ", "Comments: "};
            InputArea inputPanel = new InputArea(fields);
            inputPanel.setFields(fieldsValues, null);
            main.add(displayTable, BorderLayout.CENTER);
            main.add(inputPanel, BorderLayout.SOUTH);
            inputPanel.getCancel().addActionListener(e -> showWorkShops());
            inputPanel.getConfirm().addActionListener(e -> {
                try {
                    boolean ok = Workshops.editWorkshopInDatabase(inputPanel.getCurrentInputs(), workshop.getWorkshopID());
                    String asd = ok ? ErrorMessages.successEdit("Workshop") : ErrorMessages.failedEdit("Workshop");
                    showMessageDialog(main, asd);
                    showWorkShops();
                } catch (NumberFormatException err) {
                    showMessageDialog(main, "make sure you enter numbers where necessary");
                }
            });


        } catch (ArrayIndexOutOfBoundsException e) {
            showMessageDialog(null, ErrorMessages.errorNoItemSelected("workshop"));
            showWorkShops();
        }
    }

    /**
     * This method opens a window for deleting a specific workshop. A workshop will be selected from
     * the workshop table, by clicking the on the appropriate one. When hitting the delete-button
     * a confirmation window will appear, and the user will be asked to confirm / decline the
     * operation.
     */

    public void openDeleteWorkshopPage() {
        try {
            JTable table = (JTable) displayTable.getViewport().getView();
            int wsId = (int) table.getValueAt(table.getSelectedRow(), 0);

            boolean ok = Workshops.deleteWorkshopFromDatabase(wsId);
            String txt = ok ? ErrorMessages.successDelete("Workshop") : ErrorMessages.failedDelete("Workshop");
            showMessageDialog(main, txt);
            showWorkShops();
        } catch (ArrayIndexOutOfBoundsException e) {
            showMessageDialog(null, ErrorMessages.errorNoItemSelected("workshop"));
        }
    }

    //////// MEMBERS

    /**
     * This method creates the overview of the respective members (customers). This includes colums like
     * Member-ID, surname, firstname, gender  etc.
     * Buttons for add-, edit- and delete members is also featured. This will lead to a own
     * area where appropriate action is available.
     * The window do also have a feature for counting number of members registered in the system.
     */

    public void showMembers() {
        cleanPage();
        headerText.setText("Members Overview");
        String[] columnNamesMem = {"ID", "Surname", "Name", "Gender", "Born", "Email", "Phone #"};
        displayTable = dataHandler.getScrollPane("SELECT member_id,surname,firstname,gender,birthyear,email,phone from member", columnNamesMem);

        //Label that shows number of members
        JLabel nrOfMembers = new JLabel("Current number of members: " + Member.getNumberOfMembers());
        JPanel underMainTAble = new JPanel(new BorderLayout());
        underMainTAble.add(nrOfMembers, BorderLayout.EAST);
        String[] editButtons = {"Add member", "Edit member", "Delete member"};
        underMainTAble.add(getEditButtons(editButtons), BorderLayout.WEST);

        main.add(displayTable, BorderLayout.CENTER);
        main.add(underMainTAble, BorderLayout.SOUTH);
        main.repaint();
    }

    /**
     * This method opens a tab for adding new members to the admin program. You will be able to add
     * completely new information to a new member, with restrictions to the types of the respective variables.
     */

    public void openAddMemberPage() {
        String[] fields = {"Firstname: ", "Surname: ", "Birthyear:(yyyy) ", "Email: ", "Phone number: ", "Gender: "};
        ArrayList<JComboBox<String>> comboBoxes = new ArrayList<>();
        comboBoxes.add(new JComboBox<String>(new String[]{"Male", "Female"}));
        InputArea inputPanel = new InputArea(fields, "Gender: ", null, comboBoxes);
        inputPanel.getCancel().addActionListener(e -> showMembers());
        inputPanel.getConfirm().addActionListener(e -> {
            try {
                boolean ok = Member.addMemberToDatabase(inputPanel.getCurrentInputs());
                String txt = ok ? ErrorMessages.successAdd("Member") : ErrorMessages.failedAdd("Member");
                showMessageDialog(main, txt);
                showMembers();
            } catch (NumberFormatException er) {
                showMessageDialog(main, ErrorMessages.errorNumberFormat());
            }
        });
        main.add(displayTable, BorderLayout.CENTER);
        main.add(inputPanel, BorderLayout.SOUTH);


    }

    /**
     * This method opens a window for deleting a specific member. A member will be selected from
     * the member table, by clicking the on the appropriate one. When hitting the delete-button
     * a confirmation window will appear, and the user will be asked to confirm / decline the
     * operation.
     */

    public void openDeleteMemberPage() {
        try {
            JTable table = (JTable) displayTable.getViewport().getView();
            int memberId = (int) table.getValueAt(table.getSelectedRow(), 0);
            boolean ok = Member.deleteMemberFromDatabase(memberId);
            String txt = ok ? ErrorMessages.successDelete("Member") : ErrorMessages.failedDelete("Member");
            showMessageDialog(main, txt);
            showMembers();
        } catch (ArrayIndexOutOfBoundsException e) {
            showMessageDialog(null, ErrorMessages.errorNoItemSelected("member"));
            showMembers();
        }

    }

    /**
     * This method opens a tab for editing a specific chosen member. A member will be selected
     * from the member table, by clicking the on the appropriate one. You will be able to add / edit
     * completely new information to the member, with restrictions to the types of the respective variables.
     */

    public void openEditMemberPage() {
        try {
            JTable table = (JTable) displayTable.getViewport().getView();
            int memberId = (int) table.getValueAt(table.getSelectedRow(), 0);
            Member member = new Member(memberId);
            String[] fields = {"Firstname: ", "Lastname: ", "Birthyear:(yyyy) ", "Email: ", "Phone number: ", "Gender: "};

            ArrayList<JComboBox<String>> comboBoxes = new ArrayList<>();
            comboBoxes.add(new JComboBox<String>(new String[]{"Male", "Female"}));

            ArrayList<String> fieldValues = new ArrayList<>();
            fieldValues.add(member.getFirstname());
            fieldValues.add(member.getLastname());
            fieldValues.add(String.valueOf(member.getBirthyear()));
            fieldValues.add(member.getEmail());
            fieldValues.add(String.valueOf(member.getPhone()));
            ArrayList<String> comboboxString = new ArrayList<>();
            comboboxString.add(member.isMale() ? "Male" : "Female");

            InputArea inputPanel = new InputArea(fields, "Gender: ", null, comboBoxes);
            inputPanel.setFields(fieldValues, comboboxString);
            inputPanel.getCancel().addActionListener(e -> showMembers());
            inputPanel.getConfirm().addActionListener(e -> {
                try {
                    boolean ok = Member.editMemberInDatabase(inputPanel.getCurrentInputs(), member.getMemberID());
                    String txt = ok ? ErrorMessages.successEdit("Member") : ErrorMessages.failedEdit("Member");
                    showMessageDialog(main, txt);
                    showMembers();
                } catch (NumberFormatException err) {
                    showMessageDialog(main, "make sure you enter numbers where necessary");
                }
            });
            main.add(displayTable, BorderLayout.CENTER);
            main.add(inputPanel, BorderLayout.SOUTH);
        } catch (ArrayIndexOutOfBoundsException e) {
            showMessageDialog(null, ErrorMessages.errorNoItemSelected("member"));
            showMembers();
        }

    }

    //////// MAKE AND MODELS

    /**
     * This method creates the overview of the respective makes and models for the bikes. To make the
     * overview more arranged and tidy, we have decided to split makes and models into two tables. As
     * every manufacturer consists of many different models, we saw this a good solution to make the
     * overview better.
     * <p>
     * Buttons for add-, edit- and delete for both makes and models is also featured. This will lead to a own
     * area where appropriate action is available.
     */

    public void showMakesAndModels() {
        cleanPage();
        headerText.setText("Make / Model Overview");
        String[] columnManufHead = {"ID", "Manufacturer", "Country"};
        String[] columnModelHead = {"ID", "make"};
        mananufTable = dataHandler.getScrollPane("SELECT * FROM manufacturer", columnManufHead);


        JTable manTable = (JTable) mananufTable.getViewport().getView();
        manTable.getSelectionModel().setSelectionInterval(0, 0);

        int manufID = (int) manTable.getValueAt(manTable.getSelectedRow(), 0);
        String query = "select model_id, name from model where make=" + manufID;

        modelTabe = dataHandler.getScrollPane(query, columnModelHead);
        center = new JPanel(new GridLayout(1, 2));
        center.add(mananufTable);
        center.add(modelTabe);

        manTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int manufID = (int) manTable.getValueAt(manTable.getSelectedRow(), 0);
                String query = "select model_id, name from model where make=" + manufID;
                center.remove(modelTabe);
                modelTabe = dataHandler.getScrollPane(query, columnModelHead);
                center.add(modelTabe);
                center.revalidate();
                center.repaint();
            }
        });

        JPanel underMainTables = new JPanel(new GridLayout(1, 2));
        underMainTables.add(getEditButtons(new String[]{"Add Make", "Edit Make", "Delete Make"}));
        underMainTables.add(getEditButtons(new String[]{"Add Model", "Edit Model", "Delete Model"}));

        main.add(center, BorderLayout.CENTER);
        main.add(underMainTables, BorderLayout.SOUTH);
    }

    /**
     * This method opens a tab for adding new makes to the admin program. You will be able to add
     * completely new information to a new make, with restrictions to the types of the respective variables.
     */

    public void openAddMakePage() {

        InputArea inputPanel = new InputArea(new String[]{"Name: ", "Country: "});
        inputPanel.getCancel().addActionListener(e -> showMakesAndModels());
        inputPanel.getConfirm().addActionListener(e -> {
            boolean ok = Manufacturer.addManufacturerToDatabase(inputPanel.getCurrentInputs());
            String txt = ok ? ErrorMessages.successAdd("Manufacturer") : ErrorMessages.failedAdd("Manufacturer");
            showMessageDialog(main, txt);
            showMakesAndModels();
        });
        main.add(center, BorderLayout.CENTER);
        main.add(inputPanel, BorderLayout.SOUTH);
    }

    /**
     * This method opens a tab for editing a specific chosen make. A make will be selected
     * from the make table, by clicking the on the appropriate one. You will be able to add / edit
     * completely new information to the make, with restrictions to the types of the respective variables.
     */

    public void openEditMakePage() {
        try {
            JTable table = (JTable) mananufTable.getViewport().getView();
            int manufID = (int) table.getValueAt(table.getSelectedRow(), 0);
            Manufacturer manuf = new Manufacturer(manufID);
            ArrayList<String> fieldValues = new ArrayList<>();
            fieldValues.add(manuf.getName());
            fieldValues.add(manuf.getCountry());

            InputArea inputPanel = new InputArea(new String[]{"Name: ", "Country: "});
            inputPanel.setFields(fieldValues, null);
            inputPanel.getCancel().addActionListener(e -> showMakesAndModels());
            inputPanel.getConfirm().addActionListener(e -> {
                boolean ok = Manufacturer.editManufacturerInDatabaes(inputPanel.getCurrentInputs(), manuf.getManufID());
                String txt = ok ? ErrorMessages.successEdit("Manufacturer") : ErrorMessages.failedEdit("Manufacturer");
                showMessageDialog(main, txt);
                showMakesAndModels();
            });
            main.add(center, BorderLayout.CENTER);
            main.add(inputPanel, BorderLayout.SOUTH);
        } catch (ArrayIndexOutOfBoundsException e) {
            showMessageDialog(null, ErrorMessages.errorNoItemSelected("manufacturer"));
            showMakesAndModels();
        }
    }

    /**
     * This method opens a window for deleting a specific make. A make will be selected from
     * the make table, by clicking the on the appropriate one. When hitting the delete-button
     * a confirmation window will appear, and the user will be asked to confirm / decline the
     * operation.
     */

    public void openDeleteMakePage() {
        try {
            JTable table = (JTable) mananufTable.getViewport().getView();
            int manufID = (int) table.getValueAt(table.getSelectedRow(), 0);
            boolean ok = Manufacturer.deleteManufacturerFromDatabase(manufID);
            String txt = ok ? ErrorMessages.successDelete("Manufacturer") : ErrorMessages.failedDelete("Manufacturer");
            showMessageDialog(main, txt);
            showMakesAndModels();
        } catch (ArrayIndexOutOfBoundsException e) {
            showMessageDialog(null, ErrorMessages.errorNoItemSelected("manufacturer"));
            showMakesAndModels();
        }

    }

    /**
     * This method opens a tab for adding new models to the admin program. You will be able to add
     * completely new information to a new model, with restrictions to the types of the respective variables.
     */

    public void openAddModelPage() {
        ArrayList<JComboBox<String>> manufBox = new ArrayList<>();
        manufBox.add(new JComboBox<>(Manufacturer.getManufacturerList()));
        InputArea inputPanel = new InputArea(new String[]{"Name: ", "Manufacturer: "}, "Manufacturer: ", null, manufBox);
        inputPanel.getCancel().addActionListener(e -> showMakesAndModels());
        inputPanel.getConfirm().addActionListener(e -> {
            boolean ok = Model.insertModelIntoDatabase(inputPanel.getCurrentInputs());
            String txt = ok ? ErrorMessages.successAdd("Model") : ErrorMessages.failedAdd("Model");
            showMessageDialog(null, txt);
            showMakesAndModels();
        });
        main.add(center, BorderLayout.CENTER);
        main.add(inputPanel, BorderLayout.SOUTH);
    }

    /**
     * This method opens a tab for editing a specific chosen model. A model will be selected
     * from the model table, by clicking the on the appropriate one. You will be able to add / edit
     * completely new information to the model, with restrictions to the types of the respective variables.
     */

    public void openEditModelPage() {
        try {
            JTable table = (JTable) modelTabe.getViewport().getView();
            int modelID = (int) table.getValueAt(table.getSelectedRow(), 0);
            Model model = new Model(modelID);
            ArrayList<String> fieldValues = new ArrayList<>();
            ArrayList<String> comboBoxValue = new ArrayList<>();
            fieldValues.add(model.getName());
            comboBoxValue.add(model.getManufacturer());

            ArrayList<JComboBox<String>> manufBox = new ArrayList<>();
            manufBox.add(new JComboBox<>(Manufacturer.getManufacturerList()));
            InputArea inputPanel = new InputArea(new String[]{"Name: ", "Manufacturer: "}, "Manufacturer: ", null, manufBox);
            inputPanel.setFields(fieldValues, comboBoxValue);

            inputPanel.getCancel().addActionListener(e -> showMakesAndModels());
            inputPanel.getConfirm().addActionListener(e -> {
                boolean ok = Model.editModelInDatabase(inputPanel.getCurrentInputs(), model.getModelID());
                String txt = ok ? ErrorMessages.successEdit("Model") : ErrorMessages.failedEdit("Model");
                showMessageDialog(null, txt);
                showMakesAndModels();
            });
            main.add(center, BorderLayout.CENTER);
            main.add(inputPanel, BorderLayout.SOUTH);
        } catch (ArrayIndexOutOfBoundsException e) {
            showMessageDialog(null, ErrorMessages.errorNoItemSelected("model"));
            showMakesAndModels();
        }
    }

    /**
     * This method opens a window for deleting a specific model. A model will be selected from
     * the model table, by clicking the on the appropriate one. When hitting the delete-button
     * a confirmation window will appear, and the user will be asked to confirm / decline the
     * operation.
     */

    public void openDeletModelPage() {
        try {
            JTable table = (JTable) modelTabe.getViewport().getView();
            int modelId = (int) table.getValueAt(table.getSelectedRow(), 0);
            boolean ok = Model.deletModelFromDatabase(modelId);
            String txt = ok ? ErrorMessages.successEdit("Model") : ErrorMessages.successEdit("Model");
            showMessageDialog(null, txt);
            showMakesAndModels();
        } catch (ArrayIndexOutOfBoundsException e) {
            showMessageDialog(main, ErrorMessages.errorNoItemSelected("model"));
            showMakesAndModels();
        }
    }

    ///////// Map View

    /**
     * This method opens the map window. Our map information is gathered from Google. The map shows an overview
     * of our rented bicycles including the charging stations.
     */

    public void openMapView() {
        headerText.setText("Map View");
        final JFXPanel fx_child = new JFXPanel();

        Platform.runLater(new Runnable() { // Ensures that the intended tasks are to be run in the JavaFX thread.
            @Override
            public void run() {
                initFX(fx_child);
            }
        });

        JLabel copyright = new JLabel("Mapping feature technology by Google Maps APIv3, Google \u00a9, All rights reserved.");
        copyright.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (Desktop.isDesktopSupported()) {
                    try {
                        Desktop.getDesktop().browse(new URI("https://developers.google.com/maps/terms"));
                    } catch (Exception ex) {
                        ErrorMessages.errorDisplayErrorBox(ex, "there was an error when displaying map, pleas contact your system administrator");
                    }
                }
            }
        });
        copyright.setFont(new Font("Sans-Serif", Font.PLAIN, 10));
        copyright.putClientProperty("html", null);
        main.add(fx_child, BorderLayout.CENTER);
        main.add(copyright, BorderLayout.SOUTH);
    }

    /**
     * This method is made to implement FX functionality into Swing.
     * @param child is the FX function that has been implemented, in this case
     *              the map.
     */

    private void initFX(JFXPanel child) {
        Platform.setImplicitExit(false);
        mapView = new WebMap();
        Scene scene = mapView.initExternal();
        child.setScene(scene);
    }

    ///////// MAIN CLASS
    public static void main(String[] args) {
        Administration loginpage = new Administration();
    }
}